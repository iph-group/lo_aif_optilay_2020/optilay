![](https://www.iph-hannover.de/_media/images/logos/Headerlogo-IPH-ZUSE.jpg?m=1593587978)

# LO\_AiF\_OptiLay\_2020


[Project Homepage](https://optilay.iph-hannover.de)

## Set up a Layout

### Add workplaces and restricted areas

The first step is to create workplaces and restricted areas. These can be added by right-clicking on the respective cell. The corresponding menu items are: "Add workplace" "Add restricted area".
 
Several cells can be edited at the same time. To do this, hold down the left mouse button and drag a box around the corresponding cells.

### Add sink and source

By right-clicking on a cell at the edge of the layout, a sink or source can be added using the menu item "Add sink" or "Add source". 
If a placement is ambiguous, the system asks for further information on the placement of sink and source.

When the mouse is held still over a sink or source, further information about the sink/source is displayed after a short time.

### Add goods between sink and sources

To add goods between sinks and sources. Select a sink or source with right click and choose the menu option "Add goods".

In the following window you can choose which good to create. How much load it should have and how many goods are travelling between sink and source. 
And most importantly you can select the appropriate counterpart of the connection. The counterpart sink or source can be identified by the coordinate of the placement or their ID.


### Add conveyor

Conveyors can also be added by right-clicking on a cell. The corresponding menu entry is called "Add [Conveyor name]".
All possible conveyors are also listed in the sidebar under the Available conveyor section.

Conveyors that have already been placed can be moved within the layout by holding down the left mouse button.
If a conveyor is clicked with the right mouse button, a menu opens with which the conveyor can be rotated or deleted.
When the mouse is held still over a conveyor, further information about the conveyor is displayed after a short time.

## Evaluate Layout

If the current layout is only to be evaluated, this can be done by clicking the Evaluate button.

In the following window, the weighting of the various criteria for the evaluation can be set manually or the pairwise comparison can be used as a procedure for setting up the criteria weighting.

The evaluation is then carried out and the results are displayed.

## Optimise Layout

The optimisation procedure is started by pressing the optimise button. 
In the first step you can select the optimisation method or simply continue with the standard settings.
The second step is the selection of conveyors to be used during the optimisation.

Now everything is set up. Grab a cup of coffee and wait until the optimisation is finished.
If the optimisation is done, the 5 best layouts getting shown.