import logging
import sys
from multiprocessing import freeze_support

from PySide6.QtWidgets import QApplication

from optilay.gui.main_window import MainWindow
from optilay.main_system import MainSystem

if __name__ == '__main__':
    freeze_support()
    logging.basicConfig(level=logging.INFO)
    app = QApplication(sys.argv)
    system = MainSystem()
    window = MainWindow(system)
    window.show()
    sys.exit(app.exec())
