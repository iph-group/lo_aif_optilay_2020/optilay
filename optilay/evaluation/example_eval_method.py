from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.layout_model import LayoutModel


class MethodA:
    """
    This is an example evaluation class.
    The Evaluator initialises the class with the LayoutModel and then calls the start_evaluation method.
    Otherwise, the class can be freely modified
    """

    def __init__(self, layout: LayoutModel) -> None:
        """

        :param layout: LayoutModel that is to be evaluated.
        """
        self.layout = layout

    def start_evaluation(self) -> EvaluationResult:
        """
        The evaluation with this method is carried out here
        :return:
        """
        raise NotImplementedError
