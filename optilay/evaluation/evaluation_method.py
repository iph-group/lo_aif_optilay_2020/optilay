from enum import Enum


class EvaluationMethod(Enum):
    """
    This Enum class is used to specify in ControlParameter which evaluation method is to be used in the Evaluator.
    """
    Throughput = 0
    # LeadTime = 1
    SpaceRequirement = 2
    BufferCapacity = 3
    ComponentCosts = 4
    # ...
