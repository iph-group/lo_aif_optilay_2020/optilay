import math
import copy
from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.evaluation.util_graph.graph_generation import LayoutGraph
from optilay.evaluation.util_graph.intersection import Intersection
from optilay.evaluation.util_graph.stream import Stream


class Throughput:
    """
    This is an example evaluation class.
    The Evaluator initialises the class with the LayoutModel and then calls the start_evaluation method.
    Otherwise, the class can be freely modified
    """

    def __init__(self, layout: ConveyorSystemLayout, graph: LayoutGraph) -> None:
        """

        :param layout: LayoutModel that is to be evaluated.
        """
        self.layout = copy.deepcopy(layout)  # prevent overwriting of original data
        self.graph = graph  # generate graph from layout_model

    def start_evaluation(self) -> EvaluationResult:
        """
        # todo: Add link to CPSL-paper
        The evaluation with this method is carried out here
        :return:
        """

        # todo: use other paths from shortest_path_list

        # get paths (path planning)
        # path_list, shortest_paths_list, simple_paths_list = self.graph.get_paths(self.layout_model.packet_stream)

        # get intersections (path planning included)
        isp_vertices = []
        intersections_points = self.graph.get_intersections(self.layout.packet_stream)
        for isp in intersections_points:
            if isp[0] not in isp_vertices:
                isp_vertices.append(isp[0])

        # plot graph as picture
        # self.graph.plot_graph()

        # cluster intersections based on intersection_points
        intersections = self.cluster_intersections(intersections_points)

        # iterate over intersections and calc throughput
        max_bottle_neck = 1
        red_list = []
        while max_bottle_neck > 0:
            for intersection in intersections:
                intersection.calc_load_cycle_parameter()
                if intersection.throughput_plan == 0:
                    intersection.throughput_is = 0
                    continue

                # get conveyor of intersection to get velocity
                if intersection.vertex < 1000:  # check if virtual vertex
                    vertex = intersection.vertex
                else:
                    # reconvert virtual vertex
                    vertex = intersection.vertex - 1000

                conveyor = self.layout.get_conveyor_by_position(self.graph.graph.vs[vertex]["x_pos_v"],
                                                                self.graph.graph.vs[vertex]["y_pos_v"])

                # calc throughput of intersection
                # intersection.calc_throughput_easy(self.layout._cell_size.value, conveyor.velocity)
                intersection.calc_throughput_complex(self.layout._cell_size.value, conveyor.velocity)

            max_bottle_neck, red_entry = self.calc_stream_reduction(intersections, red_list)
            red_list += red_entry

        throughput_sum = 0
        # sum_amount_packet_streams = self.layout.get_all_good_amounts()
        for packet_stream in self.layout.packet_stream:

            if packet_stream._path:

                for isp in isp_vertices:
                    if isp in packet_stream._path:
                        break
                else:
                    # get velocity
                    velocity = 100000  # bigM
                    for vertex in packet_stream._path:
                        conveyor = self.layout.get_conveyor_by_position(self.graph.graph.vs[vertex]["x_pos_v"],
                                                                        self.graph.graph.vs[vertex]["y_pos_v"])
                        if velocity > conveyor.velocity:
                            velocity = conveyor.velocity

                    goods = packet_stream.get_goods()
                    for good in goods:
                        throughput = round((velocity /
                                            (self.layout._cell_size.value + ((good._size[0] + good._size[1]) / 2))
                                            ) * 3600)
                        if packet_stream._packets[good] > throughput:
                            packet_stream.set_goods_amount(good, throughput)

                goods = packet_stream.get_goods()
                for good in goods:
                    throughput_sum += packet_stream._packets[good]

            else:
                # add penalty if packet stream is not connected
                # pac_amount = packet_stream.get_sum_of_good_amounts()
                # throughput_sum -= round(packet_stream * (pac_amount/sum_amount_packet_streams))
                throughput_sum -= round(packet_stream.get_sum_of_good_amounts() * 0.5)
                # breakpoint()

        return throughput_sum

    def calc_stream_reduction(self, intersections, former_reduce_list):
        int_amount = []
        bottle_neck = []
        for intersection in intersections:
            bottle_neck.append(intersection.throughput_plan - intersection.throughput_is)
            if intersection.throughput_is == 0:
                int_amount.append([])
                continue
            str_amount = []
            for stream in intersection.streams:
                new_amount = math.floor((intersection.throughput_is * (stream.amount / intersection.throughput_plan)) / 10) * 10
                for pac_stream in stream.packet_streams:
                    goods = pac_stream.get_goods()
                    for good in goods:
                        if pac_stream._packets[good] > 0:
                            new_good_amount = math.floor((new_amount * (pac_stream._packets[good] / stream.amount)) / 10) * 10
                            red_good_amount = pac_stream._packets[good] - new_good_amount
                            str_amount.append([good.id, new_good_amount, red_good_amount, pac_stream._packets[good], False])
                        else:
                            str_amount.append([good.id, 0, 0, 0, False])
            else:
                # str_amount.append(new_amount)
                int_amount.append(str_amount)
        else:
            # del(str_amount, new_amount, new_good_amount, good, goods, pac_stream, stream, intersection)
            pass

        if bottle_neck:
            reduce_idx = bottle_neck.index(max(bottle_neck))
        else:
            return -1, []  # no intersections -> nothing to reduce -> no bottleneck -> return values to break while loop

        if bottle_neck[reduce_idx] > 0:
            flac = False
            tyler = 0
            for good in int_amount[reduce_idx]:
                for entry in former_reduce_list:
                    # if good was reduced before, save previous amounts
                    if good[0] == entry[0]:
                        good[2] = entry[1]  # previous reduction
                        good[3] = entry[3]  # original amount
                        good[4] = True  # flac for later identification
                        flac = True
                        # breakpoint()

            if flac:
                old_amount = 0
                for good in int_amount[reduce_idx]:
                    old_amount += good[3]  # calc original plan_throughput

                dif = []
                for good in int_amount[reduce_idx]:
                    # calc throughput based on original plan_thr
                    good[1] = math.floor(
                        (intersections[reduce_idx].throughput_is *
                         (good[3] / old_amount)) / 10) * 10
                    if good[4]:  # if good was previously reduced check if reduction was enough
                        if good[1] > good[2]:  # if actual calculated reduction  > previous reduction
                            dif.append(good[1] - good[2])  # save difference to reduce other goods
                            good[1] = good[2]  # save previous reduction as enough
                        else:
                            # overwrite / delete old reduction and set new as actual
                            good[4] = False
                            good[2] = good[3] - good[1]

                        # kick old entry in former_reduction_list from list (will be re-writen as return!)
                        for entry in former_reduce_list:
                            if good[0] == entry[0]:
                                former_reduce_list.pop(former_reduce_list.index(entry))
                                break

                if dif:
                    tyler = len(int_amount[reduce_idx]) - len(dif)  # spread not-reduce-potential over all other
                    dif = sum(dif)  # calc amount that can be reduced from other goods (not reduced before)

                    for good in int_amount[reduce_idx]:
                        if not good[4]:
                            good[1] = good[1] + math.floor(dif / tyler)  # add unused reduction potential to good
                        else:
                            good[4] = False
                        good[2] = good[3] - good[1]  # save difference between original amount and reduction

            for good in int_amount[reduce_idx]:
                if good[1] > 0:
                    self.layout.set_goods_amount_by_id(good[0], good[1])  # reduce good amount in data_model !!!
                else:
                    self.layout.set_goods_amount_by_id(good[0], 0)

        return bottle_neck[reduce_idx], int_amount[reduce_idx]

    def cluster_intersections(self, intersections_points):
        # intersection_point (isp)
        # isp[0]: vertex
        # isp[1]: intersection_type
        # isp[2]: bool if path is straight
        # isp[3]: bool if path is horizontal
        # isp[4]: packet_stream (object)
        # isp[5]: vertex of packet_stream source
        # isp[6]: [vertex sequence] of intersection

        # identify all intersection points of intersection type = 1 (opmo = opposing movement) -> separation
        opmo_vertices = []
        for isp_index in range(0, len(intersections_points), 1):
            if intersections_points[isp_index][1] == 1:
                if intersections_points[isp_index][0] not in opmo_vertices:
                    opmo_vertices.append(intersections_points[isp_index][0])

        # cluster type=1 intersection
        opmo_paths = []
        while opmo_vertices:
            isp = opmo_vertices[0]

            for path in opmo_paths:
                if isp in path:
                    path_index = opmo_paths.index(path)
                    # path.append(isp)
                    break
            else:
                opmo_paths.append([isp])
                path_index = -1

            neighbors = self.graph.graph.neighbors(isp)
            for neighbor in neighbors:
                if neighbor in opmo_vertices:
                    opmo_paths[path_index].append(neighbor)
            opmo_vertices.pop(0)

        delete_list = []
        for isp in intersections_points:
            if intersections_points.index(isp) in delete_list:
                # jump over isp that have been included in other isp before
                continue
            for isp_index in range(len(opmo_paths)):
                if isp[0] in opmo_paths[isp_index]:  # check if isp-vertex in opmo_path
                    path_index = isp[4]._path.index(isp[0])  # store path_index
                    for neighbor in range(path_index, len(isp[4]._path), 1):  # iterate over path in packet_stream
                        if isp[0] == isp[4]._path[neighbor]:  # reset intersection_path of isp
                            isp[6] = [isp[0]]
                            continue
                        if isp[4]._path[neighbor] in opmo_paths[
                            isp_index]:  # if path_element (p_stream) in opmo_path
                            isp[6].append(isp[4]._path[neighbor])  # add to intersection_path of isp
                        else:
                            break  # if path_element (p-stream) not in opmo-path -> intersection is passed

                    # recalc horizontal and straight
                    start = isp[4]._path.index(isp[6][0])
                    if start > 0:
                        start -= 1
                    end = isp[4]._path.index(isp[6][-1])
                    if end < len(isp[4]._path) - 1:
                        end += 1
                    help_path = []
                    for help_index in range(start, end + 1, 1):
                        help_path.append(isp[4]._path[help_index])
                    isp[3], isp[2] = self.graph.vertex_seq_is_horizontal(*help_path)

                    # identify and store other vertices of same packet_stream in same intersection
                    for help_index in range(intersections_points.index(isp), len(intersections_points), 1):
                        if help_index == intersections_points.index(isp):
                            continue
                        if intersections_points[help_index][0] in isp[6] and intersections_points[help_index][4] == \
                                isp[4]:
                            delete_list.append(help_index)

                    # set new virtual_vertex_id and intersection_type
                    isp[0] = 1000 + isp_index
                    isp[1] = 1

                    break

        else:
            # delete duplicates from delete_list
            new_list = []
            for elem in delete_list:
                if elem not in new_list:
                    new_list.append(elem)
            delete_list = new_list
            # reverse delete_list
            delete_list.reverse()
            # iterate over delete_list and delete unnecessary entries
            for isp_index in delete_list:
                intersections_points.pop(isp_index)
            # delete unnessary variables
            #del (neighbor, neighbors, start, end, opmo_paths, opmo_vertices, elem, new_list, path, path_index,
            #     help_index, help_path)

        # intersection_points -> intersections
        intersections = []
        while intersections_points:
            intersections.append(Intersection())
            intersections[-1].vertex = intersections_points[0][0]

            # get all intersection_points/streams belonging to intersection
            delete_list = []
            for isp in intersections_points:
                stream_index = None
                # check if same vertex
                if isp[0] == intersections[-1].vertex:
                    if isp[1] not in intersections[-1].type:
                        intersections[-1].type.append(isp[1])

                    if intersections[-1].streams:

                        for stream in intersections[-1].streams:
                            if stream.path == isp[6]:
                                for pac_stream in stream.packet_streams:
                                    if pac_stream == isp[4]:
                                        stream_index = -2
                                        break
                                else:
                                    stream_index = intersections[-1].streams.index(stream)
                                    break

                        else:
                            if stream_index != -2:
                                stream_index = -1

                    else:
                        stream_index = -1

                    if stream_index == -1:
                        # init new stream in intersection
                        intersections[-1].streams.append(Stream())
                        intersections[-1].streams[-1].path = isp[6]
                        intersections[-1].streams[-1].straight = isp[2]
                        intersections[-1].streams[-1].horizontal = isp[3]

                        if 1 in intersections[-1].type:
                            # calc number of conveyors in opmo intersection
                            intersections[-1].streams[-1].h = math.ceil(len(isp[6]) / 2)

                    if stream_index > -2:

                        intersections[-1].streams[-1].packet_streams.append(isp[4])

                        goods = isp[4].get_goods()
                        for good in goods:
                            old_amount = intersections[-1].streams[-1].amount
                            new_amount = old_amount + isp[4]._packets[good]

                            # calc new virtual good_size
                            """
                            intersections[-1].streams[-1].size[0] = math.ceil(((old_amount *
                                                                                intersections[-1].streams[-1].size[
                                                                                    0]) + (isp[4]._packets[good] *
                                                                                           good._size[
                                                                                               0])) / new_amount)
                            intersections[-1].streams[-1].size[1] = math.ceil(((old_amount *
                                                                                intersections[-1].streams[-1].size[
                                                                                    1]) + (isp[4]._packets[good] *
                                                                                           good._size[
                                                                                               1])) / new_amount)
                            """

                        intersections[-1].streams[-1].amount = new_amount  # update virtual good_amount
                        # amount_list[stream_index] += isp[4]._packets[good]

                    delete_list.append(intersections_points.index(isp))
            else:
                delete_list.reverse()
                for isp_index in delete_list:
                    intersections_points.pop(isp_index)

        return intersections
