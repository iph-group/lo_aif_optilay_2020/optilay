class EvaluationResult:
    """
    This class contains all results of an evaluation.
    """

    def __init__(self, layout_id: int) -> None:
        """

        :param layout_id: This id assigns the result to a ConveyorSystemLayout
        """
        big_m = 10000000

        self.layout_id: int = layout_id
        self.weighted_sum = 0
        self.throughput = [0, 0]  # maximise
        self.lead_time = [big_m, 0]  # minimise
        self.space_requirement = [big_m, 0]  # minimise
        self.buffer_capacity = [0, 0]  # maximise
        self.costs = [big_m, 0]  # minimise

        # If changes are made to the stored values, then these must also be adjusted
        # in weighting.py, spider_chart.py and result_table_dialog.py.
