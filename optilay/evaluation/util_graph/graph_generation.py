import igraph as ig

from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.util.cell_values import DEACTIVATED_AREA, WORKPLACE, RESTRICTED_AREA


class LayoutGraph:
    """

    """

    def __init__(self, layout_object: ConveyorSystemLayout) -> None:
        """
        side = 0 -> west
        side = 1 -> north
        side = 2 -> east
        side = 3 -> south
        side = 5 -> centre
        """
        # init
        layout_object = layout_object
        layout = layout_object.layout
        conveyor_list = layout_object.conveyors
        system_connections = layout_object.connection

        # init graph
        self.graph = ig.Graph()  # vertex_attrs={'y': 0, 'x': 0, 'side': 0})

        test_connection = [1, 1, 0, 1, 9]
        connection_list = []
        conv_velocity = 900

        # find/set vertices for conveyors
        for conveyor in conveyor_list:

            # add multiple vertices
            self.graph.add_vertices(["Conv:" + str(conveyor.x) + "," + str(conveyor.y) + ",0",
                                     "Conv:" + str(conveyor.x) + "," + str(conveyor.y) + ",1",
                                     "Conv:" + str(conveyor.x) + "," + str(conveyor.y) + ",5"],
                                    {'y': [conveyor.y, conveyor.y - 0.5, conveyor.y],
                                     'x': [conveyor.x - 0.5, conveyor.x, conveyor.x],
                                     'y_pos_v': [conveyor.y, conveyor.y, conveyor.y],
                                     'x_pos_v': [conveyor.x, conveyor.x, conveyor.x],
                                     'side_v': [0, 1, 5],
                                     'color': ["snow", "snow", "snow2"]})

            # check for endings and boarders
            if (conveyor.x + 1 > layout.shape[1] - 1 or layout[conveyor.y, conveyor.x + 1] == -1
                or layout[conveyor.y, conveyor.x + 1] == DEACTIVATED_AREA) and \
                    ((conveyor.rotation == 0 and conveyor.trans_direction.value == 0) or
                     conveyor.trans_direction.value != 0):
                self.graph.add_vertices("Conv:" + str(conveyor.x) + "," + str(conveyor.y) + ",2",
                                        {'y': conveyor.y, 'x': conveyor.x + 0.5,
                                         'y_pos_v': conveyor.y, 'x_pos_v': conveyor.x, 'side_v': 2, 'color': "snow2"})

            if (conveyor.y + 1 > layout.shape[0] - 1 or layout[conveyor.y + 1, conveyor.x] == -1 or
                layout[conveyor.y + 1, conveyor.x] == DEACTIVATED_AREA) and \
                    ((conveyor.rotation == 1 and conveyor.trans_direction.value == 0) or
                     conveyor.trans_direction.value != 0):
                self.graph.add_vertices("Conv:" + str(conveyor.x) + "," + str(conveyor.y) + ",3",
                                        {'y': conveyor.y + 0.5, 'x': conveyor.x,
                                         'y_pos_v': conveyor.y, 'x_pos_v': conveyor.x, 'side_v': 3, 'color': "snow2"})

            if conveyor.trans_direction.value == 0:
                if conveyor.rotation == 0:
                    connection_list.append(
                        [0, 0, 0, 5, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect horizontal
                    connection_list.append(
                        [0, 0, 5, 2, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect horizontal
                    connection_list.append(
                        [1, 0, 5, 0, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect horizontal
                    # old: connection_list.append([1, 0, 0, 0, 2])  # connect horizontal
                    # x_add = 1
                    # y_add = 0
                    # side_from = 0
                    # side_to = 0
                    # side_add = 2
                elif conveyor.rotation == 1:
                    connection_list.append(
                        [0, 0, 1, 5, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect vertical
                    connection_list.append(
                        [0, 0, 5, 3, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect vertical
                    connection_list.append(
                        [0, 1, 5, 1, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect vertical
                    # old: connection_list.append([0, 1, 1, 1, 3])  # connect vertical

            if conveyor.trans_direction.value != 0:
                connection_list.append([0, 0, 0, 5, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect horizontal
                connection_list.append([0, 0, 5, 2, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect horizontal
                connection_list.append([1, 0, 5, 0, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect horizontal
                connection_list.append([0, 0, 1, 5, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect vertical
                connection_list.append([0, 0, 5, 3, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect vertical
                connection_list.append([0, 1, 5, 1, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect vertical

            if conveyor.trans_direction.value == 2:
                # init help objects
                neighbor_vertical = None
                neighbor_horizontal = None

                y_add_list = [1, -1]

                for y_add in y_add_list:

                    neighbor_vertical = None
                    neighbor_horizontal = None
                    neighbor_to_connect = None

                    try:
                        neighbor_vertical = layout_object.get_conveyor_by_position(conveyor.x, conveyor.y + y_add)
                        neighbor_horizontal = layout_object.get_conveyor_by_position(conveyor.x + 1, conveyor.y)
                        neighbor_to_connect = layout_object.get_conveyor_by_position(conveyor.x + 1, conveyor.y + y_add)
                    except:
                        pass

                    if neighbor_vertical is not None and neighbor_vertical.trans_direction.value == 2 and \
                            neighbor_horizontal is not None and neighbor_horizontal.trans_direction.value == 2 and \
                            neighbor_to_connect is not None and neighbor_to_connect.trans_direction.value == 2:

                        if y_add == 1:

                            # add vertex in center of celluveyor matrix (side = 6)
                            self.graph.add_vertices("Conv:" + str(conveyor.x) + "," + str(conveyor.y) + ",6",
                                                    {'y': conveyor.y + 0.5, 'x': conveyor.x + 0.5,
                                                     'y_pos_v': conveyor.y, 'x_pos_v': conveyor.x, 'side_v': 6,
                                                     'color': "snow3"})

                            # add connections to and from new vertex
                            connection_list.append([0, 0, 5, 6, 9, conveyor.x, conveyor.y, conveyor.rotation])
                            connection_list.append([1, 1, 6, 5, 9, conveyor.x, conveyor.y, conveyor.rotation])
                            # [1, 1, 5, 5, 9, conveyor.x, conveyor.y, conveyor.rotation])  # connect side 0 and 0 y+1

                        elif y_add == -1:
                            connection_list.append([0, -1, 5, 6, 9, conveyor.x, conveyor.y, conveyor.rotation])
                            connection_list.append([1, 0, 6, 5, 9, conveyor.x, conveyor.y - 1, conveyor.rotation])
                            # [1, -1, 5, 5, 9, conveyor.x, conveyor.y, conveyor.rotation]  # connect side 0 and 0 y+1

        # connect vertices with edges
        for connection in connection_list:

            for x, y in zip(connection, test_connection):
                if x != y:
                    break
            else:
                # absolute special case: connect vertices of two other conveyors
                breakpoint()  # will it ever happen? # todo: delete
                try:
                    # connect two other vertices 'outside' conveyor
                    self.graph.add_edges([(self.graph.vs.find(x_pos_v=connection[5] + connection[0],
                                                              y_pos_v=connection[6],
                                                              side_v=connection[2]),
                                           self.graph.vs.find(x_pos_v=connection[5],
                                                              y_pos_v=connection[6] + connection[1],
                                                              side_v=connection[3]))
                                          ], {'x_pos_e': connection[5], 'y_pos_e': connection[6],
                                              'rotation': connection[7], 'weight': conv_velocity})
                except:
                    pass

            """
            # if not connection == [1,1,0,1,9]
            if not (connection[0] == 1 and connection[1] == 1 and connection[2] == 0 and
                    connection[3] == 1 and connection[4] == 9):  # todo!!!
            """
            # try add_edge
            try:
                # add edge with attributes
                self.graph.add_edges(
                    [(self.graph.vs.find(x_pos_v=connection[5], y_pos_v=connection[6], side_v=connection[2]),
                      self.graph.vs.find(x_pos_v=connection[5] + connection[0],
                                         y_pos_v=connection[6] + connection[1],
                                         side_v=connection[3]))
                     ], {'x_pos_e': connection[5], 'y_pos_e': connection[6],
                         'rotation': connection[7], 'weight': conv_velocity})
            except:
                pass

            try:
                # graph.add_edge(graph.vs.find(x=conveyor.x, y=conveyor.y, side=side_saver),
                #               graph.vs.find(x=conveyor.x, y=conveyor.y, side=side_saver + 2))

                # add edge if last conveyor in line
                self.graph.add_edges(
                    [(self.graph.vs.find(x_pos_v=connection[5], y_pos_v=connection[6], side_v=connection[2]),
                      self.graph.vs.find(x_pos_v=connection[5], y_pos_v=connection[6], side_v=connection[4]))
                     ], {'x_pos_e': connection[5], 'y_pos_e': connection[6],
                         'rotation': connection[7], 'weight': conv_velocity})
                breakpoint()  # will it ever happen? todo: delete
            except:
                pass

        # find/set vertices for system connections
        for sys_connection in system_connections:
            # source = 0 (type)
            # sink = 1 (type)
            if sys_connection.type.value == 1:
                color = "cyan"
            else:
                color = "green"

            y_add = 0
            x_add = 0
            x = sys_connection.connected_position[1]
            y = sys_connection.connected_position[0]
            # connection = [x:int, y:int, side:int]
            connection = []

            if sys_connection.direction.value == 0:  # horizontal
                if x - 1 < 0 or layout[y][x - 1] == DEACTIVATED_AREA:
                    # west
                    x_add = -1
                    connection = [sys_connection.connected_position[1], sys_connection.connected_position[0],
                                  sys_connection.type.value, 0]

                elif x + 1 >= layout.shape[1] or layout[y][x + 1] == DEACTIVATED_AREA:
                    # east
                    x_add = 1
                    connection = [sys_connection.connected_position[1], sys_connection.connected_position[0],
                                  sys_connection.type.value, 2]

                else:
                    breakpoint()

            elif sys_connection.direction.value == 1:  # vertical
                if y - 1 < 0 or layout[y - 1][x] == DEACTIVATED_AREA:
                    # north
                    y_add = -1
                    connection = [sys_connection.connected_position[1], sys_connection.connected_position[0],
                                  sys_connection.type.value, 1]

                elif y + 1 >= layout.shape[0] or layout[y + 1][x] == DEACTIVATED_AREA:
                    # south
                    y_add = 1
                    connection = [sys_connection.connected_position[1], sys_connection.connected_position[0],
                                  sys_connection.type.value, 3]

                else:
                    breakpoint()

            else:
                breakpoint()

            self.graph.add_vertices("SC:" + str(sys_connection.connected_position[1]) + "," +
                                    str(sys_connection.connected_position[0]) + "," +
                                    str(sys_connection.type.value),
                                    {'y': sys_connection.connected_position[0] + y_add,
                                     'x': sys_connection.connected_position[1] + x_add,
                                     'y_pos_v': sys_connection.connected_position[0],
                                     'x_pos_v': sys_connection.connected_position[1],
                                     'color': color,
                                     'type': sys_connection.type.value})

            try:
                self.graph.add_edges([(self.graph.vs.find(x_pos_v=connection[0], y_pos_v=connection[1],
                                                          type=connection[2]),
                                       self.graph.vs.find(x_pos_v=connection[0], y_pos_v=connection[1],
                                                          side_v=connection[3]))
                                      ], {'x_pos_e': connection[0], 'y_pos_e': connection[1], 'weight': 0})
            except:
                pass

    def plot_graph(self):
        # set drawing style
        # color names: https://en.wikipedia.org/wiki/X11_color_names

        for index in range(0, len(self.graph.vs), 1):
            self.graph.vs[index]["index"] = index

        style = {"edge_curved": False, "margin": 9, "vertex_label": self.graph.vs["index"],
                 "vertex_frame_color": "black", "vertex_label_color": "black", "vertex_label_size": 13}
        # draw graph
        ig.plot(self.graph, **style)

    def get_intersections(self, packet_streams) -> list:
        # logic from
        # https://gitlab.com/iph-group/lo_aif_mefap_2017/mefap/-/blob/master/mefap/evaluation/eval_material_flow.py

        # init path list <- writen before!
        # path_list, shortest_paths_list = self.get_paths(packet_streams, write_to_packet_streams=True)

        # init variables
        crossing_points = []
        not_crossing_points = []
        intersection_list = []

        for pac_stream in packet_streams:

            if pac_stream.get_sum_of_good_amounts == 0:
                continue

            curr_path = pac_stream.get_path()

            # init properties of path
            straight_path = True
            horizontal = True

            for element_num in range(1, len(curr_path) - 1, 1):
                curr_element = curr_path[element_num]
                # check if crossing point is all ready found
                if curr_element in not_crossing_points:  # curr_element in crossing_points or
                    continue

                # generate test triple of path elements
                curr_before = curr_path[element_num - 1]
                curr_next = curr_path[element_num + 1]
                elements_list = [curr_before, curr_element, curr_next]
                elements_list_re = [curr_next, curr_element, curr_before]

                # check all other paths, if they are using the same path_element
                for try_pac_stream in packet_streams:
                    try_path = try_pac_stream.get_path()
                    straight_try = None
                    # check if test_path equal with curr_path
                    if curr_path == try_path:
                        continue

                    # check if try_element is in test_path
                    try:
                        test_element_num = try_path.index(curr_element)
                    except ValueError:
                        continue

                    # generate second element triple
                    try_before = try_path[test_element_num - 1]
                    try_element = try_path[test_element_num]
                    try_next = try_path[test_element_num + 1]
                    try_list = [try_before, try_element, try_next]

                    # check if crossing or not
                    if elements_list != try_list:  # and elements_list_re != try_list:

                        # identify if paths are straight and horizontal
                        try:
                            horizontal, straight_path = self.vertex_seq_is_horizontal(*elements_list)
                            straight_try = self.vertex_seq_is_straight(*try_list)
                        except Exception as e:
                            print(e)
                            continue

                        # identify intersection type
                        if elements_list[0] != try_list[0] and elements_list[0] != try_list[2] \
                                and elements_list[2] != try_list[0] and elements_list[2] != try_list[2] \
                                and straight_path and straight_try:
                            # simple intersection
                            intersection_type = 0

                        elif elements_list_re == try_list or \
                                (elements_list[0] == try_list[2] and elements_list[1] == try_list[1] and
                                 elements_list[2] != try_list[0]) or \
                                (elements_list[0] != try_list[2] and elements_list[1] == try_list[1] and
                                 elements_list[2] == try_list[0]):
                            # opposing movement
                            intersection_type = 1

                        elif (elements_list[0] == try_list[0] and elements_list[2] != try_list[2]) or \
                                (elements_list[0] != try_list[0] and elements_list[2] == try_list[2]) or \
                                (elements_list[0] != try_list[0] and elements_list[1] == try_list[1] and
                                 elements_list[2] != try_list[2]):
                            if straight_path is False and straight_try is False:
                                # merge or split
                                intersection_type = 2
                            else:
                                # inflow or outflow
                                intersection_type = 3

                        else:
                            intersection_type = 5
                            print("What kind of intersection is this?: " + str(intersection_type) +
                                  "; First: " + str(elements_list) + "; Second: " + str(try_list))
                            # breakpoint()
                            # continue

                        intersection_tuple = [curr_element, intersection_type,
                                              straight_path,
                                              horizontal,
                                              pac_stream,  # .source, pac_stream.sink,
                                              curr_path[0], elements_list]  # , try_path[0], try_list)
                        intersection_list.append(intersection_tuple)
                        crossing_points.append(curr_element)
                        # check if needs the break and if it's fast without
                        # break

                # not_crossing_points
                if curr_element not in crossing_points:
                    not_crossing_points.append(curr_element)

        # print(crossing_points)

        # change color in plot
        for cp in intersection_list:
            if cp[1] == 0:
                self.graph.vs[cp[0]]['color'] = "salmon"
            elif cp[1] == 1:
                self.graph.vs[cp[0]]['color'] = "orchid"
            elif cp[1] == 2:
                self.graph.vs[cp[0]]['color'] = "pink"
            elif cp[1] == 3:
                self.graph.vs[cp[0]]['color'] = "plum"
            elif cp[1] == 5:
                self.graph.vs[cp[0]]['color'] = "red"

        return intersection_list

    def get_buffer_amount(self, packet_streams, conveyors, connections) -> int:
        """
        """
        con_vertices = []
        for connection in connections:
            con_vertices.append(self.graph.vs.find(x_pos_v=connection.connected_position[1],
                                                   y_pos_v=connection.connected_position[0],
                                                   type=connection.type.value).index)

        # path_list, shortest_paths = self.get_paths(packet_streams, write_to_packet_streams=False) -> writen before!
        combined_path_list = []
        for pac_stream in packet_streams:
            combined_path_list += pac_stream._path

        # delete duplicates
        new_list = []
        for elem in combined_path_list:
            if elem not in new_list:
                new_list.append(elem)
        combined_path_list = new_list

        buffer_capacity = 0
        for conveyor in conveyors:
            # find center_vertex of conveyor
            vertex = self.graph.vs.find(x_pos_v=conveyor.x, y_pos_v=conveyor.y,
                                        side_v=5).index  # find center_vertex of conveyor
            if vertex not in combined_path_list:
                for sys_con in con_vertices:
                    length = self.graph.shortest_paths(vertex, sys_con)
                    if not length[0][0] == float('inf'):
                        buffer_capacity += 1
                        break

        return buffer_capacity

    def get_paths(self, packet_streams, write_to_packet_streams=False) -> object:

        # init path lists
        path_list = []  # first shortest path
        # path_list_short = []  # all shortest paths
        # path_list_simple = []  # all paths

        # iterate over all material flows based on packet streams
        for pac_stream in packet_streams:

            # get source and sink of packet stream
            source_con = pac_stream.source
            sink_con = pac_stream.sink

            # only start at sources
            if not source_con.type.value == 0:
                continue

            # only end on sinks AND sink aren't sources!
            if source_con == sink_con or sink_con.type.value == 0:
                continue

            # find source vertex
            try:
                start_vertex = self.graph.vs.find(x_pos_v=source_con.connected_position[1],
                                                  y_pos_v=source_con.connected_position[0],
                                                  type=source_con.type.value)
            except:
                raise Exception("source vertex does not exist -> failure in init graph")

            # find sink vertex
            try:
                end_vertex = self.graph.vs.find(x_pos_v=sink_con.connected_position[1],
                                                y_pos_v=sink_con.connected_position[0],
                                                type=sink_con.type.value)
            except:
                raise Exception("sink vertex does not exist -> failure in init graph")

            # store paths in paths lists
            paths = self.shortest_path_vertices(start_vertex, end_vertex)
            if not paths:
                paths.append([])
            path_list.append(paths)

            # path_list_short.append(paths[0])  # self.shortest_path_vertices(start_vertex, end_vertex))
            # path_list_simple.append(self.simple_path_vertices(start_vertex, end_vertex))
            # add to current packet stream
            if write_to_packet_streams:
                pac_stream.add_path(paths[0])

        return path_list  # , path_list_short  # , path_list_simple

    def convert_paths_from_vertex_to_cell(self, layout: ConveyorSystemLayout, path_list) -> None:

        # iterate over all paths in path_list respectively over packet_streams
        for path in range(len(path_list)):
            cell_list = []

            for vertex in path_list[path]:
                # find vertex and get cell
                x_pos = self.graph.vs[vertex]["x_pos_v"]
                y_pos = self.graph.vs[vertex]["y_pos_v"]
                # store cell in list, if not allready stored
                if [x_pos, y_pos] not in cell_list:
                    cell_list.append([x_pos, y_pos])
            # write path to pac_stream
            layout.packet_stream[path].add_path(cell_list)

    def shortest_path_lengths(self, vertex_from_a, vertex_from_b, vertex_to_a, vertex_to_b) -> float:
        # calc multiple path lengths
        return self.graph.shortest_paths([vertex_from_a, vertex_from_b], [vertex_to_a, vertex_to_b])

    def shortest_path_length(self, vertex_from_a, vertex_to_a) -> float:
        # calc path length
        return self.graph.shortest_paths(vertex_from_a, vertex_to_a)

    def shortest_path_edges(self, vertex_from, vertex_to) -> list:
        # https://igraph.org/python/api/latest/igraph._igraph.GraphBase.html#get_shortest_paths
        # calc path -> return edges
        # epath = return edges
        return self.graph.get_shortest_paths(vertex_from, vertex_to, "weight", "out", "epath")

    def shortest_path_vertices(self, vertex_from, vertex_to) -> list:
        # https://igraph.org/python/api/latest/igraph._igraph.GraphBase.html#get_shortest_paths
        # calc path -> return vertices
        # vpath = return vertices
        # return self.graph.get_shortest_paths(vertex_from, vertex_to, "weight", "out", "vpath")

        # https://igraph.org/python/doc/api/igraph._igraph.GraphBase.html#get_all_shortest_paths
        # weights: edge weights in a list or the name of an edge attribute holding edge weights.
        # If None, all edges are assumed to have equal weight.
        return self.graph.get_all_shortest_paths(vertex_from, vertex_to, None, "out")  # , "vpath")

    def simple_path_vertices(self, vertex_from, vertex_to) -> list:
        # https://igraph.org/python/api/latest/igraph._igraph.GraphBase.html#get_shortest_paths
        # calc path -> return vertices
        # vpath = return vertices
        return self.graph.get_all_simple_paths(vertex_from, vertex_to, mode="out")

    def vertex_seq_is_straight(self, *args):
        straight = True
        y_flag = None

        # check if connected
        for index in range(0, len(args) - 1, 1):
            if self.graph.vs[args[index + 1]] not in self.graph.vs[args[index]].neighbors():
                raise Exception("Vertices aren't connected: " + str(args[index]) + ", " + str(args[index + 1]))

        if len(args) > 2:

            # get direction of first elements
            if self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] and \
                    self.graph.vs[args[0]]["x"] != self.graph.vs[args[1]]["x"]:
                y_flag = True  # vertical
            elif self.graph.vs[args[0]]["y"] != self.graph.vs[args[1]]["y"] and \
                    self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"]:
                y_flag = False  # horizontal
            else:
                # check if diagonal (celluveyor)
                if self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] - 0.5 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] - 0.5:
                    # diagonal left up
                    y_add, x_add = -0.5, -0.5

                elif self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] - 0.5 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] + 0.5:
                    # diagonal right up
                    y_add, x_add = -0.5, 0.5

                elif self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] + 0.5 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] - 0.5:
                    # diagonal left down
                    y_add, x_add = 0.5, -0.5

                elif self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] + 0.5 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] + 0.5:
                    # diagonal right down
                    y_add, x_add = 0.5, 0.5

                else:
                    print("Something went wrong")
                    breakpoint()

            # check if direction change
            for index in range(0, len(args) - 1, 1):
                if y_flag and self.graph.vs[args[index]]["y"] != self.graph.vs[args[index + 1]]["y"]:
                    straight = False
                    break
                elif y_flag == False and self.graph.vs[args[index]]["x"] != self.graph.vs[args[index + 1]]["x"]:
                    straight = False
                    break
                elif y_flag is None:
                    if self.graph.vs[args[index]]["y"] + y_add != self.graph.vs[args[index + 1]]["y"] or\
                            self.graph.vs[args[index]]["x"] + x_add != self.graph.vs[args[index + 1]]["x"]:
                        straight = False
                        break

        return straight

    def vertex_seq_is_horizontal(self, *args):
        horizontal = None

        try:
            straight = self.vertex_seq_is_straight(*args)
        except Exception as e:
            raise e

        if straight and len(args) > 1:

            # get direction of first elements
            if self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] and \
                    self.graph.vs[args[0]]["x"] != self.graph.vs[args[1]]["x"]:
                horizontal = True
            elif self.graph.vs[args[0]]["y"] != self.graph.vs[args[1]]["y"] and \
                    self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"]:
                horizontal = False
            else:
                # check if diagonal (celluveyor)
                if self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] - 1 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] - 1:
                    # diagonal left up
                    horizontal = True
                elif self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] - 1 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] + 1:
                    # diagonal right down
                    horizontal = True
                elif self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] + 1 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] - 1:
                    # diagonal left up
                    horizontal = True
                elif self.graph.vs[args[0]]["y"] == self.graph.vs[args[1]]["y"] + 1 and \
                        self.graph.vs[args[0]]["x"] == self.graph.vs[args[1]]["x"] + 1:
                    # diagonal right up
                    horizontal = True

                else:
                    print("Something went wrong")
                    breakpoint()
        else:
            # raise Exception("Vertices aren't straight, prediction of horizontal/veritcal not possible")
            # raise Exception("Not enough vertices to predict horizontal/vertical")
            pass

        return horizontal, straight
