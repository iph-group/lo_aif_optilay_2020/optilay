
def shortest_path_lengths(graph, vertex_from_a, vertex_from_b, vertex_to_a, vertex_to_b) -> float:

    # calc multiple path lengths
    path_lengths = graph.shortest_paths([vertex_from_a, vertex_from_b], [vertex_to_a, vertex_to_b])

    return path_lengths


def shortest_path_length(graph, vertex_from_a, vertex_to_a) -> float:

    # calc path length
    path_length = graph.shortest_paths(vertex_from_a, vertex_to_a)

    return path_length


def shortest_path_edges(graph, vertex_from, vertex_to) -> list:
    # https://igraph.org/python/api/latest/igraph._igraph.GraphBase.html#get_shortest_paths
    # calc path -> return edges
    # epath = return edges
    path = graph.get_shortest_paths(vertex_from, vertex_to, "weights", "out","epath")

    return path


def shortest_path_vertices(graph, vertex_from, vertex_to) -> list:
    # https://igraph.org/python/api/latest/igraph._igraph.GraphBase.html#get_shortest_paths
    # calc path -> return vertices
    # vpath = return vertices
    path = graph.get_shortest_paths(vertex_from, vertex_to, "weights", "out", "vpath")

    return path
