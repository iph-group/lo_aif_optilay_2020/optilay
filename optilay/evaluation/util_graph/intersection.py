import math


class Intersection:

    def __init__(self):
        self.type = []
        self.vertex: int = None
        self.streams = []
        # self.gcd: int = None
        self.throughput_is: int = None
        self.throughput_plan: int = None

    def calc_load_cycle_parameter(self) -> None:
        amount_list = []
        for stream in self.streams:
            stream.amount = 0  # reset amount
            for pac_stream in stream.packet_streams:
                goods = pac_stream.get_goods()
                for good in goods:
                    good_amount = pac_stream._packets[good]
                    if good_amount > 0:
                        stream.size[0] = math.ceil(((stream.amount * stream.size[0]) + (good_amount * good._size[0])) / (
                                stream.amount + good_amount))
                        stream.size[1] = math.ceil(((stream.amount * stream.size[1]) + (good_amount * good._size[1])) / (
                                stream.amount + good_amount))
                        stream.amount += good_amount

            amount_list.append(stream.amount)

        self.throughput_plan = sum(amount_list)

        # self.gcd = self.calc_multi_gcd(*amount_list)

        if self.throughput_plan > 0:
            for stream in self.streams:
                # todo: validate * 10 !!!! -> maximum lot size!
                n = round((stream.amount / self.throughput_plan) * 10)  # self.gcd)
                if n < 1:
                    n = 1
                stream.n = n

    def calc_multi_gcd(self, *args) -> int:
        multi_gcd = args[0]
        if len(args) > 1:
            for index in range(1, len(args), 1):
                multi_gcd = math.gcd(multi_gcd, args[index])
        return multi_gcd

    def calc_throughput_easy(self, cell_size, velocity) -> None:
        # simple throughput calculation
        l_u = 0
        n_u = 0

        s_u = cell_size
        v_u = velocity  # 1000mm/1s

        flac_type_1 = False
        if 1 in self.type:
            flac_type_1 = True
            # s_u = s_u * stream.h

        for stream in self.streams:

            n_u += stream.n

            if stream.straight:
                if stream.horizontal:
                    if not flac_type_1:
                        l_u += stream.n * (s_u + stream.size[0])
                        # s_u + (stream.n * stream.size[0])
                    else:
                        l_u += (s_u * (stream.h - 1)) + (stream.n * (s_u + stream.size[0]))
                        # (s_u * stream.h) + (stream.n * stream.size[0])
                else:
                    if not flac_type_1:
                        l_u += stream.n * (s_u + stream.size[1])
                        # s_u + (stream.n * stream.size[1])
                    else:
                        l_u += (s_u * (stream.h - 1)) + (stream.n * (s_u + stream.size[1]))
                        # (s_u * stream.h) + (stream.n * stream.size[1])
            else:
                if not flac_type_1:
                    l_u += stream.n * (s_u + ((stream.size[0] + stream.size[1]) / 2))  # todo: check if generally valid
                else:
                    l_u += (s_u * (stream.h - 1)) + (stream.n * (s_u + (stream.size[0] + stream.size[1]) / 2))

        else:
            self.throughput_is = math.floor(n_u * ((v_u / l_u) * 3600))

    def calc_throughput_complex(self, cell_size, velocity) -> None:
        # todo: overview function

        # init variables
        s_u = cell_size
        v_u = velocity  # m/s

        l_u = 0
        n_u = 0

        # virtual lot size of load cycle
        n_i = 0
        n_j = 0
        n_k = 0

        # aggregation of length and width of goods (virtual size)
        l_iH = 0
        l_iV = 0
        l_jH = 0
        l_jV = 0
        l_kH = 0
        l_kV = 0

        ### calc throughput ###

        if len(self.type) == 1:
            intersection_type = self.type[0]
        else:
            if 3 in self.type:
                if len(self.streams) == 2:
                    intersection_type = 3
                elif len(self.streams) == 3:
                    intersection_type = 4
                else:
                    intersection_type = 5

        if intersection_type == 0:
            # case A

            for stream in self.streams:

                n_u += stream.n

                if stream.horizontal:
                    n_i = stream.n
                    l_iH = stream.size[0]
                else:
                    n_j = stream.n
                    l_jV = stream.size[1]

            l_u = ((s_u + n_i * l_iH) + (s_u + n_j * l_jV))

        elif intersection_type == 1:
            # case B
            """
            h = int
            # todo: identify correct sub cases
            t_iu = h * s_u + n_i * l_iV
            t_ju = h * s_u + n_j * l_jV
            t_ku = h * s_u + n_k * ((l_kH + l_kV)/2)

            l_u = (t_iu + t_ju + t_ku) / v_u
            """

            for stream in self.streams:

                n_u += stream.n

                if stream.straight:
                    if stream.horizontal:
                        l_u += (s_u * (stream.h - 1)) + (stream.n * (s_u + stream.size[0]))
                        # (s_u * stream.h) + (stream.n * stream.size[0])
                    else:
                        l_u += (s_u * (stream.h - 1)) + (stream.n * (s_u + stream.size[1]))
                        # (s_u * stream.h) + (stream.n * stream.size[1])
                else:
                    l_u += (s_u * (stream.h - 1)) + (stream.n * (s_u + (stream.size[0] + stream.size[1]) / 2))

        elif intersection_type == 2:
            # case C

            """
            for i in range(len(self.streams)):

                n_u += self.streams[i].n

                if i == 0:
                    n_i = self.streams[i].n
                    l_iH = self.streams[i].size[0]
                    l_iV = self.streams[i].size[1]
                elif i == 1:
                    n_j = self.streams[i].n
                    l_jH = self.streams[i].size[0]
                    l_jV = self.streams[i].size[1]
                else:
                    breakpoint()

            l_u = n_i * (s_u + (l_iH + l_iV)/2) + n_j * (s_u + (l_jH + l_jV)/2)
            """
            for stream in self.streams:
                n_u += stream.n

            l_u = self.streams[0].n * (s_u + (self.streams[0].size[0] + self.streams[0].size[1]) / 2) + \
                  self.streams[1].n * (s_u + (self.streams[1].size[0] + self.streams[1].size[1]) / 2)

        elif intersection_type == 3:
            # case D

            for stream in self.streams:

                n_u += stream.n

                if stream.straight:
                    n_j = stream.n
                    l_jH = stream.size[0]
                    l_jV = stream.size[1]

                    if stream.horizontal:
                        j_horizontal = True
                    else:
                        j_horizontal = False

                else:
                    n_i = stream.n
                    l_iH = stream.size[0]
                    l_iV = stream.size[1]

            if j_horizontal:
                if n_i <= n_j:
                    l_u = n_i * (s_u/2 + l_iV/2) + n_i * (l_jH + s_u) + (l_jH * (n_j - n_i))  # n_i <= n_j
                else:
                    l_u = n_i * (s_u/2 + l_iV/2) + (n_i - n_j) * (l_jH/2 + s_u/2) + n_j * (l_jH + s_u)  # n_i > n_j
            else:
                if n_i <= n_j:
                    l_u = n_i * (s_u/2 + l_iH/2) + n_i * (l_jV + s_u) + (l_jV * (n_j - n_i))  # n_i <= n_j
                else:
                    l_u = n_i * (s_u/2 + l_iH/2) + (n_i - n_j) * (l_jV/2 + s_u/2) + n_j * (l_jV + s_u)  # n_i > n_j

        elif intersection_type == 4:
            # case E

            for stream in self.streams:

                n_u += stream.n

                if stream.straight:
                    if n_j == 0:
                        n_j = stream.n
                        l_jH = stream.size[0]
                        l_jV = stream.size[1]
                        if stream.horizontal:
                            j_horizontal = True
                        else:
                            j_horizontal = False

                    else:
                        n_k = stream.n
                        l_kH = stream.size[0]
                        l_kV = stream.size[1]
                        k_straight = True
                        if stream.horizontal:
                            k_horizontal = True
                        else:
                            k_horizontal = False
                        k_last_elem = stream.path[-1]

                else:
                    if n_i == 0:
                        n_i = stream.n
                        l_iH = stream.size[0]
                        l_iV = stream.size[1]
                        i_last_elem = stream.path[-1]
                    else:
                        n_k = stream.n
                        l_kH = stream.size[0]
                        l_kV = stream.size[1]
                        k_straight = False

            if not k_straight:
                if n_i < n_k:
                    saver = [n_i, l_iH, l_iV]
                    n_i, l_iH, l_iV = n_k, l_kH, l_kV
                    n_k, l_kH, l_kV = saver[0], saver[1], saver[2]

            else:
                # if two are straight one (k) goes alone and one (j) goes with i
                if i_last_elem == k_last_elem:
                    saver = [n_j, l_jH, l_jV]
                    n_j, l_jH, l_jV = n_k, l_kH, l_kV
                    n_k, l_kH, l_kV = saver[0], saver[1], saver[2]

            if not k_straight:
                if j_horizontal:
                    if n_i >= n_k >= n_j:
                        l_u = n_i * (s_u + (l_iH + l_iV)/2) + n_k * (s_u/2 + l_kV/2) + n_j * (l_jH + s_u) + \
                              (n_k - n_j) * (s_u/2 + l_kH/2)
                    elif (n_i + n_k) > n_j > n_i > n_k:
                        l_u = n_i * (s_u/2 + l_iV/2) + n_k * (s_u/2 + l_kV/2) + n_j * (l_jH + s_u) + \
                              ((n_i+n_k)-n_j) * (s_u/2 + l_kH/2)
                    elif n_j >= (n_i + n_k):
                        l_u = n_i * (s_u/2 + l_iV/2) + n_k * (s_u/2 + l_kV/2) + (n_i + n_k) * (l_jH + s_u) + \
                              (n_j - (n_i + n_k)) * l_jH
                    else:
                        # print("What happend here?")
                        # todo: n_i > n_j, n_i > n_k , n_j > n_k
                        # todo: n_i = n_j, n_i > n_k, n_j > n_k
                        # todo: n_i = n_k, n_i > n_j, n_k > n_j
                        # breakpoint()
                        intersection_type = 5
                else:
                    if n_i >= n_k >= n_j:
                        l_u = n_i * (s_u + (l_iH + l_iV)/2) + n_k * (s_u/2 + l_kH/2) + n_j * (l_jV + s_u) + \
                              (n_k - n_j) * (s_u/2 + l_kV/2)
                    elif (n_i + n_k) > n_j > n_i > n_k:
                        l_u = n_i * (s_u/2 + l_iH/2) + n_k * (s_u/2 + l_kH/2) + n_j * (l_jV + s_u) + \
                              ((n_i+n_k)-n_j) * (s_u/2 + l_kV/2)
                    elif n_j >= (n_i + n_k):
                        l_u = n_i * (s_u/2 + l_iH/2) + n_k * (s_u/2 + l_kH/2) + (n_i + n_k) * (l_jV + s_u) + \
                              (n_j - (n_i + n_k)) * l_jV
                    else:
                        # print("What happend here?")
                        # breakpoint()
                        intersection_type = 5
            else:
                # if two are straight one (k) goes alone and one (j) goes with i
                if j_horizontal:
                    if n_i <= n_j:
                        l_u = n_i * (s_u / 2 + l_iV / 2) + n_i * (l_jH + s_u) + (l_jH * (n_j - n_i))  # n_i <= n_j
                    else:
                        l_u = n_i * (s_u / 2 + l_iV / 2) + (n_i - n_j) * (l_jH / 2 + s_u / 2) + n_j * (l_jH + s_u)
                        # n_i > n_j
                else:
                    if n_i <= n_j:
                        l_u = n_i * (s_u / 2 + l_iH / 2) + n_i * (l_jV + s_u) + (l_jV * (n_j - n_i))  # n_i <= n_j
                    else:
                        l_u = n_i * (s_u / 2 + l_iH / 2) + (n_i - n_j) * (l_jV / 2 + s_u / 2) + n_j * (l_jV + s_u)
                        # n_i > n_j
                if k_horizontal:
                    l_u += s_u + n_k * l_kH
                else:
                    l_u += s_u + n_k * l_kV

        elif intersection_type == 5:
            pass
        else:
            breakpoint()
            raise ValueError

        if intersection_type == 5:
            # Einzelberechnung

            for stream in self.streams:

                n_u += stream.n

                if stream.straight:
                    if stream.horizontal:
                        l_u += stream.n * (s_u + stream.size[0])
                        # s_u + (stream.n * stream.size[0])
                    else:
                        l_u += stream.n * (s_u + stream.size[1])
                        # s_u + (stream.n * stream.size[1])
                else:
                    l_u += stream.n * (s_u + ((stream.size[0] + stream.size[1]) / 2))
                    # todo: check if generally valid

        self.throughput_is = math.floor(n_u * ((v_u / l_u) * 3600))

    def reduce_virtual_amount(self) -> None:

        # method is not used anymore!!! new method for reduction in optilay.evaluation.throughput

        for stream in self.streams:
            new_amount = math.floor((self.throughput_is * (stream.amount / self.throughput_plan)) / 10) * 10

            for pac_stream in stream.packet_streams:
                goods = pac_stream.get_goods()

                if len(goods) > 1:
                    # breakpoint()
                    pass

                for good in goods:
                    # reduce amount and round to nearest 10
                    good_amount = math.floor((new_amount * (pac_stream._packets[good] / stream.amount)) / 10) * 10
                    pac_stream.set_goods_amount(good, good_amount)  # overwrite good_amount with reduced amount

            stream.amount = new_amount

        self.throughput_plan = self.throughput_is
