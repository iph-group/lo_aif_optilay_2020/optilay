from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.conveyor_system_layout import ConveyorSystemLayout


class ComponentCosts:
    """
    This is an example evaluation class.
    The Evaluator initialises the class with the LayoutModel and then calls the start_evaluation method.
    Otherwise, the class can be freely modified
    """

    def __init__(self, layout: ConveyorSystemLayout) -> None:
        """

        :param layout: LayoutModel that is to be evaluated.
        """
        self.layout = layout

    def start_evaluation(self) -> EvaluationResult:
        """
        The evaluation with this method is carried out here
        :return:
        """

        sum_component_costs = 0

        # iterate over all conveyors
        for conveyor in self.layout.conveyors:
            sum_component_costs += conveyor.component_costs

        return sum_component_costs
