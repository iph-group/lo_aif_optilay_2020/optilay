from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.evaluation.util_graph.graph_generation import LayoutGraph


class BufferCapacity:
    """
    This is an example evaluation class.
    The Evaluator initialises the class with the LayoutModel and then calls the start_evaluation method.
    Otherwise, the class can be freely modified
    """

    def __init__(self, layout: ConveyorSystemLayout, graph: LayoutGraph) -> None:
        """

        :param layout: LayoutModel that is to be evaluated.
        """
        # self.layout_model = layout_model
        self.layout = layout
        self.graph = graph

    def start_evaluation(self) -> int:
        """
        The evaluation with this method is carried out here
        :return:
        """

        # sum up vertices that are:
        # - connected to min 1 system_connection(sink/source)
        # - not in paths of packet_stream!
        return self.graph.get_buffer_amount(self.layout.packet_stream,
                                            self.layout.conveyors,
                                            self.layout.connection)
