from optilay.evaluation.evaluation_result import EvaluationResult
# from optilay.model.layout_model import LayoutModel
# from optilay.evaluation.throughput import Throughput
from optilay.evaluation.util_graph.graph_generation import LayoutGraph
from optilay.model.conveyor_system_layout import ConveyorSystemLayout

class LeadTime:
    """
    This is an example evaluation class.
    The Evaluator initialises the class with the LayoutModel and then calls the start_evaluation method.
    Otherwise, the class can be freely modified
    """

    def __init__(self, layout: ConveyorSystemLayout) -> None:  # layout_model: LayoutModel, graph: LayoutGraph) -> None:
        """

        :param layout: LayoutModel that is to be evaluated.
        """
        # self.layout_model = layout_model
        self.layout = layout  # layout_model.layout
        # self.graph = graph

    def start_evaluation(self, eval_all=False) -> EvaluationResult:
        """
        The evaluation with this method is carried out here
        :return:
        """

        # get throughput
        """
        if self.layout_model.get_weighting().throughput > 0 or eval_all:
            throughput = self.layout.evaluation.throughput[0]
        else:
            throughput = Throughput(self.layout, self.graph).start_evaluation()
        """
        throughput = self.layout.evaluation.throughput[0]

        # get amount of all goods
        good_amounts = self.layout.get_all_good_amounts()

        # little`s law: lead_time = good amount / throughput
        # https://de.wikipedia.org/wiki/Durchlaufzeit#Gesetz_von_John_D._C._Little
        if throughput > 0:
            lead_time = good_amounts / throughput
        else:
            lead_time = 10000000  # big m

        if lead_time < 0:
            breakpoint()

        return lead_time
