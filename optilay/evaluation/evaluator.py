from optilay.evaluation.evaluation_method import EvaluationMethod
from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.evaluation.example_eval_method import MethodA
from optilay.evaluation.throughput import Throughput
from optilay.evaluation.lead_time import LeadTime
from optilay.evaluation.space_requirement import SpaceRequirement
from optilay.evaluation.buffer_capacity import BufferCapacity
from optilay.evaluation.component_costs import ComponentCosts
from optilay.model.layout_model import LayoutModel
from optilay.evaluation.util_graph.graph_generation import LayoutGraph
import copy
import random


class Evaluator:
    """
    The Evaluator class is there to evaluate the LayoutModel. The class can be freely modified to keep certain
    information persistent over different evaluations. (The class is not saved, but lives only as long as it is not
    being reset or the program is not terminated).
    """

    def __init__(self) -> None:
        """

        """
        pass

    def reset(self) -> None:
        """
        Resets all persistent information
        :return: None
        """
        # raise NotImplementedError
        return

    def evaluate(self, layout_model: LayoutModel, eval_all=False) -> None:
        """
        This method evaluates the given LayoutModel, depending on the evaluation method specified in the
        ControlParameter.
        :param layout_model: The LayoutModel to be evaluated.
        :param eval_all: indicates if all objectives should be evaluated
        :return: The EvaluationResult of the evaluation.
        """

        layout = layout_model.layout

        if layout_model.get_weighting().throughput > 0 or \
                layout_model.get_weighting().lead_time > 0 or \
                layout_model.get_weighting().buffer_capacity > 0 or \
                eval_all:
            graph = LayoutGraph(layout)
            # graph.plot_graph()
            path_list = graph.get_paths(layout.packet_stream, write_to_packet_streams=True)

            # store current path in list
            curr_paths = []
            for entry in path_list:
                curr_paths.append(entry[0])

        if layout_model.get_weighting().throughput > 0 or layout_model.get_weighting().lead_time > 0 or eval_all:
            best_throughput = 0
            path_counter = 10
            best_paths = []
            while path_counter > 0:
                # eval throughput with current_path
                curr_throughput = Throughput(layout, graph).start_evaluation()

                # check if best throughput -> if true store result and path
                if curr_throughput > best_throughput:
                    best_throughput = curr_throughput
                    best_paths = curr_paths

                # set new / other path
                curr_paths = []
                for pac_str in range(len(path_list)):
                    path_number = random.randint(0, len(path_list[pac_str]) - 1)
                    layout.packet_stream[pac_str].add_path(path_list[pac_str][path_number])
                    curr_paths.append(path_list[pac_str][path_number])

                path_counter -= 1
            else:
                layout.evaluation.throughput[0] = best_throughput
                graph.convert_paths_from_vertex_to_cell(layout, best_paths)
                # todo: paths should be part of decision variables (for example as part of chromosome in GA)

        if layout_model.get_weighting().lead_time > 0 or eval_all:
            layout.evaluation.lead_time[0] = LeadTime(layout).start_evaluation(eval_all)
            # layout.evaluation.lead_time[0] = LeadTime(layout_model, graph).start_evaluation(eval_all)

        if layout_model.get_weighting().space_requirement > 0 or eval_all:
            layout.evaluation.space_requirement[0] = SpaceRequirement(layout).start_evaluation()

        if layout_model.get_weighting().buffer_capacity > 0 or eval_all:
            layout.evaluation.buffer_capacity[0] = BufferCapacity(layout, graph).start_evaluation()

        if layout_model.get_weighting().costs > 0 or eval_all:
            layout.evaluation.costs[0] = ComponentCosts(layout).start_evaluation()

        """
        if layout_model.get_parameter().eval_method == EvaluationMethod.Throughput:
            layout.evaluation.throughput[0] = Throughput(layout).start_evaluation()
            # return Throughput(layout_model.layout).start_evaluation()
        # todo: elif layout_model.get_parameter().eval_method == EvaluationMethod.LeadTime:
        # return LeadTime(layout_model.layout).start_evaluation()

        elif layout_model.get_parameter().eval_method == EvaluationMethod.SpaceRequirement:
            layout.evaluation.space_requirement[0] = SpaceRequirement(layout).start_evaluation()
            # return SpaceRequirement(layout_model.layout).start_evaluation()

        elif layout_model.get_parameter().eval_method == EvaluationMethod.BufferCapacity:
            layout.evaluation.buffer_capacity[0] = BufferCapacity(layout).start_evaluation()
            # return BufferCapacity(layout_model.layout).start_evaluation()

        elif layout_model.get_parameter().eval_method == EvaluationMethod.ComponentCosts:
            layout.evaluation.costs[0] = ComponentCosts(layout).start_evaluation()
            # return ComponentCosts(layout_model.layout).start_evaluation()

        else:
            raise ValueError('Unknown EvaluationMethod')
        """
        # update best known result
        self.result_updater(layout_model, update_all=eval_all)

    def result_updater(self, layout_model: LayoutModel, update_all=False, idx=None) -> None:
        """

        :param layout_model: The LayoutModel to be updated.
        :param update_all: indicates if all objectives should be updated
        :return:
        """
        if idx:
            layout = layout_model.layouts.get_layout(idx)
        else:
            # get current layout
            layout = layout_model.layout

        layout.evaluation.layout_id = layout.layout_id

        # start eval methods
        if layout_model.get_weighting().throughput > 0 or update_all:
            # maximise throughput
            if layout.evaluation.throughput[0] >= layout_model.best_result.throughput[0]:
                layout.evaluation.throughput[1] = 100
                layout_model.best_result.throughput[0] = copy.copy(layout.evaluation.throughput[0])
            else:
                layout.evaluation.throughput[1] = (layout.evaluation.throughput[0] /
                                                   layout_model.best_result.throughput[0]) * 100

        if layout_model.get_weighting().lead_time > 0 or update_all:
            # minimise lead_time
            if layout.evaluation.lead_time[0] <= layout_model.best_result.lead_time[0]:
                layout.evaluation.lead_time[1] = 100
                layout_model.best_result.lead_time[0] = copy.copy(layout.evaluation.lead_time[0])
            else:
                layout.evaluation.lead_time[1] = (layout_model.best_result.lead_time[0] /
                                                  layout.evaluation.lead_time[0]) * 100

        if layout_model.get_weighting().space_requirement > 0 or update_all:
            # minimise space requirement
            if layout.evaluation.space_requirement[0] <= layout_model.best_result.space_requirement[0]:
                layout.evaluation.space_requirement[1] = 100
                layout_model.best_result.space_requirement[0] = copy.copy(layout.evaluation.space_requirement[0])
            else:
                if layout.evaluation.space_requirement[0] == 0:
                    layout.evaluation.space_requirement[1] = 99.87654321
                else:
                    layout.evaluation.space_requirement[1] = (layout_model.best_result.space_requirement[0] /
                                                              layout.evaluation.space_requirement[0]) * 100

        if layout_model.get_weighting().buffer_capacity > 0 or update_all:
            # maximise buffer capacity
            if layout.evaluation.buffer_capacity[0] >= layout_model.best_result.buffer_capacity[0]:
                layout.evaluation.buffer_capacity[1] = 100
                layout_model.best_result.buffer_capacity[0] = copy.copy(layout.evaluation.buffer_capacity[0])
            else:
                layout.evaluation.buffer_capacity[1] = (layout.evaluation.buffer_capacity[0] /
                                                        layout_model.best_result.buffer_capacity[0]) * 100

        if layout_model.get_weighting().costs > 0 or update_all:
            # minimise component costs
            if layout.evaluation.costs[0] <= layout_model.best_result.costs[0]:
                layout.evaluation.costs[1] = 100
                layout_model.best_result.costs[0] = copy.copy(layout.evaluation.costs[0])
            else:
                if layout.evaluation.costs[0] == 0:
                    layout.evaluation.costs[1] = 99.87654321
                else:
                    layout.evaluation.costs[1] = (layout_model.best_result.costs[0] / layout.evaluation.costs[0]) * 100

        # calc weight_sum
        layout.evaluation.weighted_sum = layout.evaluation.throughput[
                                             1] * layout_model.get_weighting().throughput + \
                                         layout.evaluation.lead_time[
                                             1] * layout_model.get_weighting().lead_time + \
                                         layout.evaluation.space_requirement[1] * \
                                         layout_model.get_weighting().space_requirement + \
                                         layout.evaluation.buffer_capacity[1] * \
                                         layout_model.get_weighting().buffer_capacity + \
                                         layout.evaluation.costs[1] * layout_model.get_weighting().costs

    def result_updater_layoutdb(self, layout_model: LayoutModel, update_all) -> None:
        """

        :param layout_model: The LayoutModel to be updated.
        :param update_all: indicates if all objectives should be updated
        :return:
        """

        # load all layouts from db
        layouts = layout_model.layouts.get_all_layouts()

        # iterate over layouts
        for db_instance in layouts:

            if layout_model.get_weighting().throughput > 0 or update_all:
                # maximise throughput
                if db_instance.evaluation.throughput[0] >= layout_model.best_result.throughput[0]:
                    db_instance.evaluation.throughput[1] = 100
                else:
                    db_instance.evaluation.throughput[1] = (db_instance.evaluation.throughput[0] /
                                                            layout_model.best_result.throughput[0]) * 100

            if layout_model.get_weighting().lead_time > 0 or update_all:
                # minimise lead_time
                if db_instance.evaluation.lead_time[0] <= layout_model.best_result.lead_time[0]:
                    db_instance.evaluation.lead_time[1] = 100
                else:
                    db_instance.evaluation.lead_time[1] = (layout_model.best_result.lead_time[0] /
                                                           db_instance.evaluation.lead_time[0]) * 100

            if layout_model.get_weighting().space_requirement > 0 or update_all:
                # minimise space requirement
                if db_instance.evaluation.space_requirement[0] <= layout_model.best_result.space_requirement[0]:
                    db_instance.evaluation.space_requirement[1] = 100
                else:
                    db_instance.evaluation.space_requirement[1] = (layout_model.best_result.space_requirement[0] /
                                                                   db_instance.evaluation.space_requirement[0]) * 100

            if layout_model.get_weighting().buffer_capacity > 0 or update_all:
                # maximise buffer capacity
                if db_instance.evaluation.buffer_capacity[0] >= layout_model.best_result.buffer_capacity[0]:
                    db_instance.evaluation.buffer_capacity[1] = 100
                else:
                    db_instance.evaluation.buffer_capacity[1] = (db_instance.evaluation.buffer_capacity[0] /
                                                                 layout_model.best_result.buffer_capacity[0]) * 100

            if layout_model.get_weighting().costs > 0 or update_all:
                # minimise component costs
                if db_instance.evaluation.costs[0] <= layout_model.best_result.costs[0]:
                    db_instance.evaluation.costs[1] = 100
                else:
                    db_instance.evaluation.costs[1] = (layout_model.best_result.costs[0] /
                                                       db_instance.evaluation.costs[0]) * 100

            # calc weight_sum
            db_instance.evaluation.weighted_sum = db_instance.evaluation.throughput[1] * \
                                                  layout_model.get_weighting().throughput + \
                                                  db_instance.evaluation.lead_time[1] * \
                                                  layout_model.get_weighting().lead_time + \
                                                  db_instance.evaluation.space_requirement[1] * \
                                                  layout_model.get_weighting().space_requirement + \
                                                  db_instance.evaluation.buffer_capacity[1] * \
                                                  layout_model.get_weighting().buffer_capacity + \
                                                  db_instance.evaluation.costs[1] * layout_model.get_weighting().costs

    def evaluate_all_objectives_layoutdb(self, layout_model: LayoutModel):
        """

        :param layout_model:
        :return:
        """
        # get layouts from layout_db
        layouts = layout_model.layouts.get_all_layouts()
        # iterate over all layouts
        for db_instance in layouts:
            layout_model.load_layout(db_instance.layout_id)  # get layout as current_layout
            self.evaluate(layout_model, eval_all=True)  # eval
            layout_model.layouts.add_layout(layout_model.get_current_layout(), overwrite=True)  # save changes
