from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.conveyor_system_layout import ConveyorSystemLayout


class SpaceRequirement:
    """
    This is an example evaluation class.
    The Evaluator initialises the class with the LayoutModel and then calls the start_evaluation method.
    Otherwise, the class can be freely modified
    """

    def __init__(self, layout: ConveyorSystemLayout) -> None:
        """

        :param layout: LayoutModel that is to be evaluated.
        """
        self.layout = layout

    def start_evaluation(self) -> EvaluationResult:
        """
        The evaluation with this method is carried out here
        :return:
        """

        #####################################################################
        # variant 1

        # get amount of objects on cells
        """
        num_layout_cells = (self.layout.layout.shape[0] - 1) * (self.layout.layout.shape[1] - 1)
        num_conveyor = len(self.layout.conveyors)
        num_workplace = len(self.layout.workplaces)
        num_restricted = len(self.layout.res_areas)
        num_deactivated = len(self.layout.deactivated)
        # calc amount of unused cells
        num_used_cells = num_conveyor + num_workplace + num_restricted + num_deactivated
        # calc evaluation value
        space_requirement = num_used_cells  # / num_layout_cells
        """
        #####################################################################
        # variant 2

        # init most distanced point
        cell_northernmost_point = 0
        cell_easternmost_point = self.layout.layout.shape[1]
        cell_southernmost_point = self.layout.layout.shape[0]
        cell_westernmost_point = 0

        # init conveyor list
        conveyors = self.layout.conveyors

        # iterate over conveyors and save outer points
        for conveyor in conveyors:
            if conveyor.y < cell_northernmost_point:
                cell_northernmost_point = conveyor.y
            if conveyor.y > cell_southernmost_point:
                cell_southernmost_point = conveyor.y
            if conveyor.x < cell_easternmost_point:
                cell_easternmost_point = conveyor.x
            if conveyor.x > cell_westernmost_point:
                cell_westernmost_point = conveyor.x

        # calc surface area
        distance_y = cell_southernmost_point - cell_northernmost_point
        distance_x = cell_westernmost_point - cell_easternmost_point

        space_requirement = distance_y * distance_x

        return space_requirement
