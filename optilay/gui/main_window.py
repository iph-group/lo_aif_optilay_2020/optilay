import logging
import multiprocessing
import os
import webbrowser

from PySide6.QtCore import QThread
from PySide6.QtGui import Qt, QIcon
from PySide6.QtWidgets import QMainWindow, QSplitter, QMessageBox, QFileDialog

from optilay.gui.all_layouts_list_dialog import AllLayoutsListDialog
from optilay.gui.calculation_progress_dialog import CalculationProgressDialog
from optilay.gui.control_parameter_dialog import ControlParameterDialog
from optilay.gui.conveyor_types_pre_optimisation import ConveyorTypesPreOptimisation
from optilay.gui.create_goods_dialog import CreateGoodsDialog
from optilay.gui.disable_cells_dialog import DisableCellsDialog
from optilay.gui.generated.main_window_gen import Ui_MainWindow
from optilay.gui.layout_comparison_dialog import LayoutComparisonDialog
from optilay.gui.layout_detail_dialog import LayoutDetailDialog
from optilay.gui.layout_scene import LayoutScene
from optilay.gui.layout_view import LayoutView
from optilay.gui.raw_data_table_dialog import RawDataTableDialog
from optilay.gui.result_table_dialog import EvaluationTableDialog
from optilay.gui.sidebar import Sidebar
from optilay.gui.size_dialog import SizeDialog
from optilay.gui.start_dialog import StartDialog
from optilay.gui.util.calculation_progress_receiver import CalculationProgressReceiver
from optilay.gui.util.fixed_gui_values import CELL_ZOOM
from optilay.gui.util.layout_mode import LayoutMode
from optilay.gui.util.license_dialog import LicenseDialog
from optilay.gui.util.write_stream import WriteStream
from optilay.gui.weighting_dialog import WeightingDialog
from optilay.io.io_type import IOType
from optilay.main_system import MainSystem
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.util.cell_size import CellSize
from optilay.util.exceptions import NoPathException


class MainWindow(QMainWindow):
    """
    This is the main GUI window from Optilay.
    """

    def __init__(self, system) -> None:
        """

        :param system: The MainSystem as the connection of the GUI to the Backend.
        """
        super().__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.system: MainSystem = system

        self.open_start_dialog()

        self.layout_scene: LayoutScene = LayoutScene(system.get_layout().get_current_layout(),
                                                     mode=LayoutMode.ADD_CONVEYOR)
        self.layout_view: LayoutView = LayoutView(self.layout_scene)
        self.layout_view.fitInView(0, 0,
                                   (
                                           self.layout_scene.conv_layout.length / self.layout_scene.conv_layout._cell_size.value) * CELL_ZOOM,
                                   (
                                           self.layout_scene.conv_layout.width / self.layout_scene.conv_layout._cell_size.value) * CELL_ZOOM,
                                   Qt.KeepAspectRatio)

        self.splitter = QSplitter(parent=self.ui.centralwidget)
        self.sidebar = Sidebar(self.system.get_layout())
        self.ui.horizontalLayout_2.addWidget(self.splitter)
        self.splitter.addWidget(self.sidebar)
        self.splitter.addWidget(self.layout_view)

        self.splitter.setSizes([100, 300])

        self.ui.actionNew.triggered.connect(self.new_layout_action)
        self.ui.actionLoad.triggered.connect(self.load_layout_action)
        self.ui.actionSave.triggered.connect(self.save_action)
        self.ui.actionSave_as.triggered.connect(self.save_as_action)
        self.ui.actionAML_Export.triggered.connect(self.aml_export_action)
        self.ui.actionAML_Import.setEnabled(False)
        self.ui.actionAML_Import.triggered.connect(self.aml_import_action)
        self.ui.actionAML_Export.setEnabled(False)
        self.ui.actionQuit.triggered.connect(self.quits)
        self.ui.actionControl_parameters.triggered.connect(self.open_control_parameter_dialog)
        self.ui.actionWeighting.triggered.connect(self.open_weighting_dialog)
        self.ui.actionResult_table.triggered.connect(self.open_evaluation_table_dialog)
        self.ui.actionAll_layouts.triggered.connect(self.open_all_layouts_list_dialog)
        self.ui.actionLayout_comparison.triggered.connect(self.open_layout_comparison_dialog)
        self.ui.actionRaw_data.triggered.connect(self.open_raw_data_table_dialog)
        self.ui.actionConveyor_selection.triggered.connect(self.open_conveyor_types_pre_optimisation)
        self.ui.actionLicense.triggered.connect(self.open_license_dialog)
        self.ui.actionAdd_goods.triggered.connect(self.open_create_goods_dialog)
        self.ui.actionInstructions.triggered.connect(self.open_instruction_dialog)

        self.ui.pushButton_evaluate.clicked.connect(self.evaluate_action)
        self.ui.pushButton_optimise.clicked.connect(self.optimise_action)
        self.ui.pushButton_reset.clicked.connect(self.reset_layout_action)

        self.application_path = os.path.dirname(os.path.abspath(__file__))

        icon = "../resources/IPH_Logo_jpg_web.ico"
        self.setWindowIcon(QIcon(os.path.join(self.application_path, icon)))

        self.layout_scene.init_grid()

        logging.info("MainWindow started successfully")

    def refresh(self) -> None:
        """
        This method refreshes all contents of the GUI if something has changed in the backend.
        :return: None
        """
        self.sidebar.clear_available_conveyors_table()
        self.sidebar.clear_available_goods_table()
        self.sidebar.set_available_conveyors_table()
        self.sidebar.set_available_goods_table()
        self.layout_scene.conv_layout = self.system.get_layout().get_current_layout()
        self.layout_scene.reset()
        # self.layout_view.fitInView(0, 0,
        #                            (
        #                                    self.layout_scene.conv_layout.length / self.layout_scene.conv_layout._cell_size.value) * CELL_ZOOM,
        #                            (
        #                                    self.layout_scene.conv_layout.width / self.layout_scene.conv_layout._cell_size.value) * CELL_ZOOM,
        #                            Qt.KeepAspectRatio)

    def new_layout_action(self) -> None:
        """
        This method connects the user's command to create a new LayoutModel with the appropriate method in the
        MainSystem.
        :return: None
        """
        self.open_size_dialog()
        self.refresh()

    def load_layout_action(self) -> None:
        """
        This method connects the user's command to load a LayoutModel with the appropriate method in the MainSystem.
        After the user has selected the file to be loaded in the QFileDialog.
        :return: None
        """
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open File', '.', "JSON file (*.json)")
            if fname[0]:
                self.system.load_layout(fname[0])
        except Exception as e:
            self.open_error_dialog("An error occured during the file loading.", str(e), QMessageBox.Icon.Critical)
        finally:
            self.refresh()

    def save_action(self) -> None:
        """
        This method connects the user's command to save the LayoutModel with the appropriate method in the MainSystem.
        When the user saves the LayoutModel for the first time, he is redirected to the save_as_action method.
        :return: None
        """
        try:
            self.system.save(IOType.JSON, gui=True)
        except NoPathException as npe:
            self.save_as_action()
        except Exception as e:
            self.open_error_dialog("An error occured during saving the file.", str(e), QMessageBox.Icon.Critical)

    def save_as_action(self) -> None:
        """
        This method connects the user's command to save the LayoutModel at a specific location with the appropriate
        method in the MainSystem. In the QFileDialog the user selects the location.
        :return: None
        """
        try:
            fname = QFileDialog.getSaveFileName(caption="Save as JSON", filter='*.json')
            if fname[0] != '':
                self.system.save_as(IOType.JSON, fname[0], gui=True)
        except Exception as e:
            self.open_error_dialog("An error occured during saving the file.", str(e), QMessageBox.Icon.Critical)

    def aml_import_action(self) -> None:
        """
        This method connects the user's command to import the LayoutModel as AML with the appropriate method in the
        MainSystem. The user has to select the file to import in the QFileDialog.
        :return: None
        """
        try:
            fname = QFileDialog.getOpenFileName(self, 'Import AML', '.', "AML file (*.aml)")
            if fname[0]:
                self.system.load_layout(fname[0])
        except Exception as e:
            self.open_error_dialog("An error occured during importing the AML file.", str(e), QMessageBox.Icon.Critical)
        finally:
            self.refresh()

    def aml_export_action(self) -> None:
        """
        This method connects the user's command to export the LayoutModel as AML with the appropriate method in the
        MainSystem. The user must select the location in the QFileDialog.
        :return: None
        """
        try:
            fname = QFileDialog.getSaveFileName(caption="Export as AML", filter='*.aml')
            if fname[0] != '':
                self.system.save_as(IOType.AML, fname[0], gui=True)
        except Exception as e:
            self.open_error_dialog("An error occured during exporting the AML file.", str(e), QMessageBox.Icon.Critical)

    def quits(self) -> None:
        """
        Quits the program.
        :return: None
        """
        exit()

    def reset_layout_action(self) -> None:
        """
        This method connects the user's command to reset the LayoutModel with the appropriate method in the MainSystem.
        A warning dialogue is displayed before execution to prevent careless resetting.
        :return: None
        """
        errordialog = QMessageBox(self)
        errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
        errordialog.setText(self.tr("Do you really want to reset the current layout?"))
        errordialog.setInformativeText(self.tr("Everything will be deleted!"))
        errordialog.setIcon(QMessageBox.Warning)
        errordialog.show()
        if errordialog.exec_() == QMessageBox.Ok:
            self.system.reset_layout()
            self.refresh()

    def evaluate_action(self) -> None:
        """
        This method connects the user's command to evaluate the LayoutModel with the appropriate method in the
        MainSystem. The weighting is queried again beforehand and the result is displayed afterwards.
        :return: None
        """
        if self.open_weighting_dialog():
            self.system.evaluate()
            self.open_evaluation_table_dialog()
            self.refresh()

    def optimise_action(self) -> None:
        """
        This method connects the user's command to optimise the LayoutModel with the appropriate method in the
        MainSystem. Before the weighting and the control parameters are queried again and after the optimisation the
        best results are displayed for comparison.
        During the optimisation, a progress bar and the outputs of the optimisation are displayed in a separate window.
        :return: None
        """
        if not self.open_weighting_dialog():
            return
        if not self.open_conveyor_types_pre_optimisation():
            return
        msg_box = QMessageBox()
        msg_box.setWindowTitle("Optimisation Setting")
        msg_box.setText("Should the current settings be used for optimisation?")
        msg_box.setStandardButtons(QMessageBox.Yes)
        msg_box.addButton(QMessageBox.No)
        msg_box.setDefaultButton(QMessageBox.Yes)
        if msg_box.exec() == QMessageBox.No and not self.open_control_parameter_dialog():
            return

        param = self.system.get_layout().get_parameter()

        queue = multiprocessing.Queue()
        error_queue = multiprocessing.Queue()
        out_stream = WriteStream(queue)
        calculation_progress_dialog = CalculationProgressDialog(param.worker)
        calculation_progress_dialog.show()

        thread = QThread()
        calculation_progress_receiver = CalculationProgressReceiver(queue, param.worker)
        calculation_progress_receiver.new_message_signal.connect(calculation_progress_dialog.append_text)
        calculation_progress_receiver.moveToThread(thread)
        thread.started.connect(calculation_progress_receiver.run)
        thread.start()

        pid_list = []

        result_queue = self.system.optimise(out_stream, error_queue, gui_pid_list=pid_list)

        if not calculation_progress_dialog.exec():
            for p in pid_list:
                out_stream.write("Done!")
                p.terminate()
                logging.error("Terminated process: {}".format(p.pid))
            thread.quit()
            thread.wait()
            return
        else:
            for p in pid_list:
                while not result_queue.empty():
                    self.system.get_layout().get_db().merge_db(result_queue.get())
                p.join()
                logging.info("Process {} joined.".format(p.pid))
            thread.quit()
            thread.wait()

        while not error_queue.empty():
            self.open_error_dialog("An error occured during optimisation!", str(error_queue.get()),
                                   QMessageBox.Icon.Critical)
        # TODO: open only if there is any result
        self.open_layout_comparison_dialog()

        self.refresh()

    def open_control_parameter_dialog(self) -> int:
        """
        Opens the ControlParameterDialog to set the ControlParameters.
        :return: The status with which the ControlParameterDialog was closed.
        """
        control_parameter_dialog = ControlParameterDialog(self.system.get_layout().get_parameter())
        control_parameter_dialog.setModal(True)
        control_parameter_dialog.show()
        result = control_parameter_dialog.exec()
        self.refresh()
        return result

    def open_weighting_dialog(self) -> int:
        """
        Opens the WeightingDialog to set the weighting.
        :return: The status with which the WeightingDialog was closed.
        """
        weighting_dialog = WeightingDialog(self.system.get_layout().get_weighting())
        weighting_dialog.setModal(True)
        weighting_dialog.show()
        result = weighting_dialog.exec()
        self.refresh()
        return result

    def open_evaluation_table_dialog(self) -> None:
        """
        Opens the EvaluationTableDialog to show the results after an evaluation.
        :return:
        """
        layout = self.system.get_layout().get_current_layout()
        best_result = self.system.get_layout().best_result
        result_table_dialog = EvaluationTableDialog(layout.evaluation,
                                                    best_result,
                                                    self.system.get_layout().get_weighting())

        result_table_dialog.setModal(True)
        result_table_dialog.show()
        result_table_dialog.exec()
        self.refresh()

    def open_all_layouts_list_dialog(self) -> None:
        """
        Opens the AllLayoutsListDialog to show all available ConveyorSystemLayout.
        :return: None
        """
        all_layout_list_dialog = AllLayoutsListDialog(self.system.get_layout())
        all_layout_list_dialog.setModal(True)
        all_layout_list_dialog.show()
        if all_layout_list_dialog.exec():
            index = int(all_layout_list_dialog.ui.listWidget.currentItem().text().split()[1][:-1])
            layout_detail_dialog = LayoutDetailDialog(self.system.get_layout(),
                                                      self.system.get_layout().get_db().get_layout(index))
            layout_detail_dialog.show()
            layout_detail_dialog.fit_view()
            if layout_detail_dialog.exec():
                self.refresh()
            else:
                self.open_all_layouts_list_dialog()

    def open_layout_comparison_dialog(self) -> None:
        """
        Opens the LayoutComparisonDialog to compare the best ConveyorSystemLayout.
        :return: None
        """
        layout_comparison_dialog = LayoutComparisonDialog(self.system.get_layout())
        layout_comparison_dialog.setModal(True)
        layout_comparison_dialog.show()
        layout_comparison_dialog.exec()
        self.refresh()

    def open_error_dialog(self, title, text, icon) -> None:
        """
        Opens a QMessageBox to show the user warnings and errors.
        :param title: Title of the warning/error
        :param text: Warning/Error description
        :param icon: Icon of the warning/error
        :return: None
        """
        errordialog = QMessageBox(self)
        errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok))
        errordialog.setText(title)
        errordialog.setInformativeText(text)
        errordialog.setIcon(icon)
        errordialog.show()
        errordialog.exec()

    def open_start_dialog(self) -> None:
        """
        Opens the StartDialog. There the user can start a new project or load a existing one.
        :return: None
        """
        start_dialog = StartDialog(self)
        start_dialog.show()
        if start_dialog.exec():
            if start_dialog.new:
                self.open_size_dialog()
            else:
                fname = QFileDialog.getOpenFileName(self, 'Open File', '.', "JSON (*.json);; AML (*.aml)")
                if fname[0]:
                    logging.info("Loading File {}".format(fname[0]))
                    self.system.load_layout(fname[0])
                else:
                    self.open_start_dialog()
        else:
            quit()

    def open_size_dialog(self) -> None:
        """
        Opens a dialog where the user can choose the layout size.
        :return: None
        """
        size_dialog = SizeDialog(self.system.get_layout().get_current_layout())
        size_dialog.show()
        if not size_dialog.exec():
            self.open_start_dialog()
        else:
            self.system.new_layout(size_dialog.ui.doubleSpinBox_factory_length.value(),
                                   size_dialog.ui.doubleSpinBox_factory_width.value(),
                                   CellSize(int(size_dialog.ui.comboBox_cell_size.currentText())))
            self.open_disable_cells_dialog(self.system.get_layout().get_current_layout())
            self.open_instruction_dialog()

    def open_disable_cells_dialog(self, layout: ConveyorSystemLayout) -> None:
        """

        :return:
        """
        disable_cells_dialog = DisableCellsDialog(layout)
        disable_cells_dialog.show()
        disable_cells_dialog.fit_view()
        if not disable_cells_dialog.exec():
            self.open_size_dialog()

    def open_raw_data_table_dialog(self) -> None:
        raw_data_table_dialog = RawDataTableDialog(self.system.get_layout().get_current_layout())
        raw_data_table_dialog.setModal(True)
        raw_data_table_dialog.show()
        raw_data_table_dialog.exec()
        self.refresh()

    def open_create_goods_dialog(self) -> None:
        reate_goods_dialog = CreateGoodsDialog(self.system.get_layout().get_goods_factory())
        reate_goods_dialog.setModal(True)
        reate_goods_dialog.show()
        reate_goods_dialog.exec()
        self.refresh()

    def open_conveyor_types_pre_optimisation(self) -> int:
        cov_typ_pre_opt_diag = ConveyorTypesPreOptimisation(
            self.system.get_layout().get_current_layout().get_available_conveyor(),
            self.system.get_layout().get_parameter())
        cov_typ_pre_opt_diag.setModal(True)
        cov_typ_pre_opt_diag.show()
        code = cov_typ_pre_opt_diag.exec()
        if code:
            self.sidebar.clear_available_conveyors_table()
            self.sidebar.set_available_conveyors_table()
        return code

    def open_license_dialog(self) -> int:
        license_dialog = LicenseDialog()
        license_dialog.setModal(True)
        license_dialog.show()
        return license_dialog.exec_()

    def open_instruction_dialog(self) -> int:
        application_path = os.path.dirname(os.path.abspath(__file__))
        return webbrowser.open_new('file://' + os.path.join(application_path, '../../README.html'))
