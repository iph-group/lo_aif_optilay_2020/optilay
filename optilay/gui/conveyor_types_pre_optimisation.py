from typing import List

from PySide6.QtWidgets import QVBoxLayout, QDialog, QCheckBox, QMessageBox, QDialogButtonBox

from optilay.model.control_parameter import ControlParameter
from optilay.model.conveyor.conveyor_type import ConveyorType


class ConveyorTypesPreOptimisation(QDialog):
    """

    """

    def __init__(self, list_avail_conv: List[str], control_parameter: ControlParameter, *args, **kwargs):
        """

        :param control_parameter:
        :type control_parameter:
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.control = control_parameter

        self.setWindowTitle("Select Conveyor types for optimisation")
        self.resize(400, 150)

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.check_boxes = []



        for conv_type in list_avail_conv:
            box = QCheckBox(conv_type)
            if conv_type in [conv for conv in self.control.conv_types]:
                box.setChecked(True)
            else:
                box.setChecked(False)
            layout.addWidget(box)
            self.check_boxes.append(box)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        layout.addWidget(button_box)

    def accept(self) -> None:
        if not any([box.isChecked() for box in self.check_boxes]):
            errordialog = QMessageBox(self)
            errordialog.setText("No conveyor type chosen!")
            errordialog.setInformativeText("Choose a minimum of one conveyor type")
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
            return

        self.control.conv_types = []
        for box in self.check_boxes:
            if box.isChecked():
                self.control.conv_types.append(box.text())

        super().accept()

