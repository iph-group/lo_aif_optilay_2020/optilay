from PySide6.QtWidgets import QDialog, QListWidgetItem

from optilay.gui.generated.remove_goods_dialog_gen import Ui_RemoveGoodsDialog
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.sytemconnection.system_connection import SystemConnection


class RemoveGoodsDialog(QDialog):
    """

    """

    def __init__(self, syscon: SystemConnection, conv_layout: ConveyorSystemLayout) -> None:
        super().__init__()

        # loading Ui
        self.ui = Ui_RemoveGoodsDialog()
        self.ui.setupUi(self)

        self.syscon = syscon
        self.conv_layout = conv_layout
        self.used_packetstreams = []

        for stream in self.conv_layout.packet_stream:
            sink = False
            source = False
            if stream._sink == syscon:
                sink = True
            elif stream._source == syscon:
                source = True
            if sink or source:
                self.used_packetstreams.append(stream)
                for good, amount in stream._packets.items():
                    # +1 so that the coordiants for the user start at 1
                    if sink:
                        QListWidgetItem(
                            "[{}, {}] -> this [{}, {}], {}x {}({}g), ID: {}".format(
                                stream.source.connected_position[1] + 1,
                                stream.source.connected_position[0] + 1,
                                stream.sink.connected_position[1] + 1,
                                stream.sink.connected_position[0] + 1,
                                amount,
                                good.name, good.actual_weight, good.id),
                            self.ui.listWidget_left)
                    elif source:
                        QListWidgetItem(
                            "This [{}, {}] -> [{}, {}], {}x {}({}g), ID: {}".format(
                                stream.source.connected_position[1] + 1,
                                stream.source.connected_position[0] + 1,
                                stream.sink.connected_position[1] + 1,
                                stream.sink.connected_position[0] + 1,
                                amount,
                                good.name, good.actual_weight, good.id),
                            self.ui.listWidget_left)

        self.ui.pushButton_right.clicked.connect(self.move_right)
        self.ui.pushButton_left.clicked.connect(self.move_left)

    def move_right(self) -> None:
        """
        Moves the selected element to the right list.
        :return: None
        """
        curr_item = self.ui.listWidget_left.currentItem()
        if curr_item:
            self.ui.listWidget_right.addItem(QListWidgetItem(curr_item.text()))
            self.ui.listWidget_left.takeItem(self.ui.listWidget_left.row(curr_item))
            del curr_item

    def move_left(self) -> None:
        """
        Moves the selected element to the left list.
        :return: None
        """
        curr_item = self.ui.listWidget_right.currentItem()
        if curr_item:
            self.ui.listWidget_left.addItem(QListWidgetItem(curr_item.text()))
            self.ui.listWidget_right.takeItem(self.ui.listWidget_right.row(curr_item))
            del curr_item

    def accept(self) -> None:
        for index in range(self.ui.listWidget_right.count()):
            list_item = self.ui.listWidget_right.item(index)
            idx = int(list_item.text().split()[-1])
            self.conv_layout.remove_good_by_id(idx)

        super().accept()
