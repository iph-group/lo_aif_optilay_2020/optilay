import os

from PySide6.QtWidgets import QDialog, QMessageBox, QSpinBox

from optilay.gui.generated.control_parameter_dialog_gen import Ui_ControlParameterDialog
from optilay.model.control_parameter import ControlParameter, InitialisationMethod
from optilay.optimisation.optimisation_method import OptimisationMethod


class ControlParameterDialog(QDialog):
    """
    In the ControlParameterDialog all values in ControlParameter can be adjusted by the user.
    """

    def __init__(self, control_parameter: ControlParameter, *args, **kwargs) -> None:
        """

        :param control_parameter: ControlParameters that are displayed and changed.
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_ControlParameterDialog()
        self.ui.setupUi(self)

        self.control_parameter = control_parameter
        self.load_parameter()

        self.ui.radioButton_genetic_algorithm.toggled.connect(self.toggle_genetic)
        self.ui.radioButton_random_placement.toggled.connect(self.toggle_random_placement)

    def toggle_genetic(self):
        if self.ui.label_3.isEnabled():
            self.ui.label_3.setEnabled(False)
            self.ui.label_4.setEnabled(False)
            self.ui.label_7.setEnabled(False)
            self.ui.label_8.setEnabled(False)
            self.ui.label_9.setEnabled(False)
            self.ui.label_10.setEnabled(False)
            self.ui.doubleSpinBox_crossover_rate.setEnabled(False)
            self.ui.doubleSpinBox_mutation_rate.setEnabled(False)
            self.ui.spinBox_crossover_two_point_operator_length.setEnabled(False)
            self.ui.spinBox_maximal_number_of_mutations.setEnabled(False)
            self.ui.spinBox_number_of_generations.setEnabled(False)
            self.ui.spinBox_population_size.setEnabled(False)
        else:
            self.ui.label_3.setEnabled(True)
            self.ui.label_4.setEnabled(True)
            self.ui.label_7.setEnabled(True)
            self.ui.label_8.setEnabled(True)
            self.ui.label_9.setEnabled(True)
            self.ui.label_10.setEnabled(True)
            self.ui.doubleSpinBox_crossover_rate.setEnabled(True)
            self.ui.doubleSpinBox_mutation_rate.setEnabled(True)
            self.ui.spinBox_crossover_two_point_operator_length.setEnabled(True)
            self.ui.spinBox_maximal_number_of_mutations.setEnabled(True)
            self.ui.spinBox_number_of_generations.setEnabled(True)
            self.ui.spinBox_population_size.setEnabled(True)

    def toggle_random_placement(self):
        if self.ui.label.isEnabled():
            self.ui.label.setEnabled(False)
            self.ui.label_2.setEnabled(False)
            self.ui.label_6.setEnabled(False)
            self.ui.label_11.setEnabled(False)
            self.ui.label_12.setEnabled(False)
            self.ui.label_13.setEnabled(False)
            self.ui.label_14.setEnabled(False)
            self.ui.spinBox_number_of_iterations.setEnabled(False)
            self.ui.doubleSpinBox_random_positioning.setEnabled(False)
            self.ui.doubleSpinBox_shift.setEnabled(False)
            self.ui.doubleSpinBox_swap.setEnabled(False)
            self.ui.comboBox_initialisation.setEnabled(False)
        else:
            self.ui.label.setEnabled(True)
            self.ui.label_2.setEnabled(True)
            self.ui.label_6.setEnabled(True)
            self.ui.label_11.setEnabled(True)
            self.ui.label_12.setEnabled(True)
            self.ui.label_13.setEnabled(True)
            self.ui.label_14.setEnabled(True)
            self.ui.spinBox_number_of_iterations.setEnabled(True)
            self.ui.doubleSpinBox_random_positioning.setEnabled(True)
            self.ui.doubleSpinBox_shift.setEnabled(True)
            self.ui.doubleSpinBox_swap.setEnabled(True)
            self.ui.comboBox_initialisation.setEnabled(True)


    def accept(self) -> None:
        if self.ui.spinBox_population_size.value() % 2 != 0:
            errordialog = QMessageBox(self)
            errordialog.setText("Population size error!")
            errordialog.setInformativeText("The population size must be an even number!")
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
            return
        if self.ui.doubleSpinBox_random_positioning.value() + self.ui.doubleSpinBox_shift.value() + self.ui.doubleSpinBox_swap.value() != 1:
            errordialog = QMessageBox(self)
            errordialog.setText("Weighting error!")
            errordialog.setInformativeText("The sum of the neighborhood components must be 1!\n Actual sum: {}".format(
                self.ui.doubleSpinBox_random_positioning.value() + self.ui.doubleSpinBox_shift.value() + self.ui.doubleSpinBox_swap.value()))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
            return
        self.set_parameter()
        super().accept()

    def set_parameter(self):
        if self.ui.radioButton_random_placement.isChecked():
            self.control_parameter.opt_method = OptimisationMethod.RandomPlacement
        else:
            self.control_parameter.opt_method = OptimisationMethod.GeneticAlgorithm

        self.control_parameter.number_of_generations = self.ui.spinBox_number_of_generations.value()
        self.control_parameter.population_size = self.ui.spinBox_population_size.value()
        self.control_parameter.mutation_rate = self.ui.doubleSpinBox_mutation_rate.value()
        self.control_parameter.maximal_number_of_mutations = self.ui.spinBox_maximal_number_of_mutations.value()
        self.control_parameter.crossover_rate = self.ui.doubleSpinBox_crossover_rate.value()
        self.control_parameter.crossover_two_point_operator_length = self.ui.spinBox_crossover_two_point_operator_length.value()

        self.control_parameter.number_of_iterations = self.ui.spinBox_number_of_iterations.value()
        self.control_parameter.neighborhood_search[
            'random_positioning'] = self.ui.doubleSpinBox_random_positioning.value()
        self.control_parameter.neighborhood_search['shift'] = self.ui.doubleSpinBox_shift.value()
        self.control_parameter.neighborhood_search['swap'] = self.ui.doubleSpinBox_swap.value()

        if self.ui.comboBox_initialisation.currentIndex() == 1:
            self.control_parameter.initialisation_method = InitialisationMethod.RANDOM
        elif self.ui.comboBox_initialisation.currentIndex() == 2:
            self.control_parameter.initialisation_method = InitialisationMethod.ALL_MODULAR
        else:
            self.control_parameter.initialisation_method = InitialisationMethod.A_STAR

        self.control_parameter.worker = self.ui.spinBox_cores.value()

        self.control_parameter.path_search_counter = self.ui.spinBox_path_search_counter.value()

    def load_parameter(self):
        if self.control_parameter.opt_method == OptimisationMethod.RandomPlacement:
            self.ui.radioButton_random_placement.setChecked(True)
            self.toggle_genetic()
        else:
            self.ui.radioButton_genetic_algorithm.setChecked(True)
            self.toggle_random_placement()

        self.ui.spinBox_number_of_generations.setMinimum(1)
        self.ui.spinBox_number_of_generations.setMaximum(10000)
        self.ui.spinBox_number_of_generations.setValue(self.control_parameter.number_of_generations)

        self.ui.spinBox_population_size.setMinimum(1)
        self.ui.spinBox_population_size.setMaximum(1000)
        self.ui.spinBox_population_size.setValue(self.control_parameter.population_size)

        self.ui.doubleSpinBox_mutation_rate.setMinimum(0)
        self.ui.doubleSpinBox_mutation_rate.setMaximum(1)
        self.ui.doubleSpinBox_mutation_rate.setSingleStep(0.01)
        self.ui.doubleSpinBox_mutation_rate.setValue(self.control_parameter.mutation_rate)

        self.ui.spinBox_maximal_number_of_mutations.setMinimum(1)
        self.ui.spinBox_maximal_number_of_mutations.setMaximum(
            self.control_parameter._length_cell * self.control_parameter._width_cell)
        self.ui.spinBox_maximal_number_of_mutations.setValue(self.control_parameter.maximal_number_of_mutations)

        self.ui.doubleSpinBox_crossover_rate.setMinimum(0)
        self.ui.doubleSpinBox_crossover_rate.setMaximum(1)
        self.ui.doubleSpinBox_crossover_rate.setSingleStep(0.01)
        self.ui.doubleSpinBox_crossover_rate.setValue(self.control_parameter.crossover_rate)

        self.ui.spinBox_crossover_two_point_operator_length.setMinimum(1)
        self.ui.spinBox_crossover_two_point_operator_length.setMaximum(
            self.control_parameter._length_cell * self.control_parameter._width_cell)
        self.ui.spinBox_crossover_two_point_operator_length.setValue(
            self.control_parameter.crossover_two_point_operator_length)

        self.ui.spinBox_number_of_iterations.setMinimum(1)
        self.ui.spinBox_number_of_iterations.setMaximum(100000)
        self.ui.spinBox_number_of_iterations.setValue(self.control_parameter.number_of_iterations)

        self.ui.doubleSpinBox_random_positioning.setMinimum(0)
        self.ui.doubleSpinBox_random_positioning.setMaximum(1)
        self.ui.doubleSpinBox_random_positioning.setSingleStep(0.01)
        self.ui.doubleSpinBox_random_positioning.setValue(
            self.control_parameter.neighborhood_search['random_positioning'])

        self.ui.doubleSpinBox_shift.setMinimum(0)
        self.ui.doubleSpinBox_shift.setMaximum(1)
        self.ui.doubleSpinBox_shift.setSingleStep(0.01)
        self.ui.doubleSpinBox_shift.setValue(
            self.control_parameter.neighborhood_search['shift'])

        self.ui.doubleSpinBox_swap.setMinimum(0)
        self.ui.doubleSpinBox_swap.setMaximum(1)
        self.ui.doubleSpinBox_swap.setSingleStep(0.01)
        self.ui.doubleSpinBox_swap.setValue(
            self.control_parameter.neighborhood_search['swap'])

        self.ui.comboBox_initialisation.addItem("A*-Algorithm (default)")
        self.ui.comboBox_initialisation.addItem("Random")
        self.ui.comboBox_initialisation.addItem("All Modular")
        if self.control_parameter.initialisation_method == InitialisationMethod.A_STAR:
            self.ui.comboBox_initialisation.setCurrentIndex(0)
        if self.control_parameter.initialisation_method == InitialisationMethod.RANDOM:
            self.ui.comboBox_initialisation.setCurrentIndex(1)
        if self.control_parameter.initialisation_method == InitialisationMethod.ALL_MODULAR:
            self.ui.comboBox_initialisation.setCurrentIndex(2)

        self.ui.spinBox_cores.setValue(self.control_parameter.worker)
        self.ui.spinBox_cores.setMinimum(1)
        self.ui.spinBox_cores.setMaximum(os.cpu_count())
        self.ui.spinBox_cores.setSingleStep(1)

        self.ui.spinBox_path_search_counter.setValue(self.control_parameter.path_search_counter)
        self.ui.spinBox_path_search_counter.setMinimum(1)
        self.ui.spinBox_path_search_counter.setMaximum(50)
        self.ui.spinBox_path_search_counter.setSingleStep(1)
