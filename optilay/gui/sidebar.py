from PySide6 import QtCore
from PySide6.QtGui import QPixmap, QPainter, QBrush, QPen, QIcon, Qt
from PySide6.QtWidgets import QDialog, QPushButton, QTreeWidget, QVBoxLayout, QTreeWidgetItem, QListWidget, \
    QListWidgetItem

from optilay.gui.util import colors
from optilay.gui.util.colors import ORANGE, GREEN
from optilay.model.layout_model import LayoutModel


class SectionExpandButton(QPushButton):
    """
    This QPushButton expands the section assigned to it.
    """

    def __init__(self, item: QTreeWidgetItem, text="", *args, **kwargs) -> None:
        """

        :param item: Section which gets assign to the button
        :param text: Text on the button
        :param args:
        :param kwargs:
        """
        super().__init__(text, *args, **kwargs)
        self.section = item
        self.clicked.connect(self.on_clicked)

    def on_clicked(self) -> None:
        """
        If the button is clicked, this method gets called and expands the section.
        :return: None
        """
        if self.section.isExpanded():
            self.section.setExpanded(False)
        else:
            self.section.setExpanded(True)


class Sidebar(QDialog):
    """
    This is the Sidebar of the MainWindow
    """

    def __init__(self, layout_model: LayoutModel, *args, **kwargs) -> None:
        """

        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.tree = QTreeWidget()
        self.tree.setHeaderHidden(True)
        layout = QVBoxLayout()
        layout.addWidget(self.tree)
        self.setLayout(layout)
        self.tree.setIndentation(0)

        self.model: LayoutModel = layout_model

        self.available_conveyors_table = QListWidget(parent=self)
        self.available_goods_table = QListWidget(parent=self)
        self.legend_table = QListWidget(parent=self)

        self.sections = []

        self.define_sections()
        self.add_sections()

        self.set_available_conveyors_table()
        self.set_available_goods_table()
        self.set_legend()

    def add_sections(self) -> None:
        """
        Creates the Widgets und Buttons for each defined section.
        :return: None
        """
        for (title, widget) in self.sections:
            button1 = self.add_button(title)
            section1 = self.add_widget(button1, widget)
            button1.addChild(section1)

    def define_sections(self) -> None:
        """
        Defines all section which should appear in the Sidebar.
        :return: None
        """
        self.sections.append(("Legend", self.legend_table))
        self.sections.append(("Available conveyors", self.available_conveyors_table))
        self.sections.append(("Available goods", self.available_goods_table))

    def add_button(self, title) -> QTreeWidgetItem:
        """
        Adds a SectionExpandButton to the Sidebar.
        :param title: Title for the SectionExpandButton.
        :return: The QTreeWidgetItem where the button is contained in.
        """
        item = QTreeWidgetItem()
        self.tree.addTopLevelItem(item)
        self.tree.setItemWidget(item, 0, SectionExpandButton(item, text=title))
        return item

    def add_widget(self, button, widget) -> QTreeWidgetItem:
        """
        Adds the widget as sub element of the button to the QTreeWidget
        :param button:
        :param widget:
        :return:
        """
        section = QTreeWidgetItem(button)
        section.setDisabled(True)
        self.tree.setItemWidget(section, 0, widget)
        return section

    def clear_available_conveyors_table(self) -> None:
        """
        Deletes all elements from the available_conveyors_table.
        :return: None
        """
        self.available_conveyors_table.clear()

    def set_available_conveyors_table(self) -> None:
        """
        Adds all conveyors that can be placed on the current layout.
        :return: None
        """
        self.clear_available_conveyors_table()
        avail_conv = self.model.get_current_layout().get_available_conveyor()
        parameter = self.model.get_parameter()
        for conv in avail_conv:
            if conv in parameter.conv_types:
                QListWidgetItem(conv, self.available_conveyors_table)
            else:
                item = QListWidgetItem(conv, self.available_conveyors_table)
                item.setForeground(colors.GREY)

    def clear_available_goods_table(self) -> None:
        """

        :return: None
        """
        self.available_goods_table.clear()

    def set_available_goods_table(self) -> None:
        """

        :return: None
        """
        self.clear_available_goods_table()
        avail_goods = self.model.get_current_layout().get_available_goods()
        for goods in avail_goods:
            QListWidgetItem(goods, self.available_goods_table)

    def set_legend(self) -> None:
        self.add_forward_backward_conveyor_icon_to_legend()
        self.add_four_way_intersection_conveyor_icon_to_legend()
        self.add_omnidirectional_conveyor_icon_to_legend()
        self.add_sink_icon_to_legend()
        self.add_source_icon_to_legend()
        self.add_workplace_icon_to_legend()
        self.add_restricted_area_icon_to_legend()

    def add_forward_backward_conveyor_icon_to_legend(self):
        conveyor_icon = QListWidgetItem(self.tr("ForwardBackward conveyor"), self.legend_table)
        conveyor_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        brush = QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(colors.BLACK)
        painter.setBrush(brush)
        painter.setPen(QPen(brush, 5, QtCore.Qt.SolidLine, QtCore.Qt.SquareCap))
        painter.drawLine((1 / 4) * pixmap.width(), 0, (1 / 4) * pixmap.width(), pixmap.height())
        painter.drawLine((2 / 4) * pixmap.width(), 0, (2 / 4) * pixmap.width(), pixmap.height())
        painter.drawLine((3 / 4) * pixmap.width(), 0, (3 / 4) * pixmap.width(), pixmap.height())
        painter.end()
        conveyor_icon.setIcon(QIcon(pixmap))

    def add_four_way_intersection_conveyor_icon_to_legend(self):
        conveyor_icon = QListWidgetItem(self.tr("FourWayIntersect conveyor"), self.legend_table)
        conveyor_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        brush = QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(colors.BLACK)
        painter.setBrush(brush)
        painter.setPen(QPen(brush, 5, QtCore.Qt.SolidLine, QtCore.Qt.SquareCap))
        painter.drawLine((1 / 4) * pixmap.width(), 0, (1 / 4) * pixmap.width(), pixmap.height())
        painter.drawLine((2 / 4) * pixmap.width(), 0, (2 / 4) * pixmap.width(), pixmap.height())
        painter.drawLine((3 / 4) * pixmap.width(), 0, (3 / 4) * pixmap.width(), pixmap.height())
        painter.drawLine(0, (1/4) * pixmap.height(), pixmap.width(), (1/4) * pixmap.height())
        painter.drawLine(0, (2/4) * pixmap.height(), pixmap.width(), (2/4) * pixmap.height())
        painter.drawLine(0, (3/4) * pixmap.height(), pixmap.width(), (3/4) * pixmap.height())
        painter.end()
        conveyor_icon.setIcon(QIcon(pixmap))

    def add_omnidirectional_conveyor_icon_to_legend(self):
        conveyor_icon = QListWidgetItem(self.tr("Omnidirectional conveyor"), self.legend_table)
        conveyor_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        brush = QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(colors.BLACK)
        painter.setBrush(brush)
        painter.setPen(QPen(brush, 5, QtCore.Qt.SolidLine, QtCore.Qt.SquareCap))
        painter.drawLine(0, 0, pixmap.width(), pixmap.height())
        painter.drawLine(pixmap.width(), 0, 0, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, 0, pixmap.height() / 2)
        painter.drawLine(pixmap.width(), pixmap.height() / 2, pixmap.width() / 2, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, pixmap.width(), pixmap.height() / 2)
        painter.drawLine(0, pixmap.height() / 2, pixmap.width() / 2, pixmap.height())
        painter.end()
        conveyor_icon.setIcon(QIcon(pixmap))

    def add_sink_icon_to_legend(self):
        source_icon = QListWidgetItem(self.tr("Sink"), self.legend_table)
        source_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        pen = painter.pen()
        pen.setWidth(5)
        pen.setColor(ORANGE)
        pen.setCapStyle(Qt.RoundCap)
        painter.setPen(pen)
        painter.drawEllipse(pixmap.width() / 6, pixmap.height() / 6, pixmap.width() * 4 / 6,
                            pixmap.height() * 4 / 6)
        painter.drawLine((2 / 6) * pixmap.width(),
                         (2 / 6) * pixmap.height(),
                         (4 / 6) * pixmap.width(),
                         (4 / 6) * pixmap.height())

        painter.drawLine((4 / 6) * pixmap.width(),
                         (2 / 6) * pixmap.height(),
                         (2 / 6) * pixmap.width(),
                         (4 / 6) * pixmap.height())
        painter.end()
        source_icon.setIcon(QIcon(pixmap))

    def add_restricted_area_icon_to_legend(self):
        restricted_icon = QListWidgetItem(self.tr("Restricted area"), self.legend_table)
        restricted_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        brush = QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(colors.RED)
        painter.setBrush(brush)
        painter.setPen(QPen(brush, 5, QtCore.Qt.SolidLine, QtCore.Qt.SquareCap))
        painter.drawLine(0, 0, pixmap.width(), pixmap.height())
        painter.drawLine(pixmap.width(), 0, 0, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, 0, pixmap.height() / 2)
        painter.drawLine(pixmap.width(), pixmap.height() / 2, pixmap.width() / 2, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, pixmap.width(), pixmap.height() / 2)
        painter.drawLine(0, pixmap.height() / 2, pixmap.width() / 2, pixmap.height())
        painter.end()
        restricted_icon.setIcon(QIcon(pixmap))

    def add_workplace_icon_to_legend(self):
        workplace_icon = QListWidgetItem(self.tr("Workplace"), self.legend_table)
        workplace_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        brush = QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(colors.BLUE)
        painter.setBrush(brush)
        painter.setPen(QPen(brush, 5, QtCore.Qt.SolidLine, QtCore.Qt.SquareCap))
        painter.drawLine(0, 0, pixmap.width(), pixmap.height())
        painter.drawLine(pixmap.width(), 0, 0, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, 0, pixmap.height() / 2)
        painter.drawLine(pixmap.width(), pixmap.height() / 2, pixmap.width() / 2, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, pixmap.width(), pixmap.height() / 2)
        painter.drawLine(0, pixmap.height() / 2, pixmap.width() / 2, pixmap.height())
        painter.end()
        workplace_icon.setIcon(QIcon(pixmap))

    def add_source_icon_to_legend(self):
        sink_icon = QListWidgetItem(self.tr("Source"), self.legend_table)
        sink_icon.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        pen = painter.pen()
        pen.setWidth(5)
        pen.setColor(GREEN)
        pen.setCapStyle(Qt.RoundCap)
        painter.setPen(pen)
        painter.drawEllipse(pixmap.width() / 6, pixmap.height() / 6, pixmap.width() * 4 / 6,
                            pixmap.height() * 4 / 6)
        pen.setWidth(10)
        painter.setPen(pen)
        painter.drawPoint(pixmap.width() / 2, pixmap.height() / 2)
        painter.end()
        sink_icon.setIcon(QIcon(pixmap))

    def clear_legend(self) -> None:
        self.legend_table.clear()
