import logging

from PySide6.QtWidgets import QDialog

from optilay.gui.generated.pairwise_comparison_dialog_gen import Ui_pairwise_comparison_dialog
from optilay.model.weighting import Weighting


class PairwiseComparisonDialog(QDialog):
    """
    In the PairwiseComparisonDialog the user can choose between different weighting attributes which are more
    important for him. In the end, a weighting is determined from this.
    """

    def __init__(self, weighting: Weighting, criteria_list, attr_name_dict, *args, **kwargs) -> None:
        """

        :param weighting: Weighting class where the weighting is stored.
        :param criteria_list: List of all attributes to be compared between.
        :param attr_name_dict: Names of all attributes to be weighted
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_pairwise_comparison_dialog()
        self.ui.setupUi(self)

        self.attr_name_dict = attr_name_dict

        self.weighting = weighting

        self.not_weighted = []
        self.weights_count = {}
        self.weights1 = []
        self.weights2 = []
        for attr, _ in self.weighting.__dict__.items():
            if attr in criteria_list:
                self.weights_count[attr] = 0
                self.weights1.append(attr)
                self.weights2.append(attr)
            else:
                self.not_weighted.append(attr)

        self.weights2.pop(0)

        self.max_c = int((len(self.weights1) ** 2 - len(self.weights1)) / 2)
        self.count = 0

        self.ui.count_label.setText("{}/{}".format(self.count, self.max_c))

        self.ui.left_pushButton.setText(self.weights1.pop(0))
        self.ui.left_pushButton.clicked.connect(self.clicked_left)

        self.ui.right_pushButton.setText(self.weights2.pop(0))
        self.ui.right_pushButton.clicked.connect(self.clicked_right)

        logging.info("Loaded PairwiseComparisonDialog")

    def clicked_left(self) -> None:
        """
        Counts the click on the left button and loads the next comparison.
        :return: None
        """
        self.weights_count[self.ui.left_pushButton.text()] += 1
        self.count += 1
        self.ui.count_label.setText("{}/{}".format(self.count, self.max_c))
        self.next_comparison()
        if self.count >= self.max_c:
            self.calc_weights()

    def clicked_right(self):
        """
        Counts the click on the right button and loads the next comparison.
        :return: None
        """
        self.weights_count[self.ui.right_pushButton.text()] += 1
        self.count += 1
        self.ui.count_label.setText("{}/{}".format(self.count, self.max_c))
        self.next_comparison()
        if self.count >= self.max_c:
            self.calc_weights()

    def next_comparison(self) -> None:
        """
        Load the next comparison.
        :return: None
        """
        if self.weights2:
            self.ui.right_pushButton.setText(self.weights2.pop(0))
        elif len(self.weights1) > 1:
            self.ui.left_pushButton.setText(self.weights1.pop(0))
            self.weights2 = self.weights1[:]
            self.ui.right_pushButton.setText(self.weights2.pop(0))

    def calc_weights(self) -> None:
        """
        Calculates the weighting from the pairwise comparison and closes the dialog.
        :return: None
        """
        for key, value in self.weights_count.items():
            setattr(self.weighting, self.attr_name_dict[key], round((value / self.max_c), 2))
        for attr in self.not_weighted:
            setattr(self.weighting, self.attr_name_dict[attr], 0)
        c = 0
        max_attr = ''
        min_attr = ''
        max_val = 0
        min_val = 1
        for attr, val in self.weighting.__dict__.items():
            c += val
            if val > max_val:
                max_val = val
                max_attr = attr
            if 0 < val < min_val:
                min_val = val
                min_attr = attr
        if c < 1:
            setattr(self.weighting, min_attr, min_val + (1 - c))
        elif c > 1:
            setattr(self.weighting, max_attr, max_val - (c - 1))
        c = 0
        for attr, val in self.weighting.__dict__.items():
            c += val
        self.accept()
