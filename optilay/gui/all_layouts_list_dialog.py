from PySide6.QtWidgets import QDialog, QListWidgetItem

from optilay.gui.generated.all_layouts_dialog_gen import Ui_All_layouts_dialog
from optilay.gui.layout_detail_dialog import LayoutDetailDialog
from optilay.model.layout_db import LayoutDB
from optilay.model.layout_model import LayoutModel


class AllLayoutsListDialog(QDialog):
    """
    The AllLayoutsListDialog shows all available ConveyorSystemLayout variants.
    """

    def __init__(self, layout: LayoutModel, *args, **kwargs) -> None:
        """

        :param db: LayoutDB in which all ConveyorSystemLayouts are stored
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_All_layouts_dialog()
        self.ui.setupUi(self)
        self.layout: LayoutModel = layout
        self.db: LayoutDB = self.layout.get_db()

        for index, conv_layout in enumerate(self.db.get_all_layouts()):
            self.ui.listWidget.addItem(
                QListWidgetItem("Layout {}: {}".format(conv_layout.layout_id, conv_layout.evaluation.weighted_sum)))
