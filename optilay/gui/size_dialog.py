import math

from PySide6.QtWidgets import QDialog

from optilay.gui.generated.size_dialog_gen import Ui_size_dialog
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.util.cell_size import CellSize
from optilay.model.util.fixed_values import MAX_SIZE


class SizeDialog(QDialog):
    """
    In this QDialog the user can adjust the size and cell size of the factory.
    """

    def __init__(self, layout:ConveyorSystemLayout, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.ui = Ui_size_dialog()
        self.ui.setupUi(self)

        self.max_size = MAX_SIZE

        for i, size in enumerate(CellSize):
            self.ui.comboBox_cell_size.insertItem(i, str(size.value))

        #self.ui.doubleSpinBox_factory_length.setValue(int(int(self.ui.comboBox_cell_size.currentText()) * round(
        #    math.sqrt(self.max_size) / int(self.ui.comboBox_cell_size.currentText()))))
        #self.ui.doubleSpinBox_factory_width.setValue(int(int(self.ui.comboBox_cell_size.currentText()) * round(
        #    math.sqrt(self.max_size) / int(self.ui.comboBox_cell_size.currentText()))))
        self.ui.doubleSpinBox_factory_length.setValue(layout.length)
        self.ui.doubleSpinBox_factory_width.setValue(layout.width)

        self.adjust_spinbox_steps()

        self.ui.warn_label.setStyleSheet("font-weight: bold; color: red")

        self.ui.doubleSpinBox_factory_length.valueChanged.connect(self.change_factory_length)
        self.ui.doubleSpinBox_factory_width.valueChanged.connect(self.change_factory_width)
        self.ui.comboBox_cell_size.currentTextChanged.connect(self.change_factory_cell_size)

    def change_factory_length(self) -> None:
        """

        :return: None
        """
        if self.ui.doubleSpinBox_factory_length.value() % int(self.ui.comboBox_cell_size.currentText()) != 0:
            self.ui.warn_label.setText("The size of the factory must be divisible by the cell size!")
        elif self.ui.doubleSpinBox_factory_width.value() % int(self.ui.comboBox_cell_size.currentText()) == 0:
            self.ui.warn_label.setText("")

    def change_factory_width(self) -> None:
        """

        :return: None
        """
        if self.ui.doubleSpinBox_factory_width.value() % int(self.ui.comboBox_cell_size.currentText()) != 0:
            self.ui.warn_label.setText("The size of the factory must be divisible by the cell size!")
        elif self.ui.doubleSpinBox_factory_length.value() % int(self.ui.comboBox_cell_size.currentText()) == 0:
            self.ui.warn_label.setText("")

    def change_factory_cell_size(self) -> None:
        """

        :return: None
        """
        self.adjust_spinbox_steps()
        self.change_factory_width()
        self.change_factory_length()

    def adjust_spinbox_steps(self) -> None:
        """
        This method adjusts the steps of the QDoubleSpinBox, when the cell size was changed.
        :return: None
        """
        self.ui.doubleSpinBox_factory_length.setSingleStep(int(self.ui.comboBox_cell_size.currentText()))
        self.ui.doubleSpinBox_factory_width.setSingleStep(int(self.ui.comboBox_cell_size.currentText()))
