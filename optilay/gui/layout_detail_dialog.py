import copy
import logging

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QDialog, QVBoxLayout, QDialogButtonBox

from optilay.gui.generated.layout_detail_dialog_gen import Ui_LayoutDetailDialog
from optilay.gui.layout_scene import LayoutScene
from optilay.gui.layout_view import LayoutView
from optilay.gui.util.fixed_gui_values import CELL_ZOOM
from optilay.gui.util.result_chart_widget import ResultChartWidget
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.layout_model import LayoutModel


class LayoutDetailDialog(QDialog):
    """

    """

    def __init__(self, layout: LayoutModel, conv_layout: ConveyorSystemLayout):
        super().__init__()
        self.ui = Ui_LayoutDetailDialog()
        self.ui.setupUi(self)

        self.layout = layout
        self.conv_layout = conv_layout

        self.view = LayoutView(self)
        scene = LayoutScene(self.conv_layout)
        scene.init_grid()
        self.view.setScene(scene)

        layout = QVBoxLayout()
        self.setLayout(layout)

        layout.addWidget(self.view)
        layout.addWidget(ResultChartWidget([self.conv_layout.evaluation], dpi=100))
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel)
        self.buttonBox.addButton("Layout übernehmen", QDialogButtonBox.AcceptRole)
        layout.addWidget(self.buttonBox)
        self.buttonBox.accepted.connect(self.apply)
        self.buttonBox.rejected.connect(self.reject)

    def fit_view(self) -> None:
        """
        Fits content of graphics view in given place.
        :return: None
        """
        self.view.fitInView(0, 0,
                            (
                                    self.view.scene().conv_layout.length / self.view.scene().conv_layout._cell_size.value) * CELL_ZOOM,
                            (
                                    self.view.scene().conv_layout.width / self.view.scene().conv_layout._cell_size.value) * CELL_ZOOM,
                            Qt.KeepAspectRatio)

    def apply(self) -> None:
        """
        Sets layout to main layout in MainView.
        :return: None
        """
        self.layout.load_layout(self.conv_layout.layout_id)
        self.accept()
