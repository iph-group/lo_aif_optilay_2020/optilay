from PySide6 import QtWidgets
from PySide6.QtWidgets import QDialog, QHeaderView, QTableWidgetItem

from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.gui.generated.evaluation_table_dialog_gen import Ui_evaluatio_table_dialog
from optilay.model.weighting import Weighting


class EvaluationTableDialog(QDialog):
    """
    In the EvaluationTableDialog the results are displayed after an evaluation.
    """

    def __init__(self, result: EvaluationResult, best: EvaluationResult, weights: Weighting, *args, **kwargs) -> None:
        """

        :param result: Results of the evaluation.
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_evaluatio_table_dialog()
        self.ui.setupUi(self)
        self.result = result
        self.best = best
        self.weighting = weights
        self.load_evaluation_data()

    def load_evaluation_data(self) -> None:
        """
        This method is loading the elements of the EvaluationTable.
        :return: None
        """
        self.ui.table_eval.setRowCount(5 + 1)
        self.ui.table_eval.setColumnCount(6)
        header = self.ui.table_eval.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)

        self.ui.table_eval.setHorizontalHeaderLabels(
            ["Criteria", "Weighting [%]", "Result values",
             "Rating", "Best values", "Rating"])
        self.ui.table_eval.verticalHeader().hide()

        self.ui.table_eval.setItem(0, 0, QTableWidgetItem("Throughput [Goods/hour]"))
        self.ui.table_eval.setItem(0, 1, QTableWidgetItem(str(int(self.weighting.throughput * 100))))
        self.ui.table_eval.setItem(0, 2, QTableWidgetItem(str(int(round(self.result.throughput[0], 0)))))
        self.ui.table_eval.setItem(0, 3, QTableWidgetItem(str(round(self.result.throughput[1], 2))))
        if self.best:
            self.ui.table_eval.setItem(0, 4, QTableWidgetItem(str(int(round(self.best.throughput[0], 0)))))
            self.ui.table_eval.setItem(0, 5, QTableWidgetItem(str(round(self.best.throughput[1], 2))))

        self.ui.table_eval.setItem(1, 0, QTableWidgetItem("Lead time [s]"))
        self.ui.table_eval.setItem(1, 1, QTableWidgetItem(str(int(self.weighting.lead_time * 100))))
        self.ui.table_eval.setItem(1, 2, QTableWidgetItem(str(int(round(self.result.lead_time[0], 0)))))
        self.ui.table_eval.setItem(1, 3, QTableWidgetItem(str(round(self.result.lead_time[1], 2))))
        if self.best:
            self.ui.table_eval.setItem(1, 4, QTableWidgetItem(str(int(round(self.best.lead_time[0], 0)))))
            self.ui.table_eval.setItem(1, 5, QTableWidgetItem(str(round(self.best.lead_time[1], 2))))

        self.ui.table_eval.setItem(2, 0, QTableWidgetItem("Space requirement [mm²]"))
        self.ui.table_eval.setItem(2, 1, QTableWidgetItem(str(int(self.weighting.space_requirement * 100))))
        self.ui.table_eval.setItem(2, 2, QTableWidgetItem(str(int(round(self.result.space_requirement[0], 0)))))
        self.ui.table_eval.setItem(2, 3, QTableWidgetItem(str(round(self.result.space_requirement[1], 2))))
        if self.best:
            self.ui.table_eval.setItem(2, 4, QTableWidgetItem(str(int(round(self.best.space_requirement[0], 0)))))
            self.ui.table_eval.setItem(2, 5, QTableWidgetItem(str(round(self.best.space_requirement[1], 2))))

        self.ui.table_eval.setItem(3, 0, QTableWidgetItem("Buffer capacity [Amount]"))
        self.ui.table_eval.setItem(3, 1, QTableWidgetItem(str(int(self.weighting.buffer_capacity * 100))))
        self.ui.table_eval.setItem(3, 2, QTableWidgetItem(str(int(round(self.result.buffer_capacity[0], 0)))))
        self.ui.table_eval.setItem(3, 3, QTableWidgetItem(str(round(self.result.buffer_capacity[1], 2))))
        if self.best:
            self.ui.table_eval.setItem(3, 4, QTableWidgetItem(str(int(round(self.best.buffer_capacity[0], 0)))))
            self.ui.table_eval.setItem(3, 5, QTableWidgetItem(str(round(self.best.buffer_capacity[1], 2))))

        self.ui.table_eval.setItem(4, 0, QTableWidgetItem("Costs [€]"))
        self.ui.table_eval.setItem(4, 1, QTableWidgetItem(str(int(self.weighting.costs * 100))))
        self.ui.table_eval.setItem(4, 2, QTableWidgetItem(str(int(round(self.result.costs[0], 0)))))
        self.ui.table_eval.setItem(4, 3, QTableWidgetItem(str(round(self.result.costs[1], 2))))
        if self.best:
            self.ui.table_eval.setItem(4, 4, QTableWidgetItem(str(int(round(self.best.costs[0], 0)))))
            self.ui.table_eval.setItem(4, 5, QTableWidgetItem(str(round(self.best.costs[1], 2))))

        self.ui.table_eval.setItem(5, 0, QTableWidgetItem("Weighted Sum"))
        self.ui.table_eval.setItem(5, 2, QTableWidgetItem(str(round(self.result.weighted_sum, 3))))
        if self.best and self.best.weighted_sum:
            self.ui.table_eval.setItem(5, 4, QTableWidgetItem(str(round(self.best.weighted_sum, 3))))

        self.ui.table_eval.resizeColumnsToContents()
