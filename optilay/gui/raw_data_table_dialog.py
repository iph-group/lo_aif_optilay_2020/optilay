from PySide6.QtWidgets import QDialog, QTableWidgetItem, QAbstractItemView

from optilay.gui.generated.raw_data_table_dialog_gen import Ui_raw_data_table_dialog
from optilay.model.conveyor_system_layout import ConveyorSystemLayout


class RawDataTableDialog(QDialog):
    """

    """

    def __init__(self, con_layout, *args, **kwargs) -> None:
        """

        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_raw_data_table_dialog()
        self.ui.setupUi(self)
        self.conv_layout: ConveyorSystemLayout = con_layout

        self.init_general()
        self.init_systemconnections()
        self.init_goods()
        self.init_conveyor()

    def init_general(self) -> None:
        self.ui.general_table_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ui.general_table_widget.setColumnCount(2)
        self.ui.general_table_widget.setRowCount(5)

        self.ui.general_table_widget.setHorizontalHeaderLabels(['Attribute', 'Value'])

        self.ui.general_table_widget.setItem(0, 0, QTableWidgetItem("Layout ID"))
        self.ui.general_table_widget.setItem(0, 1, QTableWidgetItem(str(self.conv_layout.layout_id)))
        self.ui.general_table_widget.setItem(1, 0, QTableWidgetItem("Length [mm]"))
        self.ui.general_table_widget.setItem(1, 1, QTableWidgetItem(str(self.conv_layout.length)))
        self.ui.general_table_widget.setItem(2, 0, QTableWidgetItem("Width [mm]"))
        self.ui.general_table_widget.setItem(2, 1, QTableWidgetItem(str(self.conv_layout.width)))
        self.ui.general_table_widget.setItem(3, 0, QTableWidgetItem("Cell size [mm]"))
        self.ui.general_table_widget.setItem(3, 1, QTableWidgetItem(str(self.conv_layout._cell_size.name)))
        self.ui.general_table_widget.setItem(4, 0, QTableWidgetItem("Max size [mm²]"))
        self.ui.general_table_widget.setItem(4, 1, QTableWidgetItem(str(self.conv_layout.MAXSIZE)))

    def init_systemconnections(self) -> None:
        self.ui.systemconnection_table_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ui.systemconnection_table_widget.setRowCount(len(self.conv_layout.connection))
        self.ui.systemconnection_table_widget.setColumnCount(4)

        self.ui.systemconnection_table_widget.setHorizontalHeaderLabels(
            ['ID', 'Connected position', 'Type', 'Direction'])

        for i, syscon in enumerate(self.conv_layout.connection):
            self.ui.systemconnection_table_widget.setItem(i, 0, QTableWidgetItem(str(syscon.id)))
            self.ui.systemconnection_table_widget.setItem(i, 1, QTableWidgetItem(
                '[' + str(syscon.connected_position[1]) + ', ' + str(syscon.connected_position[0]) + ']'))
            self.ui.systemconnection_table_widget.setItem(i, 2, QTableWidgetItem(syscon.type.name))
            self.ui.systemconnection_table_widget.setItem(i, 3, QTableWidgetItem(syscon.direction.name))

    def init_goods(self) -> None:
        goods_count = 0

        self.ui.goods_table_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.ui.goods_table_widget.setColumnCount(11)
        self.ui.goods_table_widget.setHorizontalHeaderLabels(
            ['ID', 'Name', 'Amount', 'Source ID', 'Sink ID', 'Type', 'Actual weight [g]', 'Empty weight [g]',
             'Load weight [g]', 'Max weight [g]', 'Size'])

        for packetstream in self.conv_layout.packet_stream:
            for goods, amount in packetstream.get_goods().items():
                self.ui.goods_table_widget.setRowCount(self.ui.goods_table_widget.rowCount() + 1)
                self.ui.goods_table_widget.setItem(goods_count, 0, QTableWidgetItem(str(goods.id)))
                self.ui.goods_table_widget.setItem(goods_count, 1, QTableWidgetItem(goods.name))
                self.ui.goods_table_widget.setItem(goods_count, 2, QTableWidgetItem(str(amount)))
                self.ui.goods_table_widget.setItem(goods_count, 3, QTableWidgetItem(str(packetstream.source.id)))
                self.ui.goods_table_widget.setItem(goods_count, 4, QTableWidgetItem(str(packetstream.sink.id)))
                self.ui.goods_table_widget.setItem(goods_count, 5, QTableWidgetItem(goods.type.name))
                self.ui.goods_table_widget.setItem(goods_count, 6, QTableWidgetItem(str(goods.actual_weight)))
                self.ui.goods_table_widget.setItem(goods_count, 7, QTableWidgetItem(str(goods._empty_weight)))
                self.ui.goods_table_widget.setItem(goods_count, 8, QTableWidgetItem(str(goods._load_weight)))
                self.ui.goods_table_widget.setItem(goods_count, 9, QTableWidgetItem(str(goods._max_weight)))
                self.ui.goods_table_widget.setItem(goods_count, 10, QTableWidgetItem(
                    '[' + str(goods._size[1]) + ', ' + str(goods._size[0]) + ']'))
                goods_count += 1

    def init_conveyor(self) -> None:
        self.ui.conveyor_table_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ui.conveyor_table_widget.setRowCount(len(self.conv_layout.conveyors))
        self.ui.conveyor_table_widget.setColumnCount(7)

        self.ui.conveyor_table_widget.setHorizontalHeaderLabels(
            ['ID', 'Name', 'Position', 'Size', 'Transport direction', 'Velocity [m/s]', 'Maximum transport weight [g]'])

        for i, conveyor in enumerate(self.conv_layout.conveyors):
            self.ui.conveyor_table_widget.setItem(i, 0, QTableWidgetItem(str(conveyor.cell_value)))
            self.ui.conveyor_table_widget.setItem(i, 1, QTableWidgetItem(conveyor.name))
            self.ui.conveyor_table_widget.setItem(i, 2, QTableWidgetItem(
                '[' + str(conveyor.position[1]) + ', ' + str(conveyor.position[0]) + ']'))
            self.ui.conveyor_table_widget.setItem(i, 3, QTableWidgetItem(
                '[' + str(conveyor.size[1]) + ', ' + str(conveyor.size[0]) + ']'))
            self.ui.conveyor_table_widget.setItem(i, 4, QTableWidgetItem(conveyor.trans_direction.name))
            self.ui.conveyor_table_widget.setItem(i, 5, QTableWidgetItem(str(conveyor.velocity)))
            self.ui.conveyor_table_widget.setItem(i, 6, QTableWidgetItem(str(conveyor.max_weight)))
