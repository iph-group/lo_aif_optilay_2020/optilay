from PySide6.QtWidgets import QDialog, QButtonGroup, QWidget, QVBoxLayout, QRadioButton

from optilay.gui.generated.layout_compare_dialog_gen import Ui_LayoutComparisonDialog
from optilay.gui.layout_scene import LayoutScene
from optilay.gui.layout_view import LayoutView
from optilay.gui.util.result_chart_widget import ResultChartWidget
from optilay.model.layout_db import LayoutDB
from optilay.model.layout_model import LayoutModel


class LayoutComparisonDialog(QDialog):
    """
    In the LayoutComparisonDialog the best ConveyorSystemLayout are displayed so that the user can compare them and
    choose the most suitable one.
    """

    def __init__(self, layout: LayoutModel, *args, **kwargs) -> None:
        """

        :param db: LayoutDB where the ConveyorSystemLayout are stored inside.
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_LayoutComparisonDialog()
        self.ui.setupUi(self)

        self.layout: LayoutModel = layout
        self.db: LayoutDB = self.layout.get_db()
        num_compare_layouts = self.layout.get_parameter().num_compare_layouts

        self.button_group = QButtonGroup()
        self.button_group.setExclusive(True)

        self.views = []
        row = 0
        column = 0

        self.best_layouts = self.db.get_best_layouts(num_compare_layouts)

        for i, layout in enumerate(self.best_layouts):
            view = LayoutView()
            scene = LayoutScene(layout)
            scene.init_grid()
            view.setScene(scene)
            self.views.append(view)
            widget = QWidget()
            vbox = QVBoxLayout()
            vbox.addWidget(view)
            r_btn = QRadioButton(
                self.tr("Layout {}: {:.3f}%").format(i, layout.evaluation.weighted_sum))
            vbox.addWidget(r_btn)
            self.button_group.addButton(r_btn, i)
            widget.setLayout(vbox)
            self.ui.gridLayout.addWidget(widget, row, column)
            self.ui.gridLayout.setRowMinimumHeight(row, 500)
            if column == 1:
                column = 0
                row += 1
            else:
                column += 1

            if i == 0:
                self.button_group.button(0).setChecked(True)

        self.diagram = ResultChartWidget([layout.evaluation for layout in self.best_layouts], dpi=100)
        if column == 1:
            self.ui.gridLayout.addWidget(self.diagram, row + 1, 0, row + 2, 2)
            self.ui.gridLayout.setRowMinimumHeight(row + 1, 500)
        else:
            self.ui.gridLayout.addWidget(self.diagram, row, 0, row + 1, 2)
            self.ui.gridLayout.setRowMinimumHeight(row, 500)
        self.ui.buttonBox.accepted.connect(self.accept)

    def accept(self) -> None:
        checked_id = self.button_group.checkedId()
        if checked_id >= 0:
            self.layout.load_layout(self.best_layouts[checked_id].layout_id)
        super().accept()

    def fit_view(self) -> None:
        """
        Fits the content of all LayoutView into the available space.
        :return: None
        """
        pass
#        for view in self.views:
#            view.fitInView(0, 0, self.factory.layout.shape[1] * self.factory.cell_zoom,
#                           self.factory.layout.shape[0] * self.factory.cell_zoom,
#                           Qt.KeepAspectRatio)
