import functools
from typing import List, Tuple

from PySide6 import QtGui
from PySide6.QtCore import QRect, QSize, QPoint
from PySide6.QtGui import Qt, QAction
from PySide6.QtWidgets import QGraphicsView, QFrame, QRubberBand, QMenu

from optilay.gui.objects.conveyor_cell import ConveyorCell
from optilay.gui.objects.layout_cell import LayoutCell
from optilay.gui.objects.system_connection_cell import SystemConnectionCell
from optilay.gui.util.fixed_gui_values import CELL_ZOOM
from optilay.gui.util.layout_mode import LayoutMode
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType


class LayoutView(QGraphicsView):
    """

    """

    def __init__(self, *args, **kwargs) -> None:
        """

        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setFrameShape(QFrame.NoFrame)
        # for rubberband
        self.rubber_band = QRubberBand(QRubberBand.Rectangle, self)
        self.origin = QPoint()

        self.curr_scale = 1

        self.setContextMenuPolicy(Qt.CustomContextMenu)

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        """ This method is responsible for zooming in this scene, a little bit buggy"""
        self.update()
        # Zoom Factor
        zoom_in_factor = 1.15
        zoom_out_factor = 1 / zoom_in_factor

        # Zoom
        if event.angleDelta().y() > 0:
            self.curr_scale *= zoom_in_factor
            self.scale(zoom_in_factor, zoom_in_factor)
        elif self.curr_scale > 0.1:
            self.curr_scale *= zoom_out_factor
            self.scale(zoom_out_factor, zoom_out_factor)

        super().wheelEvent(event)

    def mousePressEvent(self, event: QtGui.QMouseEvent) -> None:
        """ This method created a rubberband an leftclick"""
        if event.button() == Qt.LeftButton:
            point = self.mapToScene(event.pos())
            under = self.scene().items(point)
            selected_conveyor = [x for x in under if isinstance(x, ConveyorCell)]
            if not selected_conveyor:
                self.origin = QPoint(int(event.pos().x()), int(event.pos().y()))
                self.rubber_band.setGeometry(QRect(self.origin, QSize()))
                self.rubber_band.show()

        super().mousePressEvent(event)

    def mouseMoveEvent(self, event: QtGui.QMouseEvent) -> None:
        """ This method will change the rubberband size if the mouse is moving"""
        if not self.origin.isNull():
            self.rubber_band.setGeometry(QRect(self.origin, event.pos()).normalized())

        super().mouseMoveEvent(event)

    def mouseReleaseEvent(self, event: QtGui.QMouseEvent) -> None:
        """
        If FactoryCell items are selected by the rubberband
        they will be removed from the scene
        """
        point = self.mapToScene(event.pos())
        under = self.scene().items(point)
        selected_point = [x for x in under if isinstance(x, LayoutCell)]
        selected_conveyor = [x for x in under if isinstance(x, ConveyorCell)]
        selected_sys_connection = [x for x in under if isinstance(x, SystemConnectionCell)]
        if event.button() == Qt.LeftButton:
            if self.rubber_band.isVisible():
                self.rubber_band.hide()
                rect = self.rubber_band.geometry()
                rect_scene = self.mapToScene(rect).boundingRect()
                selected = self.scene().items(rect_scene)
                selected = [x for x in selected if isinstance(x, LayoutCell)]
                if selected:
                    if self.scene().mode == LayoutMode.REMOVE_CELLS:
                        self.scene().disable_enable_cells(selected)
                    elif self.scene().mode == LayoutMode.ADD_CONVEYOR:
                        self.show_adding_context_menu(event.pos(), selected, multi=True)
            else:
                if self.scene().mode == LayoutMode.REMOVE_CELLS:
                    self.scene().disable_enable_cells(selected_point)
                if self.scene().mode == LayoutMode.ADD_CONVEYOR:
                    if len(selected_sys_connection) >= 1:
                        self.scene().add_highlight_sys_conn(selected_sys_connection[0].syscon)
                        self.scene().reset()

        elif event.button() == Qt.RightButton and selected_point and not selected_conveyor:
            if self.scene().mode == LayoutMode.ADD_CONVEYOR:
                self.show_adding_context_menu(event.pos(), selected_point)
        elif event.button() == Qt.MiddleButton:
            self.fitInView(0, 0,
                           (self.scene().conv_layout.length / self.scene().conv_layout._cell_size.value) * CELL_ZOOM,
                           (self.scene().conv_layout.width / self.scene().conv_layout._cell_size.value) * CELL_ZOOM,
                           Qt.KeepAspectRatio)

        super().mouseReleaseEvent(event)

    def show_adding_context_menu(self, pos: QPoint, cells: List[LayoutCell], multi: int = False) -> None:
        """

        :param pos:
        :param cells:
        :return:
        """
        menu = QMenu("Context menu", self)
        empty = True
        cell_positions: List[Tuple[int, int]] = []
        for cell in cells:
            cell_positions.append((cell.x_cord, cell.y_cord))
            if not self.scene().cell_is_empty(cell.x_cord, cell.y_cord):
                empty = False
        if empty and not multi:
            action_add_res = QAction("Add restricted area", self)
            action_add_res.triggered.connect(
                functools.partial(self.scene().add_restricted_areas, areas=cell_positions))
            menu.addAction(action_add_res)
            action_add_work = QAction("Add workplace", self)
            action_add_work.triggered.connect(
                functools.partial(self.scene().add_workplaces, workplaces=cell_positions))
            menu.addAction(action_add_work)
            if self.scene().check_system_connection_position(cell_positions[0][0], cell_positions[0][1]):
                self.add_sink_source_to_menu(menu, cell_positions)
            for conv in self.scene().conv_layout.get_available_conveyor():
                action_add_conv = QAction("Add {}".format(conv), self)
                action_add_conv.triggered.connect(
                    functools.partial(self.scene().add_conveyor, x=cell_positions[0][0], y=cell_positions[0][1],
                                      name=conv))
                menu.addAction(action_add_conv)
        elif empty and multi:
            action_add_res = QAction("Add restricted areas", self)
            action_add_res.triggered.connect(
                functools.partial(self.scene().add_restricted_areas, areas=cell_positions))
            menu.addAction(action_add_res)
            action_add_work = QAction("Add workplace", self)
            action_add_work.triggered.connect(
                functools.partial(self.scene().add_workplaces, workplaces=cell_positions))
            menu.addAction(action_add_work)
        elif not multi and self.scene().check_system_connection_position(cell_positions[0][0], cell_positions[0][1]):
            self.add_sink_source_to_menu(menu, cell_positions)
        else:
            action_delete = QAction("Remove", self)
            action_delete.triggered.connect(
                functools.partial(self.scene().remove_on_positions, positions=cell_positions))
            menu.addAction(action_delete)
        menu.exec_(self.mapToGlobal(pos))

    def add_sink_source_to_menu(self, menu, cell_positions):
        if not self.scene().is_sink_source(cell_positions[0][0], cell_positions[0][1]):
            action_add_sink = QAction("Add sink", self)
            action_add_sink.triggered.connect(
                functools.partial(self.scene().add_sink_source, cell_positions[0][0], cell_positions[0][1], SystemConnectionType.SINK))
            menu.addAction(action_add_sink)
            action_add_source = QAction("Add source", self)
            action_add_source.triggered.connect(
                functools.partial(self.scene().add_sink_source, cell_positions[0][0], cell_positions[0][1], SystemConnectionType.SOURCE))
            menu.addAction(action_add_source)
