from PySide6.QtCore import Qt
from PySide6.QtGui import QBrush
from PySide6.QtWidgets import QGraphicsRectItem

from optilay.gui.util.colors import EMPTY_CELL, RED, BLACK, BLUE, PATH_CELL, PATH_HIGHLIGHT_CELL


class LayoutCell(QGraphicsRectItem):
    """
    This Object representing a cell on the grid.
    Its x and y coordinates define where in the grid the cell resides.
    """

    def __init__(self, x_c, y_c, *args, **kwargs):
        QGraphicsRectItem.__init__(self, *args, **kwargs)

        # Position inside the layout matrix
        self.x_cord = x_c
        self.y_cord = y_c

        # Color of this cell
        self.color_empty()

    def color_restriction(self):
        self.setBrush(QBrush(RED, Qt.DiagCrossPattern))

    def color_workplace(self):
        self.setBrush(QBrush(BLUE, Qt.DiagCrossPattern))

    def color_empty(self):
        self.setBrush(QBrush(EMPTY_CELL))

    def color_deactivated(self):
        self.setBrush(QBrush(BLACK))

    def color_path(self):
        self.setBrush(QBrush(PATH_CELL))

    def color_path_highlight(self):
        self.setBrush(QBrush(PATH_HIGHLIGHT_CELL))
