import functools
from typing import Optional

import PySide6.QtWidgets
from PySide6.QtCore import Qt
from PySide6.QtGui import QBrush, QAction
from PySide6.QtWidgets import QGraphicsRectItem, QMenu, QGraphicsSceneMouseEvent

from optilay.gui.util.colors import EMPTY_CELL, GREEN, ORANGE
from optilay.gui.util.layout_mode import LayoutMode
from optilay.gui.util.system_connection_draw_direction import SystemConnectionDrawDirection
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType


class SystemConnectionCell(QGraphicsRectItem):
    """
    """

    def __init__(self, x_c, y_c, syscon: SystemConnection, direction: SystemConnectionDrawDirection, *args, **kwargs):
        QGraphicsRectItem.__init__(self, *args, **kwargs)

        # Position inside the layout matrix
        self.x_cord = x_c
        self.y_cord = y_c
        self.syscon = syscon
        self.direction = direction

        # Color of this cell
        self.setBrush(QBrush(EMPTY_CELL))
        self.setToolTip(str(self.syscon.tool_tip()))

    def paint(self, painter: PySide6.QtGui.QPainter, option: PySide6.QtWidgets.QStyleOptionGraphicsItem,
              widget: Optional[PySide6.QtWidgets.QWidget] = ...) -> None:
        super().paint(painter, option, widget)
        if self.syscon.type == SystemConnectionType.SOURCE:
            painter.setPen(GREEN)
            painter.drawEllipse(self.boundingRect().center(), self.boundingRect().width() / 3,
                                self.boundingRect().height() / 3)
            pen = painter.pen()
            p_width = pen.width()
            pen.setWidth(5)
            pen.setCapStyle(Qt.RoundCap)
            painter.setPen(pen)
            painter.drawPoint(self.boundingRect().center())
            pen.setWidth(p_width)
            painter.setPen(pen)
        elif self.syscon.type == SystemConnectionType.SINK:
            painter.setPen(ORANGE)
            painter.drawEllipse(self.boundingRect().center(), self.boundingRect().width() / 3,
                                self.boundingRect().height() / 3)
            painter.drawLine(self.boundingRect().x() + (1 / 3) * self.boundingRect().width(),
                             self.boundingRect().y() + (1 / 3) * self.boundingRect().height(),
                             self.boundingRect().x() + (2 / 3) * self.boundingRect().width(),
                             self.boundingRect().y() + (2 / 3) * self.boundingRect().height())

            painter.drawLine(self.boundingRect().x() + (2 / 3) * self.boundingRect().width(),
                             self.boundingRect().y() + (1 / 3) * self.boundingRect().height(),
                             self.boundingRect().x() + (1 / 3) * self.boundingRect().width(),
                             self.boundingRect().y() + (2 / 3) * self.boundingRect().height())

        width_relation = 1 / 16
        height_relation = 1 / 10
        if self.direction == SystemConnectionDrawDirection.TOP:
            self.draw_top_arrow(height_relation, painter, width_relation)
        elif self.direction == SystemConnectionDrawDirection.RIGHT:
            self.draw_right_arrow(height_relation, painter, width_relation)
        elif self.direction == SystemConnectionDrawDirection.BOTTOM:
            self.draw_bottom_arrow(height_relation, painter, width_relation)
        elif self.direction == SystemConnectionDrawDirection.LEFT:
            self.draw_left_arrow(height_relation, painter, width_relation)

    def draw_left_arrow(self, height_relation, painter, width_relation):
        painter.drawLine(self.boundingRect().x() + (1 / 3) * self.boundingRect().width(),
                         self.boundingRect().center().y(),
                         self.boundingRect().x() + 2 * self.pen().width(),
                         self.boundingRect().center().y())
        if self.syscon.type == SystemConnectionType.SOURCE:
            painter.drawLine(self.boundingRect().x() + height_relation * self.boundingRect().width(),
                             self.boundingRect().center().y() - width_relation * self.boundingRect().width(),
                             self.boundingRect().x() + 2 * self.pen().width(),
                             self.boundingRect().center().y())
            painter.drawLine(self.boundingRect().x() + height_relation * self.boundingRect().width(),
                             self.boundingRect().center().y() + width_relation * self.boundingRect().width() + self.pen().width(),
                             self.boundingRect().x() + 2 * self.pen().width(),
                             self.boundingRect().center().y())
        if self.syscon.type == SystemConnectionType.SINK:
            painter.drawLine(self.boundingRect().x() + (1 / 3) * self.boundingRect().width() - height_relation * self.boundingRect().width(),
                             self.boundingRect().center().y() - width_relation * self.boundingRect().width(),
                             self.boundingRect().x() + (1 / 3) * self.boundingRect().width(),
                             self.boundingRect().center().y())
            painter.drawLine(self.boundingRect().x() + (1 / 3) * self.boundingRect().width() - height_relation * self.boundingRect().width(),
                             self.boundingRect().center().y() + width_relation * self.boundingRect().width() + self.pen().width(),
                             self.boundingRect().x() + (1 / 3) * self.boundingRect().width(),
                             self.boundingRect().center().y())

    def draw_bottom_arrow(self, height_relation, painter, width_relation):
        painter.drawLine(self.boundingRect().center().x(),
                         self.boundingRect().y() + (2 / 3) * self.boundingRect().height(),
                         self.boundingRect().center().x(),
                         self.boundingRect().y() + self.boundingRect().height() - 2 * self.pen().width())
        if self.syscon.type == SystemConnectionType.SOURCE:
            painter.drawLine(self.boundingRect().center().x() - width_relation * self.boundingRect().width(),
                             self.boundingRect().y() + self.boundingRect().height() - height_relation * self.boundingRect().height(),
                             self.boundingRect().center().x(),
                             self.boundingRect().y() + self.boundingRect().height() - 2 * self.pen().width())
            painter.drawLine(
                self.boundingRect().center().x() + width_relation * self.boundingRect().width() + self.pen().width(),
                self.boundingRect().y() + self.boundingRect().height() - height_relation * self.boundingRect().height(),
                self.boundingRect().center().x(),
                self.boundingRect().y() + self.boundingRect().height() - 2 * self.pen().width())
        if self.syscon.type == SystemConnectionType.SINK:
            painter.drawLine(self.boundingRect().center().x() - width_relation * self.boundingRect().width(),
                             self.boundingRect().y() + (2 / 3) * self.boundingRect().height() + height_relation * self.boundingRect().height(),
                             self.boundingRect().center().x(),
                             self.boundingRect().y() + (2 / 3) * self.boundingRect().height())
            painter.drawLine(
                self.boundingRect().center().x() + width_relation * self.boundingRect().width() + self.pen().width(),
                self.boundingRect().y() + (2 / 3) * self.boundingRect().height() + height_relation * self.boundingRect().height(),
                self.boundingRect().center().x(),
                self.boundingRect().y() + (2 / 3) * self.boundingRect().height())

    def draw_top_arrow(self, height_relation, painter, width_relation):
        painter.drawLine(self.boundingRect().center().x(),
                         self.boundingRect().y() + (1 / 3) * self.boundingRect().height(),
                         self.boundingRect().center().x(),
                         self.boundingRect().y() + 2 * self.pen().width())
        if self.syscon.type == SystemConnectionType.SOURCE:
            painter.drawLine(self.boundingRect().center().x() - width_relation * self.boundingRect().width(),
                             self.boundingRect().y() + height_relation * self.boundingRect().height(),
                             self.boundingRect().center().x(),
                             self.boundingRect().y() + 2 * self.pen().width())
            painter.drawLine(
                self.boundingRect().center().x() + width_relation * self.boundingRect().width() + self.pen().width(),
                self.boundingRect().y() + height_relation * self.boundingRect().height(),
                self.boundingRect().center().x(),
                self.boundingRect().y() + 2 * self.pen().width())
        elif self.syscon.type == SystemConnectionType.SINK:
            painter.drawLine(self.boundingRect().center().x() - width_relation * self.boundingRect().width(),
                             self.boundingRect().y() + (
                                         1 / 3) * self.boundingRect().height() - height_relation * self.boundingRect().height(),
                             self.boundingRect().center().x(),
                             self.boundingRect().y() + (1 / 3) * self.boundingRect().height())
            painter.drawLine(
                self.boundingRect().center().x() + width_relation * self.boundingRect().width() + self.pen().width(),
                self.boundingRect().y() + (
                            1 / 3) * self.boundingRect().height() - height_relation * self.boundingRect().height(),
                self.boundingRect().center().x(),
                self.boundingRect().y() + (1 / 3) * self.boundingRect().height())

    def draw_right_arrow(self, height_relation, painter, width_relation):
        painter.drawLine(self.boundingRect().x() + (2 / 3) * self.boundingRect().width(),
                         self.boundingRect().center().y(),
                         self.boundingRect().x() + self.boundingRect().width() - 2 * self.pen().width(),
                         self.boundingRect().center().y())
        if self.syscon.type == SystemConnectionType.SOURCE:
            painter.drawLine(
                self.boundingRect().x() + self.boundingRect().width() - height_relation * self.boundingRect().width(),
                self.boundingRect().center().y() - width_relation * self.boundingRect().width(),
                self.boundingRect().x() + self.boundingRect().width() - 2 * self.pen().width(),
                self.boundingRect().center().y())
            painter.drawLine(
                self.boundingRect().x() + self.boundingRect().width() - height_relation * self.boundingRect().width(),
                self.boundingRect().center().y() + width_relation * self.boundingRect().width() + self.pen().width(),
                self.boundingRect().x() + self.boundingRect().width() - 2 * self.pen().width(),
                self.boundingRect().center().y())
        elif self.syscon.type == SystemConnectionType.SINK:
            painter.drawLine(
                self.boundingRect().x() + (
                        2 / 3) * self.boundingRect().width() + height_relation * self.boundingRect().width(),
                self.boundingRect().center().y() - width_relation * self.boundingRect().width(),
                self.boundingRect().x() + (2 / 3) * self.boundingRect().width(),
                self.boundingRect().center().y())
            painter.drawLine(
                self.boundingRect().x() + (
                        2 / 3) * self.boundingRect().width() + height_relation * self.boundingRect().width(),
                self.boundingRect().center().y() + width_relation * self.boundingRect().width() + self.pen().width(),
                self.boundingRect().x() + (2 / 3) * self.boundingRect().width(),
                self.boundingRect().center().y())

    def mousePressEvent(self, event: QGraphicsSceneMouseEvent) -> None:
        """

        :param event:
        :return: None
        """
        if self.scene().mode == LayoutMode.ADD_CONVEYOR:
            if event.button() == Qt.RightButton:
                menu = QMenu("SystemConnection menu")
                add_goods = QAction("Add goods")
                add_goods.triggered.connect(functools.partial(self.scene().sink_source_add_goods, self.syscon))
                menu.addAction(add_goods)
                if self.scene().syscon_having_goods(self.syscon):
                    remove_goods = QAction("Remove goods")
                    remove_goods.triggered.connect(
                        functools.partial(self.scene().sink_source_remove_goods, self.syscon))
                    menu.addAction(remove_goods)
                delete = QAction("Remove {}".format(self.syscon.type.name.lower()))
                delete.triggered.connect(functools.partial(self.scene().remove_sink_source, self.x_cord, self.y_cord))
                menu.addAction(delete)
                menu.exec_(event.screenPos())
