import functools

from PySide6.QtGui import QBrush, Qt, QAction
from PySide6.QtWidgets import QGraphicsRectItem, QGraphicsItem, QGraphicsSceneMouseEvent, QMenu

from optilay.gui.objects.layout_cell import LayoutCell
from optilay.gui.util.colors import GREY
from optilay.gui.util.layout_mode import LayoutMode
from optilay.model.conveyor.conveyor import Conveyor
from optilay.model.conveyor.conveyor_direction import ConveyorDirection
from optilay.model.conveyor.conveyor_type import ConveyorType
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType


class ConveyorCell(QGraphicsRectItem):
    def __init__(self, conve: Conveyor, *args, **kwargs) -> None:
        QGraphicsRectItem.__init__(self, *args, **kwargs)
        self.conv = conve
        self.color_conveyor(self.conv.trans_direction, self.conv.rotation)
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.setToolTip(str(self.conv.tool_tip()))

    def color_conveyor_conventional_horr(self):
        self.setBrush(QBrush(GREY, Qt.HorPattern))

    def color_conveyor_conventional_vert(self):
        self.setBrush(QBrush(GREY, Qt.VerPattern))

    def color_conveyor_omnidirectional(self):
        self.setBrush(QBrush(GREY, Qt.DiagCrossPattern))

    def color_conveyor_fourway(self):
        self.setBrush(QBrush(GREY, Qt.CrossPattern))

    def color_conveyor(self, d_type: ConveyorDirection, rotation: int = 0):
        if d_type == ConveyorDirection.Omnidirectional:
            self.color_conveyor_omnidirectional()
        elif d_type == ConveyorDirection.ForwardBackward:
            if rotation == 1 or rotation == 3:
                self.color_conveyor_conventional_horr()
            else:
                self.color_conveyor_conventional_vert()
        elif d_type == ConveyorDirection.FourWayIntersection:
            self.color_conveyor_fourway()

    def mouseReleaseEvent(self, event: QGraphicsSceneMouseEvent) -> None:
        """
        On right click open menu with edit options for the positioning of this facility
        :param event:
        :return: None
        """
        if self.scene().mode == LayoutMode.ADD_CONVEYOR:
            if event.button() == Qt.RightButton:
                menu = QMenu("Conveyor menu")
                rotate90 = QAction("Rotate 90°")
                rotate90.triggered.connect(functools.partial(self.rotate, rotation=1))
                menu.addAction(rotate90)
                rotate180 = QAction("Rotate 180°")
                rotate180.triggered.connect(functools.partial(self.rotate, rotation=2))
                menu.addAction(rotate180)
                delete = QAction("Remove")
                delete.triggered.connect(self.delete)
                menu.addAction(delete)

                coll_item = self.scene().collidingItems(self)
                coll_item = [item for item in coll_item if isinstance(item, LayoutCell)]
                if len(coll_item) > 1:
                    for i, cell in enumerate(coll_item):
                        if self.scene().check_system_connection_position(cell.x_cord,
                                                                         cell.y_cord) and not self.scene().is_sink_source(
                                cell.x_cord, cell.y_cord):
                            action_add_sink = QAction("Add sink ({}, {})".format(cell.x_cord + 1, cell.y_cord + 1),
                                                      menu)
                            action_add_sink.triggered.connect(
                                functools.partial(self.scene().add_sink_source, cell.x_cord, cell.y_cord,
                                                  SystemConnectionType.SINK))
                            menu.addAction(action_add_sink)
                            action_add_source = QAction("Add source ({}, {})".format(cell.x_cord + 1, cell.y_cord + 1),
                                                        menu)
                            action_add_source.triggered.connect(
                                functools.partial(self.scene().add_sink_source, cell.x_cord, cell.y_cord,
                                                  SystemConnectionType.SOURCE))
                            menu.addAction(action_add_source)
                elif len(coll_item) == 1:
                    cell = coll_item[0]
                    if self.scene().check_system_connection_position(cell.x_cord,
                                                                     cell.y_cord) and not self.scene().is_sink_source(
                            cell.x_cord, cell.y_cord):
                        action_add_sink = QAction("Add sink")
                        action_add_sink.triggered.connect(
                            functools.partial(self.scene().add_sink_source, cell.x_cord, cell.y_cord,
                                              SystemConnectionType.SINK))
                        menu.addAction(action_add_sink)
                        action_add_source = QAction("Add source")
                        action_add_source.triggered.connect(
                            functools.partial(self.scene().add_sink_source, cell.x_cord, cell.y_cord,
                                              SystemConnectionType.SOURCE))
                        menu.addAction(action_add_source)

                menu.exec_(event.screenPos())
            else:
                try:
                    self.change_position()
                except:
                    pass
                finally:
                    self.scene().reset()

    def change_position(self) -> None:
        top_left = self.mapToScene(self.boundingRect().topLeft())
        cell = self.scene().items(top_left)[0]
        self.conv.position = [cell.y_cord, cell.x_cord]

    def rotate(self, rotation: int):
        rotation += self.conv.rotation
        self.conv.rotation = rotation % 4
        self.scene().reset()

    def delete(self):
        self.scene().remove_on_positions([(self.conv.x, self.conv.y)])
