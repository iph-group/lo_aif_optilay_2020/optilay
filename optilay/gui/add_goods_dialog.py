from PySide6.QtCore import Slot
from PySide6.QtWidgets import QDialog

from optilay.gui.generated.add_goods_dialog_gen import Ui_add_goods_dialog
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType


class AddGoodsDialog(QDialog):
    """

    """

    def __init__(self, syscon: SystemConnection, conv_lay: ConveyorSystemLayout, *args, **kwargs) -> None:
        """

        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_add_goods_dialog()
        self.ui.setupUi(self)
        self.conv_lay = conv_lay

        if syscon.type == SystemConnectionType.SINK:
            self.ui.label_3.setText("Source")
            self.syscons = self.conv_lay.get_sources()
            # +1 so that the coordiants for the user start at 1
            self.ui.label_actual.setText(
                "Current sink: [{}, {}], ID: {}".format(syscon.connected_position[1] + 1,
                                                        syscon.connected_position[0] + 1,
                                                        syscon.id))
        else:
            self.ui.label_3.setText("Sink")
            self.syscons = self.conv_lay.get_sinks()
            self.ui.label_actual.setText(
                "Current source: [{}, {}], ID: {}".format(syscon.connected_position[1] + 1,
                                                          syscon.connected_position[0] + 1,
                                                          syscon.id))

        self.curr_syscon = self.syscons[0]

        self.ui.comboBox_sink_source.addItems(["[" + str(conv.connected_position[1] + 1) + ', ' + str(
            conv.connected_position[0] + 1) + '], ID: ' + str(conv.id) for conv in self.syscons])

        self.ui.comboBox_goods_type.addItems(conv_lay.get_available_goods())
        self.update_goods(self.ui.comboBox_goods_type.currentText())

        self.ui.comboBox_goods_type.currentTextChanged.connect(self.update_goods)
        self.ui.comboBox_sink_source.currentIndexChanged.connect(self.update_curr_syscon)

    @Slot()
    def update_curr_syscon(self, idx: int):
        self.curr_syscon = self.syscons[idx]

    @Slot()
    def update_goods(self, new_text: str) -> None:
        new_conv = self.conv_lay.get_goods_information(new_text)
        self.ui.spinBox_load.setMaximum(new_conv['max_weight'] - new_conv['empty_weight'])
        self.ui.label_4.setText("Load [g] (max: {}g)".format(new_conv['max_weight'] - new_conv['empty_weight']))
