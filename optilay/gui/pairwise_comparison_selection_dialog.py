import logging

from PySide6.QtWidgets import QDialog, QListWidgetItem

from optilay.gui.generated.pairwise_comparison_selection_dialog_gen import Ui_PairwiseComparisonSelectionDialog
from optilay.model.weighting import Weighting


class PairwiseComparisonSelectionDialog(QDialog):
    """
    In the PairwiseComparisonSelectionDialog the user can select the attributes of the weighting between which he
    wants to perform a PairwiseComparison.
    """

    def __init__(self, weighting: Weighting, *args, **kwargs) -> None:
        """

        :param weighting: Weighting class where all available attributes are included.
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_PairwiseComparisonSelectionDialog()
        self.ui.setupUi(self)

        self.attr_name = {}
        self.weighting = weighting

        for attr, _ in self.weighting.__dict__.items():
            self.attr_name[attr] = attr
            QListWidgetItem(attr, self.ui.listWidget_left)

        self.ui.pushButton_right.clicked.connect(self.move_right)
        self.ui.pushButton_left.clicked.connect(self.move_left)

        logging.info("Loaded PairwiseComparisonSelectionDialog")

    def move_right(self) -> None:
        """
        Moves the selected element to the right list. (no criteria)
        :return: None
        """
        if self.ui.listWidget_left.count() > 2:
            curr_item = self.ui.listWidget_left.currentItem()
            if curr_item:
                self.ui.listWidget_right.addItem(QListWidgetItem(curr_item.text()))
                self.ui.listWidget_left.takeItem(self.ui.listWidget_left.row(curr_item))
                del curr_item

    def move_left(self) -> None:
        """
        Moves the selected element to the left list. (criteria)
        :return: None
        """
        curr_item = self.ui.listWidget_right.currentItem()
        if curr_item:
            self.ui.listWidget_left.addItem(QListWidgetItem(curr_item.text()))
            self.ui.listWidget_right.takeItem(self.ui.listWidget_right.row(curr_item))
            del curr_item
