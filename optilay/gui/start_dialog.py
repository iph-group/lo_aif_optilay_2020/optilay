from PySide6.QtCore import Qt
from PySide6.QtGui import QKeyEvent
from PySide6.QtWidgets import QDialog

from optilay.gui.generated.start_dialog_gen import Ui_start_dialog


class StartDialog(QDialog):
    """Dialog which is starting with MainWindow where the user can start a new LayoutModel
    or load an saved LayoutModel"""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.ui = Ui_start_dialog()
        self.ui.setupUi(self)
        self.ui.load_layout_button.clicked.connect(self.set_load)
        self.ui.new_layout_button.clicked.connect(self.set_new)

        self.new: bool = True

    def keyPressEvent(self, arg__1: QKeyEvent) -> None:
        """
        Switch between buttons with the up and down arrow.
        :param arg__1:
        :return: None
        """
        if arg__1.key() == Qt.Key_Up:
            self.ui.new_layout_button.setDefault(True)
        elif arg__1.key() == Qt.Key_Down:
            self.ui.load_layout_button.setDefault(True)
        super().keyPressEvent(arg__1)

    def set_load(self) -> None:
        """

        :return: None
        """
        self.new = False
        self.accept()

    def set_new(self) -> None:
        """

        :return: None
        """
        self.accept()
