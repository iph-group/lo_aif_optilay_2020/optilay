from typing import List, Tuple

from PySide6.QtWidgets import QGraphicsScene, QInputDialog

from optilay.gui.add_goods_dialog import AddGoodsDialog
from optilay.gui.objects.conveyor_cell import ConveyorCell
from optilay.gui.objects.layout_cell import LayoutCell
from optilay.gui.objects.system_connection_cell import SystemConnectionCell
from optilay.gui.remove_goods_dialog import RemoveGoodsDialog
from optilay.gui.util.fixed_gui_values import CELL_ZOOM
from optilay.gui.util.layout_mode import LayoutMode
from optilay.gui.util.system_connection_draw_direction import SystemConnectionDrawDirection
from optilay.model.sytemconnection.system_connection import check_systemconnectionable_position, SystemConnection
from optilay.model.sytemconnection.system_connection_direction import SystemConnectionDirection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType
from optilay.model.util.cell_values import DEACTIVATED_AREA, RESTRICTED_AREA, WORKPLACE
from optilay.util.exceptions import NoAutoDirection


class LayoutScene(QGraphicsScene):
    """

    """

    def __init__(self, conv_layout, mode=LayoutMode.SHOW, *args, **kwargs) -> None:
        """

        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.cells: List[LayoutCell] = []
        self.syscon_cells: List[SystemConnectionCell] = []
        self.conveyors: List[ConveyorCell] = []
        self.conv_layout = conv_layout
        self.cell_zoom = CELL_ZOOM

        self.highlighted_sys_con: SystemConnection = None

        self.mode = mode

    def init_grid(self, with_hidden=False) -> None:
        """
        Initialises grid.
        :return: None
        """
        self.clear()
        self.generate_cells(with_hidden=with_hidden)
        self.generate_conveyor()
        self.generate_system_connections()
        self.generate_paths()

    def generate_cells(self, with_hidden=False) -> None:
        """
        Generated empty cells for the grid.
        :return: A List of FactoryCells.
        """
        layout = self.conv_layout.layout
        for x_cord in range(layout.shape[1]):
            for y_cord in range(layout.shape[0]):
                index = self.conv_layout.layout[y_cord][x_cord]
                if not (index == DEACTIVATED_AREA) or with_hidden:
                    cell = LayoutCell(x_cord, y_cord,
                                      x_cord * self.cell_zoom,
                                      y_cord * self.cell_zoom,
                                      self.cell_zoom,
                                      self.cell_zoom)
                    pen = cell.pen()
                    pen.setWidth(1)
                    cell.setPen(pen)
                    if index == RESTRICTED_AREA:
                        cell.color_restriction()
                    elif index == WORKPLACE:
                        cell.color_workplace()
                    elif index == DEACTIVATED_AREA:
                        cell.color_deactivated()
                    self.addItem(cell)
                    self.cells.append(cell)

    def generate_conveyor(self):
        for conveyor in self.conv_layout.conveyors:
            for cell in self.cells:
                if cell.x_cord == conveyor.x and cell.y_cord == conveyor.y:
                    conveyor_cell = ConveyorCell(conveyor, cell.boundingRect().topLeft().x() + 1.5,
                                                 cell.boundingRect().topLeft().y() + 1.5,
                                                 conveyor.length * cell.boundingRect().width() - conveyor.length - 2,
                                                 conveyor.width * cell.boundingRect().height() - conveyor.width - 2,
                                                 )
                    self.addItem(conveyor_cell)
                    self.conveyors.append(conveyor_cell)
                    break

    def generate_system_connections(self):
        layout = self.conv_layout.layout
        for syscon in self.conv_layout.connection:
            offset = [0, 0]
            direction = None
            if syscon.direction == SystemConnectionDirection.Vertical:
                if syscon.connected_position[0] == 0 or layout[
                    syscon.connected_position[0] - 1, syscon.connected_position[1]] == DEACTIVATED_AREA:
                    offset[0] = -1
                    direction = SystemConnectionDrawDirection.BOTTOM
                elif syscon.connected_position[0] == layout.shape[0] - 1 or layout[
                    syscon.connected_position[0] + 1, syscon.connected_position[1]] == DEACTIVATED_AREA:
                    offset[0] = 1
                    direction = SystemConnectionDrawDirection.TOP
            elif syscon.direction == SystemConnectionDirection.Horizontal:
                if syscon.connected_position[1] == 0 or layout[
                    syscon.connected_position[0], syscon.connected_position[1] - 1] == DEACTIVATED_AREA:
                    offset[1] = -1
                    direction = SystemConnectionDrawDirection.RIGHT
                elif syscon.connected_position[1] == layout.shape[1] - 1 or layout[
                    syscon.connected_position[0], syscon.connected_position[1] + 1] == DEACTIVATED_AREA:
                    offset[1] = 1
                    direction = SystemConnectionDrawDirection.LEFT
            else:
                raise ValueError("SystemConnectionDirection not placeable")
            syscon_cell = SystemConnectionCell(syscon.connected_position[1], syscon.connected_position[0],
                                               syscon, direction,
                                               (syscon.connected_position[1] + offset[1]) * self.cell_zoom,
                                               (syscon.connected_position[0] + offset[0]) * self.cell_zoom,
                                               self.cell_zoom,
                                               self.cell_zoom)
            self.addItem(syscon_cell)
            self.syscon_cells.append(syscon_cell)

    def generate_paths(self):
        all_path_cell = []
        highlight_path_cell = []
        for stream in self.conv_layout.packet_stream:
            if stream.source == self.highlighted_sys_con or stream.sink == self.highlighted_sys_con:
                highlight_path_cell += stream.get_path()
            else:
                all_path_cell += stream.get_path()
        for cell in self.cells:
            if [cell.x_cord, cell.y_cord] in all_path_cell:
                cell.color_path()
            if [cell.x_cord, cell.y_cord] in highlight_path_cell:
                cell.color_path_highlight()

    def disable_enable_cells(self, cells: List[LayoutCell]) -> None:
        """

        :param cells:
        :return:
        """
        current_layout = self.conv_layout.layout

        for cell in cells:
            if current_layout[cell.y_cord][cell.x_cord] == -99:
                cell.color_empty()
                self.conv_layout.remove_deactivated_area(cell.x_cord, cell.y_cord)
            else:
                cell.color_deactivated()
                self.conv_layout.add_deactivated_area(cell.x_cord, cell.y_cord)

    def cell_is_empty(self, x: int, y: int) -> bool:
        return self.conv_layout.cell_is_empty(x, y)

    def remove_on_positions(self, positions: List[Tuple[int, int]]) -> None:
        for position in positions:
            self.conv_layout.remove_on_position(position[0], position[1])
        self.reset()

    def add_restricted_area(self, x: int, y: int):
        self.conv_layout.add_restricted_area(x, y)
        self.reset()

    def add_restricted_areas(self, areas: List[Tuple[int, int]]):
        for area in areas:
            self.conv_layout.add_restricted_area(area[0], area[1])
        self.reset()

    def add_workplaces(self, workplaces: List[Tuple[int, int]]):
        for workplace in workplaces:
            self.conv_layout.add_workplace(workplace[0], workplace[1])
        self.reset()

    def add_conveyor(self, x: int, y: int, name: str):
        self.conv_layout.add_conveyor(name, x, y)
        self.reset()

    def check_system_connection_position(self, x: int, y: int):
        try:
            check_systemconnectionable_position(x, y, self.conv_layout.layout)
        except:
            return False
        return True

    def add_sink_source(self, x: int, y: int, c_type: SystemConnectionType):
        try:
            self.conv_layout.add_system_connection(x, y, c_type)
        except NoAutoDirection:
            directions = ['Horizontal', 'Vertical']
            item, ok = QInputDialog.getItem(self.parent(), "Choose input direction",
                                            "Several input directions are possible at the selected position. Please select the appropriate direction.",
                                            directions, 0, False)
            if item == directions[0]:
                self.conv_layout.add_system_connection(x, y, c_type, direction=SystemConnectionDirection.Horizontal)
            elif item == directions[1]:
                self.conv_layout.add_system_connection(x, y, c_type, direction=SystemConnectionDirection.Vertical)
        self.reset()

    def remove_sink_source(self, x: int, y: int):
        self.conv_layout.remove_system_connection(x, y)
        self.reset()

    def is_sink_source(self, x: int, y: int) -> bool:
        return self.conv_layout.cell_is_sink_source(x, y)

    def sink_source_add_goods(self, syscon: SystemConnection) -> None:
        raw_data_table_dialog = AddGoodsDialog(syscon, conv_lay=self.conv_layout)
        raw_data_table_dialog.setModal(True)
        raw_data_table_dialog.show()
        if raw_data_table_dialog.exec():
            if syscon.type == SystemConnectionType.SINK:
                self.conv_layout.add_goods_between(syscon, raw_data_table_dialog.curr_syscon,
                                                   raw_data_table_dialog.ui.comboBox_goods_type.currentText(),
                                                   load=raw_data_table_dialog.ui.spinBox_load.value(),
                                                   amount=raw_data_table_dialog.ui.spinBox_amount.value())
            else:
                self.conv_layout.add_goods_between(raw_data_table_dialog.curr_syscon, syscon,
                                                   raw_data_table_dialog.ui.comboBox_goods_type.currentText(),
                                                   load=raw_data_table_dialog.ui.spinBox_load.value(),
                                                   amount=raw_data_table_dialog.ui.spinBox_amount.value())

    def syscon_having_goods(self, syscon: SystemConnection) -> bool:
        return self.conv_layout.systemconnection_having_goods(syscon)

    def sink_source_remove_goods(self, syscon: SystemConnection) -> None:
        delete_goods_dialog = RemoveGoodsDialog(syscon, self.conv_layout)
        delete_goods_dialog.setModal(True)
        delete_goods_dialog.show()
        delete_goods_dialog.exec()

    def add_highlight_sys_conn(self, highlight: SystemConnection):
        if self.highlighted_sys_con == highlight:
            self.highlighted_sys_con = None
        else:
            self.highlighted_sys_con = highlight

    def reset(self):
        self.clear()
        self.cells.clear()
        self.syscon_cells.clear()
        self.conveyors.clear()
        self.mode = LayoutMode.ADD_CONVEYOR
        self.init_grid()
