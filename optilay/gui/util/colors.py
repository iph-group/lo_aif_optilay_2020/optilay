"""
The colors module contains all the colors used
for displaying the FactoryCell inside the FactoryView
"""
# General
from PySide6.QtGui import QColor

BLACK = QColor(0, 0, 0)
IPH_RED = QColor(218, 37, 30)
WHITE = QColor(255, 255, 255)
RED = QColor(255, 0, 0)
ORANGE = QColor(255, 127, 0)
GREY = QColor(128, 128, 128)
GREEN = QColor(0, 255, 0)
BLUE = QColor(0, 0, 255)
# Colors for filling cells
EMPTY_CELL = QColor(255, 255, 255)
PATH_CELL = QColor(110, 110, 110)
PATH_CELL.setAlpha(64)
PATH_HIGHLIGHT_CELL = QColor(255, 196, 128)
PATH_HIGHLIGHT_CELL.setAlpha(64)
