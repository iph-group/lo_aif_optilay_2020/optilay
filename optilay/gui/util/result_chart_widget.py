from typing import List

from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget

from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.gui.util.spider_chart import SpiderChart


class ResultChartWidget(QWidget):
    """
    This Dialog shows the spider chart from SpiderChart
    """

    def __init__(self, data: List[EvaluationResult], *args, width=4, height=4, dpi=100, index=None,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)

        if not index:
            self.sc = SpiderChart(data, width, height, dpi)
        else:
            self.sc = SpiderChart(data, width, height, dpi, index=index)
        # self.sc = FigureCanvasQTAgg(Figure())
        # Create toolbar, passing canvas as first parament, parent (self, the MainWindow) as second.
        # self.toolbar = NavigationToolbar(self.sc, self)
        self.layout = QtWidgets.QHBoxLayout()
        # self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.sc)

        self.setLayout(self.layout)

    def update_canvas(self, data: List[EvaluationResult]) -> None:
        """
        Removes old data from diagram and created a new Diagram from the given EvaluationData.
        :param data: List of EvaluationData.
        :return: None
        """
        old_size = self.sc.old
        self.layout.removeWidget(self.sc)
        self.layout.removeWidget(self.toolbar)
        del self.sc
        del self.toolbar
        self.sc = SpiderChart(data, old_size[0], old_size[1], old_size[2])
        # self.toolbar = NavigationToolbar(self.sc, self)
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.sc)
