from enum import Enum


class SystemConnectionDrawDirection(Enum):
    """

    """
    TOP = 0
    RIGHT = 1
    BOTTOM = 2
    LEFT = 3