from PySide6.QtCore import QObject, Signal, Slot


class CalculationProgressReceiver(QObject):
    """
    The CalculationProgressReceiver runs as a thread and notifies the CalculationProgressDialog whenever new messages
    appear in the WriteStream.
    """
    new_message_signal = Signal(str)

    def __init__(self, msg_queue, cores, *args, **kwargs) -> None:
        """

        :param msg_queue: Queue where new messages are written to be listened to.
        :param cores: The number of cores the CalculationProgressReceiver should wait for.
        :param args:
        :param kwargs:
        """
        QObject.__init__(self, *args, **kwargs)
        self.msg_queue = msg_queue
        self.active_process = cores

    @Slot()
    def run(self) -> None:
        """
        Main event loop. Exit if "Done!" was written as many times into the msg_queue as cores.
        :return: None
        """
        while True:
            text = self.msg_queue.get()
            self.new_message_signal.emit(text)
            if text == 'Done!':
                self.active_process -= 1
                if self.active_process == 0:
                    break
