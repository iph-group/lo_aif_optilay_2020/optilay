from math import pi
from typing import List

from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg
from matplotlib.figure import Figure

from optilay.evaluation.evaluation_result import EvaluationResult


class SpiderChart(FigureCanvasQTAgg):
    """
    This class is for creating a spider chart with the given EvaluationResult
    """

    def __init__(self, data: List[EvaluationResult], width, height, dpi, parent=None, index=None):
        self.old = (width, height, dpi)
        fig = Figure(figsize=(width, height), dpi=dpi)

        catalog = ['Throughput [Goods/hour]', 'Lead time [s]', 'Space requirement [mm²]', 'Buffer capacity [Amount]',
                   'Costs [€]']

        # number of variable
        categories = catalog
        N = len(catalog)

        # What will be the angle of each axis in the plot? (we divide the plot / number of variable)
        angles = [n / float(N) * 2 * pi for n in range(N)]
        angles += angles[:1]

        # Initialise the spider plot
        self.axes = fig.add_subplot(111, polar=True)

        # If you want the first axis to be on top:
        self.axes.set_theta_offset(pi / 2)
        self.axes.set_theta_direction(-1)

        # Draw one axe per variable + add labels labels yet
        self.axes.set_xticks(angles[:-1])
        self.axes.set_xticklabels(categories)

        # Draw ylabels
        self.axes.set_rlabel_position(0)
        self.axes.set_ylim(0, 1)
        self.axes.set_yticks([0.2, 0.4, 0.6, 0.8, 1])
        self.axes.set_yticklabels(["0.2", "0.4", "0.6", "0.8", "1"], color="grey", size=7)

        # ------- PART 2: Add plots
        name_list = []
        if data:
            for i, eval_data in enumerate(data):
                self.add_eval(eval_data, angles, i)
                name_list.append("Layout {}".format(i))

        # Add legend
        if not index:
            fig.legend(loc='lower left', labels=name_list)
        else:
            fig.legend(loc='lower left', labels=["Layout {}".format(index)])

        super().__init__(fig)

    def add_eval(self, eval_data: EvaluationResult, angles, group) -> None:
        """
        Adds EvaluationResult to the spider chart.
        :param eval_data: The EvaluationResult which gets added
        :param angles: Angle for each axis in the plot
        :param group: The data belongs to.
        :return: None
        """
        values = [eval_data.throughput[1]/100, eval_data.lead_time[1]/100, eval_data.space_requirement[1]/100,
                  eval_data.buffer_capacity[1]/100, eval_data.costs[1]/100]
        values += values[:1]
        self.axes.plot(angles, values, linewidth=1, linestyle='solid',
                       label="group {}".format(group))
        #self.axes.fill(angles, values, 'r', alpha=0.1)
