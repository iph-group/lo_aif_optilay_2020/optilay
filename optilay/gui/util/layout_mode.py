from enum import Enum


class LayoutMode(Enum):
    """
    The LayoutMode class is an enumeration object,
    which contains the different modes of LayoutScene.
    """
    SHOW = 0
    REMOVE_CELLS = 1
    ADD_CONVEYOR = 2