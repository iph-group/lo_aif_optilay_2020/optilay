from PySide6.QtCore import Slot
from PySide6.QtGui import QTextCursor
from PySide6.QtWidgets import QTextBrowser, QVBoxLayout, QDialog, QProgressBar


class CalculationProgressDialog(QDialog):
    """
    This QDialog will be opened during optimisation calculation.
    Through this Dialog the user gets information about the ongoing calculations.
    """

    def __init__(self, cores, *args, **kwargs) -> None:
        """

        :param cores: Number auf processes which running in optimisation
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.resize(500, 200)
        self._console = QTextBrowser(self)
        self._progress_bar = QProgressBar(self)
        self.active_process = cores
        self.workers = cores

        self.progress_count = self.active_process * 100
        self._progress_bar.setValue(0)

        layout = QVBoxLayout()
        layout.addWidget(self._progress_bar)
        layout.addWidget(self._console)
        self.setLayout(layout)

        self._console.cursorPositionChanged.connect(self.check_end)

    @Slot(str)
    def append_text(self, text) -> None:
        """
        If Signal from CalculationProgressReceiver is emitted, this method gets triggered and appends the text to
        the output console. In addition, the "+1" is obeyed in order to display the progress in the QProgressBar. When
        "Done!" has appeared as often as processes have been started in the optimisation, the QDialog terminates.
        :param text: Text for appending
        :return: None
        """

        if text == 'Done!':
            self.active_process -= 1
            if self.active_process == 0:
                self.accept()
        elif text == '+1':
            self.progress_count -= 1
            if self.progress_count % self.workers == 0:
                self._progress_bar.setValue(self._progress_bar.value() + 1)
        else:
            self._console.insertPlainText(text)
            self._console.moveCursor(QTextCursor.End)

    def check_end(self) -> None:
        """
        Scrolls the QTextBrowser down after each entry.
        :return: None
        """
        self._console.verticalScrollBar().setValue(self._console.verticalScrollBar().maximum())
