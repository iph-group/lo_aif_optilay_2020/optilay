from PySide6.QtCore import Qt
from PySide6.QtWidgets import QDialog

from optilay.gui.generated.disable_cells_dialog_gen import Ui_disable_cells_dialog
from optilay.gui.layout_scene import LayoutScene
from optilay.gui.layout_view import LayoutView
from optilay.gui.util.fixed_gui_values import CELL_ZOOM
from optilay.gui.util.layout_mode import LayoutMode


class DisableCellsDialog(QDialog):
    """
    This QDialog is responsible for removing unnecessary FactoryCells from our FactoryModel.
    This is for resizing the factory.
    """

    def __init__(self, layout, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.ui = Ui_disable_cells_dialog()
        self.ui.setupUi(self)

        self.conv_layout = layout

        # initialise factory visualisation
        self.layout_scene = LayoutScene(self.conv_layout, LayoutMode.REMOVE_CELLS)

        self.layout_view = LayoutView(self)
        self.ui.vertical_layout.addWidget(self.layout_view)
        self.ui.vertical_layout.addWidget(self.ui.button_box)
        self.layout_view.setScene(self.layout_scene)

        self.layout_scene.init_grid(with_hidden=True)

        self.layout_view.setToolTip(self.tr(
            "With the left mouse button cells can be marked which will later be hidden.\n\nWhite = in use\nBlack = not in use"))

    def fit_view(self) -> None:
        """
        Fits the view into the given space in this dialog.
        :return: None
        """
        self.layout_view.fitInView(0, 0,
                                   (self.conv_layout.length / self.conv_layout._cell_size.value) * CELL_ZOOM,
                                   (self.conv_layout.width / self.conv_layout._cell_size.value) * CELL_ZOOM,
                                   Qt.KeepAspectRatio)
