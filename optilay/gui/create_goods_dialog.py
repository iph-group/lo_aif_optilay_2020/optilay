from PySide6.QtCore import Slot
from PySide6.QtWidgets import QDialog

from optilay.gui.generated.add_goods_dialog_gen import Ui_add_goods_dialog
from optilay.gui.generated.create_goods_dialog_gen import Ui_CreateGoodsDialog
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.goods.goods_factory import GoodsFactory
from optilay.model.goods.goods_type import GoodsType
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType


class CreateGoodsDialog(QDialog):
    """

    """

    def __init__(self, g_factory: GoodsFactory, *args, **kwargs) -> None:
        """

        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_CreateGoodsDialog()
        self.ui.setupUi(self)

        self.g_factory: GoodsFactory = g_factory

        self.ui.comboBox_type.addItems([g_type.name for g_type in GoodsType])

    def accept(self) -> None:
        name = self.ui.lineEdit_name.text()
        width = self.ui.spinBox_width.value()
        length = self.ui.spinBox_length.value()
        empty_weight = self.ui.spinBox_empty_weight.value()
        max_weight = self.ui.spinBox_maximum_weight.value()
        g_type = GoodsType[self.ui.comboBox_type.currentText()]
        self.g_factory.add_good(name=name, width=width, length=length, g_type=g_type, empty_weight=empty_weight,
                                max_weight=max_weight)
        super().accept()
