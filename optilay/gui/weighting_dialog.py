import logging

from PySide6.QtWidgets import QDialog, QMessageBox

from optilay.gui.generated.weighting_dialog_gen import Ui_WeightingDialog
from optilay.gui.pairwise_comparison_dialog import PairwiseComparisonDialog
from optilay.gui.pairwise_comparison_selection_dialog import PairwiseComparisonSelectionDialog
from optilay.model.weighting import Weighting


class WeightingDialog(QDialog):
    """
    In the WeightingDialog, all values in the weighting can be set by the user.
    """

    def __init__(self, weighting: Weighting, *args, **kwargs) -> None:
        """

        :param weighting: The weighting of the LayoutModel
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self.ui = Ui_WeightingDialog()
        self.ui.setupUi(self)
        self.weighting = weighting

        self.load_values()
        self.update_sum()

        self.ui.pushButton_balance.clicked.connect(self.equalise)
        self.ui.pushButton_pairwise.clicked.connect(self.pairwise_comparison)

        self.ui.spinBox_buffer_capacity.valueChanged.connect(self.update_sum)
        self.ui.spinBox_troughput.valueChanged.connect(self.update_sum)
        self.ui.spinBox_lead_time.valueChanged.connect(self.update_sum)
        self.ui.spinBox_space_requirement.valueChanged.connect(self.update_sum)
        self.ui.spinBox_cost.valueChanged.connect(self.update_sum)

        logging.info("Loaded Weighting Dialog")

    def load_values(self) -> None:
        """
        Loads all values from Weighting into the dialog.
        :return: None
        """
        self.ui.spinBox_troughput.setValue(self.weighting.throughput * 100)
        self.ui.spinBox_lead_time.setValue(self.weighting.lead_time * 100)
        self.ui.spinBox_space_requirement.setValue(self.weighting.space_requirement * 100)
        self.ui.spinBox_buffer_capacity.setValue(self.weighting.buffer_capacity * 100)
        self.ui.spinBox_cost.setValue(self.weighting.costs * 100)

    def set_values(self) -> None:
        """
        Writes all values from the dial back into the Weighting class.
        :return: None
        """
        self.weighting.throughput = self.ui.spinBox_troughput.value() / 100
        self.weighting.lead_time = self.ui.spinBox_lead_time.value() / 100
        self.weighting.space_requirement = self.ui.spinBox_space_requirement.value() / 100
        self.weighting.buffer_capacity = self.ui.spinBox_buffer_capacity.value() / 100
        self.weighting.costs = self.ui.spinBox_cost.value() / 100

    def equalise(self) -> None:
        """
        Adjust the Weighting so that all attributes are weighted as equally as possible.
        :return: None
        """
        self.weighting.equalise()
        self.load_values()

    def accept(self) -> None:
        """
        Checks whether the user has set everything correctly and then closes the WeightingDialog.
        :return: None
        """
        sum_val = self.update_sum()
        if sum_val != 100:
            errordialog = QMessageBox(self)
            errordialog.setText("Weighting error!")
            errordialog.setInformativeText(
                "The sum of the weights must be 100!\n Actual sum: {}".format(sum_val))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        else:
            self.set_values()
            super().accept()

    def update_sum(self) -> None:
        """
        When the user changes a weight, the sum of all weights is calculated to indicate to the user that they have set
        the weight correctly.
        :return: None
        """
        sum_val = self.ui.spinBox_buffer_capacity.value() + self.ui.spinBox_troughput.value() + self.ui.spinBox_cost.value() + self.ui.spinBox_lead_time.value() + self.ui.spinBox_space_requirement.value()
        self.ui.label_sum.setText(str(sum_val) + '/100')
        if sum_val > 100:
            self.ui.label_sum.setStyleSheet("font-weight: bold; color: red")
        elif sum_val < 100:
            self.ui.label_sum.setStyleSheet("font-weight: bold; color: red")
        else:
            self.ui.label_sum.setStyleSheet("")
        return sum_val

    def pairwise_comparison(self) -> None:
        """
        Starts the dialogue for a pairwise comparison.
        :return: None
        """
        compare_selection_dialog = PairwiseComparisonSelectionDialog(self.weighting)
        compare_selection_dialog.setModal(True)
        compare_selection_dialog.show()
        if compare_selection_dialog.exec():
            criteria = [compare_selection_dialog.ui.listWidget_left.item(x).text() for x in
                        range(compare_selection_dialog.ui.listWidget_left.model().rowCount())]
            pair_compare_dialog = PairwiseComparisonDialog(self.weighting, criteria, compare_selection_dialog.attr_name)
            pair_compare_dialog.setModal(True)
            pair_compare_dialog.show()
            pair_compare_dialog.exec()

            self.load_values()
