# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'disable_cells_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_disable_cells_dialog(object):
    def setupUi(self, disable_cells_dialog):
        if not disable_cells_dialog.objectName():
            disable_cells_dialog.setObjectName(u"disable_cells_dialog")
        disable_cells_dialog.resize(674, 480)
        self.vertical_layout = QVBoxLayout(disable_cells_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.button_box = QDialogButtonBox(disable_cells_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.vertical_layout.addWidget(self.button_box)


        self.retranslateUi(disable_cells_dialog)
        self.button_box.accepted.connect(disable_cells_dialog.accept)
        self.button_box.rejected.connect(disable_cells_dialog.reject)

        QMetaObject.connectSlotsByName(disable_cells_dialog)
    # setupUi

    def retranslateUi(self, disable_cells_dialog):
        disable_cells_dialog.setWindowTitle(QCoreApplication.translate("disable_cells_dialog", u"Disable unused cells", None))
    # retranslateUi

