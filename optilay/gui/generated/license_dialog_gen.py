# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'license_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_License_dialog(object):
    def setupUi(self, License_dialog):
        if not License_dialog.objectName():
            License_dialog.setObjectName(u"License_dialog")
        License_dialog.resize(714, 546)
        self.verticalLayout = QVBoxLayout(License_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.textEdit = QTextEdit(License_dialog)
        self.textEdit.setObjectName(u"textEdit")

        self.verticalLayout.addWidget(self.textEdit)


        self.retranslateUi(License_dialog)

        QMetaObject.connectSlotsByName(License_dialog)
    # setupUi

    def retranslateUi(self, License_dialog):
        License_dialog.setWindowTitle(QCoreApplication.translate("License_dialog", u"License", None))
    # retranslateUi

