# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'weighting_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_WeightingDialog(object):
    def setupUi(self, WeightingDialog):
        if not WeightingDialog.objectName():
            WeightingDialog.setObjectName(u"WeightingDialog")
        WeightingDialog.resize(346, 327)
        self.verticalLayout = QVBoxLayout(WeightingDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label = QLabel(WeightingDialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.label)

        self.formLayout_2 = QFormLayout()
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.label_2 = QLabel(WeightingDialog)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_2)

        self.label_3 = QLabel(WeightingDialog)
        self.label_3.setObjectName(u"label_3")

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_3)

        self.label_4 = QLabel(WeightingDialog)
        self.label_4.setObjectName(u"label_4")

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_4)

        self.label_5 = QLabel(WeightingDialog)
        self.label_5.setObjectName(u"label_5")

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole, self.label_5)

        self.label_6 = QLabel(WeightingDialog)
        self.label_6.setObjectName(u"label_6")

        self.formLayout_2.setWidget(4, QFormLayout.LabelRole, self.label_6)

        self.spinBox_troughput = QSpinBox(WeightingDialog)
        self.spinBox_troughput.setObjectName(u"spinBox_troughput")
        self.spinBox_troughput.setMaximum(100)

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.spinBox_troughput)

        self.spinBox_lead_time = QSpinBox(WeightingDialog)
        self.spinBox_lead_time.setObjectName(u"spinBox_lead_time")
        self.spinBox_lead_time.setMaximum(100)

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.spinBox_lead_time)

        self.spinBox_space_requirement = QSpinBox(WeightingDialog)
        self.spinBox_space_requirement.setObjectName(u"spinBox_space_requirement")
        self.spinBox_space_requirement.setMaximum(100)

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.spinBox_space_requirement)

        self.spinBox_buffer_capacity = QSpinBox(WeightingDialog)
        self.spinBox_buffer_capacity.setObjectName(u"spinBox_buffer_capacity")
        self.spinBox_buffer_capacity.setMaximum(100)

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.spinBox_buffer_capacity)

        self.spinBox_cost = QSpinBox(WeightingDialog)
        self.spinBox_cost.setObjectName(u"spinBox_cost")
        self.spinBox_cost.setMaximum(100)

        self.formLayout_2.setWidget(4, QFormLayout.FieldRole, self.spinBox_cost)


        self.verticalLayout_2.addLayout(self.formLayout_2)


        self.verticalLayout.addLayout(self.verticalLayout_2)

        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setFormAlignment(Qt.AlignHCenter|Qt.AlignTop)
        self.label_total = QLabel(WeightingDialog)
        self.label_total.setObjectName(u"label_total")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_total)

        self.label_sum = QLabel(WeightingDialog)
        self.label_sum.setObjectName(u"label_sum")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.label_sum)


        self.verticalLayout.addLayout(self.formLayout)

        self.pushButton_balance = QPushButton(WeightingDialog)
        self.pushButton_balance.setObjectName(u"pushButton_balance")

        self.verticalLayout.addWidget(self.pushButton_balance)

        self.pushButton_pairwise = QPushButton(WeightingDialog)
        self.pushButton_pairwise.setObjectName(u"pushButton_pairwise")

        self.verticalLayout.addWidget(self.pushButton_pairwise)

        self.buttonBox = QDialogButtonBox(WeightingDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(WeightingDialog)
        self.buttonBox.accepted.connect(WeightingDialog.accept)
        self.buttonBox.rejected.connect(WeightingDialog.reject)

        QMetaObject.connectSlotsByName(WeightingDialog)
    # setupUi

    def retranslateUi(self, WeightingDialog):
        WeightingDialog.setWindowTitle(QCoreApplication.translate("WeightingDialog", u"Weighting", None))
        self.label.setText(QCoreApplication.translate("WeightingDialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Example</span></p></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("WeightingDialog", u"Troughput", None))
        self.label_3.setText(QCoreApplication.translate("WeightingDialog", u"Lead time", None))
        self.label_4.setText(QCoreApplication.translate("WeightingDialog", u"Space requirements", None))
        self.label_5.setText(QCoreApplication.translate("WeightingDialog", u"Buffer capacity", None))
        self.label_6.setText(QCoreApplication.translate("WeightingDialog", u"Cost", None))
        self.label_total.setText(QCoreApplication.translate("WeightingDialog", u"Total:", None))
        self.label_sum.setText(QCoreApplication.translate("WeightingDialog", u"0", None))
        self.pushButton_balance.setText(QCoreApplication.translate("WeightingDialog", u"Balance of all criteria", None))
        self.pushButton_pairwise.setText(QCoreApplication.translate("WeightingDialog", u"Pairwise comparison", None))
    # retranslateUi

