# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'add_goods_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QComboBox, QDialog,
    QDialogButtonBox, QFrame, QHBoxLayout, QLabel,
    QSizePolicy, QSpinBox, QVBoxLayout)

class Ui_add_goods_dialog(object):
    def setupUi(self, add_goods_dialog):
        if not add_goods_dialog.objectName():
            add_goods_dialog.setObjectName(u"add_goods_dialog")
        add_goods_dialog.resize(457, 235)
        self.verticalLayout = QVBoxLayout(add_goods_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_actual = QLabel(add_goods_dialog)
        self.label_actual.setObjectName(u"label_actual")

        self.verticalLayout.addWidget(self.label_actual)

        self.line_2 = QFrame(add_goods_dialog)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(add_goods_dialog)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.comboBox_goods_type = QComboBox(add_goods_dialog)
        self.comboBox_goods_type.setObjectName(u"comboBox_goods_type")

        self.horizontalLayout.addWidget(self.comboBox_goods_type)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_2 = QLabel(add_goods_dialog)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_2.addWidget(self.label_2)

        self.spinBox_amount = QSpinBox(add_goods_dialog)
        self.spinBox_amount.setObjectName(u"spinBox_amount")
        self.spinBox_amount.setMinimum(1)
        self.spinBox_amount.setMaximum(10000)

        self.horizontalLayout_2.addWidget(self.spinBox_amount)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_4 = QLabel(add_goods_dialog)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_4.addWidget(self.label_4)

        self.spinBox_load = QSpinBox(add_goods_dialog)
        self.spinBox_load.setObjectName(u"spinBox_load")
        self.spinBox_load.setMinimum(1)
        self.spinBox_load.setMaximum(999999)
        self.spinBox_load.setValue(100)

        self.horizontalLayout_4.addWidget(self.spinBox_load)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_3 = QLabel(add_goods_dialog)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayout_3.addWidget(self.label_3)

        self.comboBox_sink_source = QComboBox(add_goods_dialog)
        self.comboBox_sink_source.setObjectName(u"comboBox_sink_source")

        self.horizontalLayout_3.addWidget(self.comboBox_sink_source)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.buttonBox = QDialogButtonBox(add_goods_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
        self.buttonBox.setSizePolicy(sizePolicy)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(add_goods_dialog)
        self.buttonBox.accepted.connect(add_goods_dialog.accept)
        self.buttonBox.rejected.connect(add_goods_dialog.reject)

        QMetaObject.connectSlotsByName(add_goods_dialog)
    # setupUi

    def retranslateUi(self, add_goods_dialog):
        add_goods_dialog.setWindowTitle(QCoreApplication.translate("add_goods_dialog", u"Add a good", None))
        self.label_actual.setText(QCoreApplication.translate("add_goods_dialog", u"TextLabel", None))
        self.label.setText(QCoreApplication.translate("add_goods_dialog", u"Goods", None))
        self.label_2.setText(QCoreApplication.translate("add_goods_dialog", u"Amount", None))
        self.label_4.setText(QCoreApplication.translate("add_goods_dialog", u"Load [g]", None))
        self.label_3.setText(QCoreApplication.translate("add_goods_dialog", u"TextLabel", None))
    # retranslateUi

