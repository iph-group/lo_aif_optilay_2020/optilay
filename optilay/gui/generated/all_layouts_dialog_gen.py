# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'all_layouts_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QListWidget, QListWidgetItem, QSizePolicy, QVBoxLayout)

class Ui_All_layouts_dialog(object):
    def setupUi(self, All_layouts_dialog):
        if not All_layouts_dialog.objectName():
            All_layouts_dialog.setObjectName(u"All_layouts_dialog")
        All_layouts_dialog.resize(584, 403)
        self.verticalLayout = QVBoxLayout(All_layouts_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.listWidget = QListWidget(All_layouts_dialog)
        self.listWidget.setObjectName(u"listWidget")

        self.verticalLayout.addWidget(self.listWidget)

        self.buttonBox = QDialogButtonBox(All_layouts_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(All_layouts_dialog)
        self.buttonBox.accepted.connect(All_layouts_dialog.accept)
        self.buttonBox.rejected.connect(All_layouts_dialog.reject)

        QMetaObject.connectSlotsByName(All_layouts_dialog)
    # setupUi

    def retranslateUi(self, All_layouts_dialog):
        All_layouts_dialog.setWindowTitle(QCoreApplication.translate("All_layouts_dialog", u"All Layouts", None))
    # retranslateUi

