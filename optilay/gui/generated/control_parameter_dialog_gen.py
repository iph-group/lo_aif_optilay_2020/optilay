# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'control_parameter_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_ControlParameterDialog(object):
    def setupUi(self, ControlParameterDialog):
        if not ControlParameterDialog.objectName():
            ControlParameterDialog.setObjectName(u"ControlParameterDialog")
        ControlParameterDialog.resize(920, 400)
        self.verticalLayout = QVBoxLayout(ControlParameterDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.radioButton_genetic_algorithm = QRadioButton(ControlParameterDialog)
        self.radioButton_genetic_algorithm.setObjectName(u"radioButton_genetic_algorithm")
        self.radioButton_genetic_algorithm.setChecked(True)

        self.horizontalLayout_3.addWidget(self.radioButton_genetic_algorithm)

        self.label_17 = QLabel(ControlParameterDialog)
        self.label_17.setObjectName(u"label_17")

        self.horizontalLayout_3.addWidget(self.label_17)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.label_4 = QLabel(ControlParameterDialog)
        self.label_4.setObjectName(u"label_4")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_4)

        self.label_3 = QLabel(ControlParameterDialog)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_3)

        self.spinBox_number_of_generations = QSpinBox(ControlParameterDialog)
        self.spinBox_number_of_generations.setObjectName(u"spinBox_number_of_generations")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.spinBox_number_of_generations)

        self.spinBox_population_size = QSpinBox(ControlParameterDialog)
        self.spinBox_population_size.setObjectName(u"spinBox_population_size")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.spinBox_population_size)

        self.label_7 = QLabel(ControlParameterDialog)
        self.label_7.setObjectName(u"label_7")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_7)

        self.label_8 = QLabel(ControlParameterDialog)
        self.label_8.setObjectName(u"label_8")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label_8)

        self.label_9 = QLabel(ControlParameterDialog)
        self.label_9.setObjectName(u"label_9")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_9)

        self.label_10 = QLabel(ControlParameterDialog)
        self.label_10.setObjectName(u"label_10")

        self.formLayout.setWidget(5, QFormLayout.LabelRole, self.label_10)

        self.doubleSpinBox_mutation_rate = QDoubleSpinBox(ControlParameterDialog)
        self.doubleSpinBox_mutation_rate.setObjectName(u"doubleSpinBox_mutation_rate")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.doubleSpinBox_mutation_rate)

        self.spinBox_maximal_number_of_mutations = QSpinBox(ControlParameterDialog)
        self.spinBox_maximal_number_of_mutations.setObjectName(u"spinBox_maximal_number_of_mutations")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.spinBox_maximal_number_of_mutations)

        self.doubleSpinBox_crossover_rate = QDoubleSpinBox(ControlParameterDialog)
        self.doubleSpinBox_crossover_rate.setObjectName(u"doubleSpinBox_crossover_rate")

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.doubleSpinBox_crossover_rate)

        self.spinBox_crossover_two_point_operator_length = QSpinBox(ControlParameterDialog)
        self.spinBox_crossover_two_point_operator_length.setObjectName(u"spinBox_crossover_two_point_operator_length")

        self.formLayout.setWidget(5, QFormLayout.FieldRole, self.spinBox_crossover_two_point_operator_length)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout.setItem(6, QFormLayout.FieldRole, self.verticalSpacer)


        self.verticalLayout_2.addLayout(self.formLayout)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.radioButton_random_placement = QRadioButton(ControlParameterDialog)
        self.radioButton_random_placement.setObjectName(u"radioButton_random_placement")

        self.horizontalLayout_2.addWidget(self.radioButton_random_placement)

        self.label_16 = QLabel(ControlParameterDialog)
        self.label_16.setObjectName(u"label_16")

        self.horizontalLayout_2.addWidget(self.label_16)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.formLayout_2 = QFormLayout()
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.label = QLabel(ControlParameterDialog)
        self.label.setObjectName(u"label")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label)

        self.spinBox_number_of_iterations = QSpinBox(ControlParameterDialog)
        self.spinBox_number_of_iterations.setObjectName(u"spinBox_number_of_iterations")

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.spinBox_number_of_iterations)

        self.label_2 = QLabel(ControlParameterDialog)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.label_5 = QLabel(ControlParameterDialog)
        self.label_5.setObjectName(u"label_5")

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.label_5)

        self.label_11 = QLabel(ControlParameterDialog)
        self.label_11.setObjectName(u"label_11")

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_11)

        self.doubleSpinBox_random_positioning = QDoubleSpinBox(ControlParameterDialog)
        self.doubleSpinBox_random_positioning.setObjectName(u"doubleSpinBox_random_positioning")

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.doubleSpinBox_random_positioning)

        self.label_12 = QLabel(ControlParameterDialog)
        self.label_12.setObjectName(u"label_12")

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole, self.label_12)

        self.doubleSpinBox_shift = QDoubleSpinBox(ControlParameterDialog)
        self.doubleSpinBox_shift.setObjectName(u"doubleSpinBox_shift")

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.doubleSpinBox_shift)

        self.label_13 = QLabel(ControlParameterDialog)
        self.label_13.setObjectName(u"label_13")

        self.formLayout_2.setWidget(4, QFormLayout.LabelRole, self.label_13)

        self.doubleSpinBox_swap = QDoubleSpinBox(ControlParameterDialog)
        self.doubleSpinBox_swap.setObjectName(u"doubleSpinBox_swap")

        self.formLayout_2.setWidget(4, QFormLayout.FieldRole, self.doubleSpinBox_swap)

        self.label_6 = QLabel(ControlParameterDialog)
        self.label_6.setObjectName(u"label_6")

        self.formLayout_2.setWidget(5, QFormLayout.LabelRole, self.label_6)

        self.comboBox_initialisation = QComboBox(ControlParameterDialog)
        self.comboBox_initialisation.setObjectName(u"comboBox_initialisation")

        self.formLayout_2.setWidget(6, QFormLayout.FieldRole, self.comboBox_initialisation)

        self.label_14 = QLabel(ControlParameterDialog)
        self.label_14.setObjectName(u"label_14")

        self.formLayout_2.setWidget(6, QFormLayout.LabelRole, self.label_14)

        self.label_15 = QLabel(ControlParameterDialog)
        self.label_15.setObjectName(u"label_15")

        self.formLayout_2.setWidget(5, QFormLayout.FieldRole, self.label_15)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout_2.setItem(7, QFormLayout.FieldRole, self.verticalSpacer_2)


        self.verticalLayout_3.addLayout(self.formLayout_2)


        self.horizontalLayout.addLayout(self.verticalLayout_3)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.line = QFrame(ControlParameterDialog)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_19 = QLabel(ControlParameterDialog)
        self.label_19.setObjectName(u"label_19")

        self.horizontalLayout_4.addWidget(self.label_19)

        self.spinBox_path_search_counter = QSpinBox(ControlParameterDialog)
        self.spinBox_path_search_counter.setObjectName(u"spinBox_path_search_counter")
        self.spinBox_path_search_counter.setMinimum(1)
        self.spinBox_path_search_counter.setMaximum(50)

        self.horizontalLayout_4.addWidget(self.spinBox_path_search_counter)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_3)

        self.label_18 = QLabel(ControlParameterDialog)
        self.label_18.setObjectName(u"label_18")

        self.horizontalLayout_4.addWidget(self.label_18)

        self.spinBox_cores = QSpinBox(ControlParameterDialog)
        self.spinBox_cores.setObjectName(u"spinBox_cores")

        self.horizontalLayout_4.addWidget(self.spinBox_cores)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.buttonBox = QDialogButtonBox(ControlParameterDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(ControlParameterDialog)
        self.buttonBox.accepted.connect(ControlParameterDialog.accept)
        self.buttonBox.rejected.connect(ControlParameterDialog.reject)

        QMetaObject.connectSlotsByName(ControlParameterDialog)
    # setupUi

    def retranslateUi(self, ControlParameterDialog):
        ControlParameterDialog.setWindowTitle(QCoreApplication.translate("ControlParameterDialog", u"Control parameter", None))
        self.radioButton_genetic_algorithm.setText("")
        self.label_17.setText(QCoreApplication.translate("ControlParameterDialog", u"<h2>Genetic Algorithm (default)</h2>", None))
        self.label_4.setText(QCoreApplication.translate("ControlParameterDialog", u"Number of generations", None))
        self.label_3.setText(QCoreApplication.translate("ControlParameterDialog", u"Population size", None))
        self.label_7.setText(QCoreApplication.translate("ControlParameterDialog", u"Mutation rate", None))
        self.label_8.setText(QCoreApplication.translate("ControlParameterDialog", u"Maximal number of mutations", None))
        self.label_9.setText(QCoreApplication.translate("ControlParameterDialog", u"Crossover rate", None))
        self.label_10.setText(QCoreApplication.translate("ControlParameterDialog", u"Crossover two-point-operator length", None))
        self.radioButton_random_placement.setText("")
        self.label_16.setText(QCoreApplication.translate("ControlParameterDialog", u"<h2>Random Placement</h2\n"
">\n"
"", None))
        self.label.setText(QCoreApplication.translate("ControlParameterDialog", u"Number of iterations", None))
        self.label_2.setText(QCoreApplication.translate("ControlParameterDialog", u"<h3>Neighboorhood search</h3>\n"
"", None))
        self.label_5.setText("")
        self.label_11.setText(QCoreApplication.translate("ControlParameterDialog", u"Random positioning", None))
        self.label_12.setText(QCoreApplication.translate("ControlParameterDialog", u"Shift", None))
        self.label_13.setText(QCoreApplication.translate("ControlParameterDialog", u"Swap", None))
        self.label_6.setText(QCoreApplication.translate("ControlParameterDialog", u"<h3>Initialisation</h3>\n"
"", None))
        self.label_14.setText(QCoreApplication.translate("ControlParameterDialog", u"Initialisation method", None))
        self.label_15.setText("")
        self.label_19.setText(QCoreApplication.translate("ControlParameterDialog", u"Path search counter", None))
        self.label_18.setText(QCoreApplication.translate("ControlParameterDialog", u"Cores", None))
    # retranslateUi

