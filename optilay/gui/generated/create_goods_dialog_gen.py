# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'create_goods_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_CreateGoodsDialog(object):
    def setupUi(self, CreateGoodsDialog):
        if not CreateGoodsDialog.objectName():
            CreateGoodsDialog.setObjectName(u"CreateGoodsDialog")
        CreateGoodsDialog.resize(400, 300)
        self.verticalLayout = QVBoxLayout(CreateGoodsDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.label = QLabel(CreateGoodsDialog)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.label_2 = QLabel(CreateGoodsDialog)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.label_3 = QLabel(CreateGoodsDialog)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_3)

        self.label_4 = QLabel(CreateGoodsDialog)
        self.label_4.setObjectName(u"label_4")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label_4)

        self.label_5 = QLabel(CreateGoodsDialog)
        self.label_5.setObjectName(u"label_5")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_5)

        self.label_6 = QLabel(CreateGoodsDialog)
        self.label_6.setObjectName(u"label_6")

        self.formLayout.setWidget(5, QFormLayout.LabelRole, self.label_6)

        self.lineEdit_name = QLineEdit(CreateGoodsDialog)
        self.lineEdit_name.setObjectName(u"lineEdit_name")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.lineEdit_name)

        self.spinBox_width = QSpinBox(CreateGoodsDialog)
        self.spinBox_width.setObjectName(u"spinBox_width")
        self.spinBox_width.setMaximum(9999)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.spinBox_width)

        self.spinBox_length = QSpinBox(CreateGoodsDialog)
        self.spinBox_length.setObjectName(u"spinBox_length")
        self.spinBox_length.setMaximum(9999)

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.spinBox_length)

        self.comboBox_type = QComboBox(CreateGoodsDialog)
        self.comboBox_type.setObjectName(u"comboBox_type")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.comboBox_type)

        self.spinBox_empty_weight = QSpinBox(CreateGoodsDialog)
        self.spinBox_empty_weight.setObjectName(u"spinBox_empty_weight")
        self.spinBox_empty_weight.setMaximum(99999)

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.spinBox_empty_weight)

        self.spinBox_maximum_weight = QSpinBox(CreateGoodsDialog)
        self.spinBox_maximum_weight.setObjectName(u"spinBox_maximum_weight")
        self.spinBox_maximum_weight.setMaximum(99999)

        self.formLayout.setWidget(5, QFormLayout.FieldRole, self.spinBox_maximum_weight)


        self.verticalLayout.addLayout(self.formLayout)

        self.buttonBox = QDialogButtonBox(CreateGoodsDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(CreateGoodsDialog)
        self.buttonBox.accepted.connect(CreateGoodsDialog.accept)
        self.buttonBox.rejected.connect(CreateGoodsDialog.reject)

        QMetaObject.connectSlotsByName(CreateGoodsDialog)
    # setupUi

    def retranslateUi(self, CreateGoodsDialog):
        CreateGoodsDialog.setWindowTitle(QCoreApplication.translate("CreateGoodsDialog", u"Create new Good", None))
        self.label.setText(QCoreApplication.translate("CreateGoodsDialog", u"Name", None))
        self.label_2.setText(QCoreApplication.translate("CreateGoodsDialog", u"Width [mm]", None))
        self.label_3.setText(QCoreApplication.translate("CreateGoodsDialog", u"Length [mm]", None))
        self.label_4.setText(QCoreApplication.translate("CreateGoodsDialog", u"Type", None))
        self.label_5.setText(QCoreApplication.translate("CreateGoodsDialog", u"Empty weight [g]", None))
        self.label_6.setText(QCoreApplication.translate("CreateGoodsDialog", u"Maximum weight [g]", None))
    # retranslateUi

