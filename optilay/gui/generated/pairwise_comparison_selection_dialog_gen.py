# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'pairwaise_comparison_selection_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_PairwiseComparisonSelectionDialog(object):
    def setupUi(self, PairwiseComparisonSelectionDialog):
        if not PairwiseComparisonSelectionDialog.objectName():
            PairwiseComparisonSelectionDialog.setObjectName(u"PairwiseComparisonSelectionDialog")
        PairwiseComparisonSelectionDialog.resize(616, 487)
        self.verticalLayout_4 = QVBoxLayout(PairwiseComparisonSelectionDialog)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label = QLabel(PairwiseComparisonSelectionDialog)
        self.label.setObjectName(u"label")

        self.verticalLayout_4.addWidget(self.label)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_2 = QLabel(PairwiseComparisonSelectionDialog)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout_2.addWidget(self.label_2, 0, Qt.AlignHCenter)

        self.listWidget_left = QListWidget(PairwiseComparisonSelectionDialog)
        self.listWidget_left.setObjectName(u"listWidget_left")

        self.verticalLayout_2.addWidget(self.listWidget_left)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.pushButton_right = QPushButton(PairwiseComparisonSelectionDialog)
        self.pushButton_right.setObjectName(u"pushButton_right")

        self.verticalLayout.addWidget(self.pushButton_right)

        self.pushButton_left = QPushButton(PairwiseComparisonSelectionDialog)
        self.pushButton_left.setObjectName(u"pushButton_left")

        self.verticalLayout.addWidget(self.pushButton_left)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label_3 = QLabel(PairwiseComparisonSelectionDialog)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_3.addWidget(self.label_3, 0, Qt.AlignHCenter)

        self.listWidget_right = QListWidget(PairwiseComparisonSelectionDialog)
        self.listWidget_right.setObjectName(u"listWidget_right")

        self.verticalLayout_3.addWidget(self.listWidget_right)


        self.horizontalLayout.addLayout(self.verticalLayout_3)


        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.buttonBox = QDialogButtonBox(PairwiseComparisonSelectionDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout_4.addWidget(self.buttonBox)


        self.retranslateUi(PairwiseComparisonSelectionDialog)
        self.buttonBox.accepted.connect(PairwiseComparisonSelectionDialog.accept)
        self.buttonBox.rejected.connect(PairwiseComparisonSelectionDialog.reject)

        QMetaObject.connectSlotsByName(PairwiseComparisonSelectionDialog)
    # setupUi

    def retranslateUi(self, PairwiseComparisonSelectionDialog):
        PairwiseComparisonSelectionDialog.setWindowTitle(QCoreApplication.translate("PairwiseComparisonSelectionDialog", u"Dialog", None))
        self.label.setText(QCoreApplication.translate("PairwiseComparisonSelectionDialog", u"<html><head/><body><h1>Choose your weighting criteria</h1></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("PairwiseComparisonSelectionDialog", u"Criteria", None))
        self.pushButton_right.setText(QCoreApplication.translate("PairwiseComparisonSelectionDialog", u">", None))
        self.pushButton_left.setText(QCoreApplication.translate("PairwiseComparisonSelectionDialog", u"<", None))
        self.label_3.setText(QCoreApplication.translate("PairwiseComparisonSelectionDialog", u"Non criteria", None))
    # retranslateUi

