# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'start_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_start_dialog(object):
    def setupUi(self, start_dialog):
        if not start_dialog.objectName():
            start_dialog.setObjectName(u"start_dialog")
        start_dialog.resize(400, 250)
        start_dialog.setMinimumSize(QSize(400, 250))
        start_dialog.setMaximumSize(QSize(400, 250))
        self.gridLayout = QGridLayout(start_dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.new_layout_button = QPushButton(start_dialog)
        self.new_layout_button.setObjectName(u"new_layout_button")

        self.gridLayout.addWidget(self.new_layout_button, 3, 1, 1, 1)

        self.horizontal_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontal_spacer, 3, 0, 1, 1)

        self.welcome_text_label = QLabel(start_dialog)
        self.welcome_text_label.setObjectName(u"welcome_text_label")
        self.welcome_text_label.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.welcome_text_label, 1, 1, 1, 1)

        self.load_layout_button = QPushButton(start_dialog)
        self.load_layout_button.setObjectName(u"load_layout_button")

        self.gridLayout.addWidget(self.load_layout_button, 4, 1, 1, 1)

        self.vertical_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.vertical_spacer_3, 2, 1, 1, 1)

        self.horizontal_spacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontal_spacer_2, 3, 2, 1, 1)

        self.vertical_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.vertical_spacer, 5, 1, 1, 1)

        self.vertical_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.vertical_spacer_2, 0, 1, 1, 1)

        self.button_box = QDialogButtonBox(start_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Close)

        self.gridLayout.addWidget(self.button_box, 6, 2, 1, 1)


        self.retranslateUi(start_dialog)
        self.button_box.rejected.connect(start_dialog.reject)

        QMetaObject.connectSlotsByName(start_dialog)
    # setupUi

    def retranslateUi(self, start_dialog):
        start_dialog.setWindowTitle(QCoreApplication.translate("start_dialog", u"Optilay - Start", None))
#if QT_CONFIG(tooltip)
        self.new_layout_button.setToolTip(QCoreApplication.translate("start_dialog", u"A new factory can be created here", None))
#endif // QT_CONFIG(tooltip)
        self.new_layout_button.setText(QCoreApplication.translate("start_dialog", u"New conveyor layout", None))
        self.welcome_text_label.setText(QCoreApplication.translate("start_dialog", u"Welcome to Optilay", None))
#if QT_CONFIG(tooltip)
        self.load_layout_button.setToolTip(QCoreApplication.translate("start_dialog", u"If you already created a factory you can reload it here.", None))
#endif // QT_CONFIG(tooltip)
        self.load_layout_button.setText(QCoreApplication.translate("start_dialog", u"Load conveyor layout", None))
    # retranslateUi

