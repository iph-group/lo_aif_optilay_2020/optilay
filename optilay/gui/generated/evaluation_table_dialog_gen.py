# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'evaluation_table_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QAbstractScrollArea, QApplication, QDialog,
    QDialogButtonBox, QHeaderView, QSizePolicy, QTableWidget,
    QTableWidgetItem, QVBoxLayout)

class Ui_evaluatio_table_dialog(object):
    def setupUi(self, evaluatio_table_dialog):
        if not evaluatio_table_dialog.objectName():
            evaluatio_table_dialog.setObjectName(u"evaluatio_table_dialog")
        evaluatio_table_dialog.resize(1174, 346)
        self.verticalLayout = QVBoxLayout(evaluatio_table_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.table_eval = QTableWidget(evaluatio_table_dialog)
        self.table_eval.setObjectName(u"table_eval")
        self.table_eval.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        self.verticalLayout.addWidget(self.table_eval)

        self.buttonBox = QDialogButtonBox(evaluatio_table_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(evaluatio_table_dialog)
        self.buttonBox.accepted.connect(evaluatio_table_dialog.accept)
        self.buttonBox.rejected.connect(evaluatio_table_dialog.reject)

        QMetaObject.connectSlotsByName(evaluatio_table_dialog)
    # setupUi

    def retranslateUi(self, evaluatio_table_dialog):
        evaluatio_table_dialog.setWindowTitle(QCoreApplication.translate("evaluatio_table_dialog", u"Evaluation", None))
    # retranslateUi

