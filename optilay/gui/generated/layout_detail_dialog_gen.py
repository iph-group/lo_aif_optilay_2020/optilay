# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'layout_detail_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QSizePolicy)

class Ui_LayoutDetailDialog(object):
    def setupUi(self, LayoutDetailDialog):
        if not LayoutDetailDialog.objectName():
            LayoutDetailDialog.setObjectName(u"LayoutDetailDialog")
        LayoutDetailDialog.resize(685, 733)

        self.retranslateUi(LayoutDetailDialog)

        QMetaObject.connectSlotsByName(LayoutDetailDialog)
    # setupUi

    def retranslateUi(self, LayoutDetailDialog):
        LayoutDetailDialog.setWindowTitle(QCoreApplication.translate("LayoutDetailDialog", u"Layout Details", None))
    # retranslateUi

