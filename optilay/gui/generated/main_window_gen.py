# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main_window.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1003, 693)
        self.actionNew = QAction(MainWindow)
        self.actionNew.setObjectName(u"actionNew")
        self.actionLoad = QAction(MainWindow)
        self.actionLoad.setObjectName(u"actionLoad")
        self.actionSave = QAction(MainWindow)
        self.actionSave.setObjectName(u"actionSave")
        self.actionSave_as = QAction(MainWindow)
        self.actionSave_as.setObjectName(u"actionSave_as")
        self.actionAML_Import = QAction(MainWindow)
        self.actionAML_Import.setObjectName(u"actionAML_Import")
        self.actionAML_Export = QAction(MainWindow)
        self.actionAML_Export.setObjectName(u"actionAML_Export")
        self.actionQuit = QAction(MainWindow)
        self.actionQuit.setObjectName(u"actionQuit")
        self.actionMaterial_flow = QAction(MainWindow)
        self.actionMaterial_flow.setObjectName(u"actionMaterial_flow")
        self.actionLayout = QAction(MainWindow)
        self.actionLayout.setObjectName(u"actionLayout")
        self.actionWeighting = QAction(MainWindow)
        self.actionWeighting.setObjectName(u"actionWeighting")
        self.actionControl_parameters = QAction(MainWindow)
        self.actionControl_parameters.setObjectName(u"actionControl_parameters")
        self.actionExtended_settings = QAction(MainWindow)
        self.actionExtended_settings.setObjectName(u"actionExtended_settings")
        self.actionTable = QAction(MainWindow)
        self.actionTable.setObjectName(u"actionTable")
        self.actionLayout_comparison = QAction(MainWindow)
        self.actionLayout_comparison.setObjectName(u"actionLayout_comparison")
        self.actionAll_layouts = QAction(MainWindow)
        self.actionAll_layouts.setObjectName(u"actionAll_layouts")
        self.actionResult_table = QAction(MainWindow)
        self.actionResult_table.setObjectName(u"actionResult_table")
        self.actionRaw_data = QAction(MainWindow)
        self.actionRaw_data.setObjectName(u"actionRaw_data")
        self.actionConveyor_selection = QAction(MainWindow)
        self.actionConveyor_selection.setObjectName(u"actionConveyor_selection")
        self.actionAdd_goods = QAction(MainWindow)
        self.actionAdd_goods.setObjectName(u"actionAdd_goods")
        self.actionInstructions = QAction(MainWindow)
        self.actionInstructions.setObjectName(u"actionInstructions")
        self.actionLicense = QAction(MainWindow)
        self.actionLicense.setObjectName(u"actionLicense")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")

        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pushButton_reset = QPushButton(self.centralwidget)
        self.pushButton_reset.setObjectName(u"pushButton_reset")

        self.horizontalLayout.addWidget(self.pushButton_reset)

        self.pushButton_evaluate = QPushButton(self.centralwidget)
        self.pushButton_evaluate.setObjectName(u"pushButton_evaluate")

        self.horizontalLayout.addWidget(self.pushButton_evaluate)

        self.pushButton_optimise = QPushButton(self.centralwidget)
        self.pushButton_optimise.setObjectName(u"pushButton_optimise")

        self.horizontalLayout.addWidget(self.pushButton_optimise)


        self.verticalLayout.addLayout(self.horizontalLayout)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1003, 24))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuData = QMenu(self.menubar)
        self.menuData.setObjectName(u"menuData")
        self.menuParameter = QMenu(self.menubar)
        self.menuParameter.setObjectName(u"menuParameter")
        self.menuResults = QMenu(self.menubar)
        self.menuResults.setObjectName(u"menuResults")
        self.menuHelp = QMenu(self.menubar)
        self.menuHelp.setObjectName(u"menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuData.menuAction())
        self.menubar.addAction(self.menuParameter.menuAction())
        self.menubar.addAction(self.menuResults.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionNew)
        self.menuFile.addAction(self.actionLoad)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionSave_as)
        self.menuFile.addAction(self.actionAML_Import)
        self.menuFile.addAction(self.actionAML_Export)
        self.menuFile.addAction(self.actionQuit)
        self.menuData.addAction(self.actionRaw_data)
        self.menuData.addAction(self.actionAdd_goods)
        self.menuParameter.addAction(self.actionWeighting)
        self.menuParameter.addAction(self.actionControl_parameters)
        self.menuParameter.addAction(self.actionConveyor_selection)
        self.menuResults.addAction(self.actionResult_table)
        self.menuResults.addAction(self.actionLayout_comparison)
        self.menuResults.addAction(self.actionAll_layouts)
        self.menuHelp.addAction(self.actionInstructions)
        self.menuHelp.addAction(self.actionLicense)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.actionNew.setText(QCoreApplication.translate("MainWindow", u"New", None))
#if QT_CONFIG(shortcut)
        self.actionNew.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+N", None))
#endif // QT_CONFIG(shortcut)
        self.actionLoad.setText(QCoreApplication.translate("MainWindow", u"Load", None))
#if QT_CONFIG(shortcut)
        self.actionLoad.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+O", None))
#endif // QT_CONFIG(shortcut)
        self.actionSave.setText(QCoreApplication.translate("MainWindow", u"Save", None))
#if QT_CONFIG(shortcut)
        self.actionSave.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionSave_as.setText(QCoreApplication.translate("MainWindow", u"Save as", None))
        self.actionAML_Import.setText(QCoreApplication.translate("MainWindow", u"AML-Import", None))
        self.actionAML_Export.setText(QCoreApplication.translate("MainWindow", u"AML-Export", None))
        self.actionQuit.setText(QCoreApplication.translate("MainWindow", u"Quit", None))
#if QT_CONFIG(shortcut)
        self.actionQuit.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+Q", None))
#endif // QT_CONFIG(shortcut)
        self.actionMaterial_flow.setText(QCoreApplication.translate("MainWindow", u"Material flow", None))
        self.actionLayout.setText(QCoreApplication.translate("MainWindow", u"Layout", None))
        self.actionWeighting.setText(QCoreApplication.translate("MainWindow", u"Weighting", None))
        self.actionControl_parameters.setText(QCoreApplication.translate("MainWindow", u"Control parameters", None))
        self.actionExtended_settings.setText(QCoreApplication.translate("MainWindow", u"Extended settings", None))
        self.actionTable.setText(QCoreApplication.translate("MainWindow", u"Table", None))
        self.actionLayout_comparison.setText(QCoreApplication.translate("MainWindow", u"Layout comparison", None))
        self.actionAll_layouts.setText(QCoreApplication.translate("MainWindow", u"All layouts", None))
        self.actionResult_table.setText(QCoreApplication.translate("MainWindow", u"Result table", None))
        self.actionRaw_data.setText(QCoreApplication.translate("MainWindow", u"Raw layout data", None))
        self.actionConveyor_selection.setText(QCoreApplication.translate("MainWindow", u"Conveyor selection", None))
        self.actionAdd_goods.setText(QCoreApplication.translate("MainWindow", u"Add goods", None))
        self.actionInstructions.setText(QCoreApplication.translate("MainWindow", u"Instructions", None))
        self.actionLicense.setText(QCoreApplication.translate("MainWindow", u"License", None))
        self.pushButton_reset.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
        self.pushButton_evaluate.setText(QCoreApplication.translate("MainWindow", u"Evaluate", None))
        self.pushButton_optimise.setText(QCoreApplication.translate("MainWindow", u"Optimise", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuData.setTitle(QCoreApplication.translate("MainWindow", u"Data", None))
        self.menuParameter.setTitle(QCoreApplication.translate("MainWindow", u"Parameter", None))
        self.menuResults.setTitle(QCoreApplication.translate("MainWindow", u"Results", None))
        self.menuHelp.setTitle(QCoreApplication.translate("MainWindow", u"Help", None))
    # retranslateUi

