# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'pairwaise_comparison_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_pairwise_comparison_dialog(object):
    def setupUi(self, pairwise_comparison_dialog):
        if not pairwise_comparison_dialog.objectName():
            pairwise_comparison_dialog.setObjectName(u"pairwise_comparison_dialog")
        pairwise_comparison_dialog.resize(610, 255)
        self.verticalLayout = QVBoxLayout(pairwise_comparison_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.title_label = QLabel(pairwise_comparison_dialog)
        self.title_label.setObjectName(u"title_label")

        self.verticalLayout.addWidget(self.title_label)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.left_pushButton = QPushButton(pairwise_comparison_dialog)
        self.left_pushButton.setObjectName(u"left_pushButton")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.left_pushButton.sizePolicy().hasHeightForWidth())
        self.left_pushButton.setSizePolicy(sizePolicy)
        self.left_pushButton.setAutoDefault(False)

        self.horizontalLayout_2.addWidget(self.left_pushButton)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout_2.addItem(self.verticalSpacer)

        self.right_pushButton = QPushButton(pairwise_comparison_dialog)
        self.right_pushButton.setObjectName(u"right_pushButton")
        sizePolicy.setHeightForWidth(self.right_pushButton.sizePolicy().hasHeightForWidth())
        self.right_pushButton.setSizePolicy(sizePolicy)
        self.right_pushButton.setAutoDefault(False)

        self.horizontalLayout_2.addWidget(self.right_pushButton)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.count_label = QLabel(pairwise_comparison_dialog)
        self.count_label.setObjectName(u"count_label")

        self.horizontalLayout_3.addWidget(self.count_label)

        self.buttonBox = QDialogButtonBox(pairwise_comparison_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel)

        self.horizontalLayout_3.addWidget(self.buttonBox)


        self.verticalLayout.addLayout(self.horizontalLayout_3)


        self.retranslateUi(pairwise_comparison_dialog)
        self.buttonBox.rejected.connect(pairwise_comparison_dialog.reject)

        QMetaObject.connectSlotsByName(pairwise_comparison_dialog)
    # setupUi

    def retranslateUi(self, pairwise_comparison_dialog):
        pairwise_comparison_dialog.setWindowTitle(QCoreApplication.translate("pairwise_comparison_dialog", u"Pairwise Comparison", None))
        self.title_label.setText(QCoreApplication.translate("pairwise_comparison_dialog", u"<html><head/><body><h1>Which one is more important?</h1></body></html>", None))
        self.left_pushButton.setText(QCoreApplication.translate("pairwise_comparison_dialog", u"PushButton", None))
        self.right_pushButton.setText(QCoreApplication.translate("pairwise_comparison_dialog", u"PushButton", None))
        self.count_label.setText(QCoreApplication.translate("pairwise_comparison_dialog", u"TextLabel", None))
    # retranslateUi

