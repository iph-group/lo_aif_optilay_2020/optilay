# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'raw_data_table_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QHBoxLayout, QHeaderView, QSizePolicy, QTabWidget,
    QTableWidget, QTableWidgetItem, QVBoxLayout, QWidget)

class Ui_raw_data_table_dialog(object):
    def setupUi(self, raw_data_table_dialog):
        if not raw_data_table_dialog.objectName():
            raw_data_table_dialog.setObjectName(u"raw_data_table_dialog")
        raw_data_table_dialog.resize(874, 620)
        self.verticalLayout = QVBoxLayout(raw_data_table_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.tab_widget = QTabWidget(raw_data_table_dialog)
        self.tab_widget.setObjectName(u"tab_widget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.vertical_layout_2 = QVBoxLayout(self.tab)
        self.vertical_layout_2.setObjectName(u"vertical_layout_2")
        self.general_table_widget = QTableWidget(self.tab)
        self.general_table_widget.setObjectName(u"general_table_widget")

        self.vertical_layout_2.addWidget(self.general_table_widget)

        self.tab_widget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.vertical_layout_3 = QVBoxLayout(self.tab_2)
        self.vertical_layout_3.setObjectName(u"vertical_layout_3")
        self.systemconnection_table_widget = QTableWidget(self.tab_2)
        self.systemconnection_table_widget.setObjectName(u"systemconnection_table_widget")

        self.vertical_layout_3.addWidget(self.systemconnection_table_widget)

        self.tab_widget.addTab(self.tab_2, "")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.vertical_layout_4 = QVBoxLayout(self.tab_3)
        self.vertical_layout_4.setObjectName(u"vertical_layout_4")
        self.goods_table_widget = QTableWidget(self.tab_3)
        self.goods_table_widget.setObjectName(u"goods_table_widget")

        self.vertical_layout_4.addWidget(self.goods_table_widget)

        self.tab_widget.addTab(self.tab_3, "")
        self.tab_4 = QWidget()
        self.tab_4.setObjectName(u"tab_4")
        self.verticalLayout_2 = QVBoxLayout(self.tab_4)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.conveyor_table_widget = QTableWidget(self.tab_4)
        self.conveyor_table_widget.setObjectName(u"conveyor_table_widget")

        self.verticalLayout_2.addWidget(self.conveyor_table_widget)

        self.tab_widget.addTab(self.tab_4, "")

        self.verticalLayout.addWidget(self.tab_widget)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.button_box = QDialogButtonBox(raw_data_table_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Ok)

        self.horizontalLayout_2.addWidget(self.button_box)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.retranslateUi(raw_data_table_dialog)
        self.button_box.accepted.connect(raw_data_table_dialog.accept)
        self.button_box.rejected.connect(raw_data_table_dialog.reject)

        self.tab_widget.setCurrentIndex(3)


        QMetaObject.connectSlotsByName(raw_data_table_dialog)
    # setupUi

    def retranslateUi(self, raw_data_table_dialog):
        raw_data_table_dialog.setWindowTitle(QCoreApplication.translate("raw_data_table_dialog", u"Layout information", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab), QCoreApplication.translate("raw_data_table_dialog", u"General", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_2), QCoreApplication.translate("raw_data_table_dialog", u"Systemconnections", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_3), QCoreApplication.translate("raw_data_table_dialog", u"Goods", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_4), QCoreApplication.translate("raw_data_table_dialog", u"Conveyor", None))
    # retranslateUi

