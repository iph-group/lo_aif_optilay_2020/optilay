# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'remove_goods_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QHBoxLayout, QLabel, QListWidget, QListWidgetItem,
    QPushButton, QSizePolicy, QSpacerItem, QVBoxLayout)

class Ui_RemoveGoodsDialog(object):
    def setupUi(self, RemoveGoodsDialog):
        if not RemoveGoodsDialog.objectName():
            RemoveGoodsDialog.setObjectName(u"RemoveGoodsDialog")
        RemoveGoodsDialog.resize(616, 487)
        self.verticalLayout_4 = QVBoxLayout(RemoveGoodsDialog)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_2 = QLabel(RemoveGoodsDialog)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout_2.addWidget(self.label_2, 0, Qt.AlignHCenter)

        self.listWidget_left = QListWidget(RemoveGoodsDialog)
        self.listWidget_left.setObjectName(u"listWidget_left")

        self.verticalLayout_2.addWidget(self.listWidget_left)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.pushButton_right = QPushButton(RemoveGoodsDialog)
        self.pushButton_right.setObjectName(u"pushButton_right")

        self.verticalLayout.addWidget(self.pushButton_right)

        self.pushButton_left = QPushButton(RemoveGoodsDialog)
        self.pushButton_left.setObjectName(u"pushButton_left")

        self.verticalLayout.addWidget(self.pushButton_left)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label_3 = QLabel(RemoveGoodsDialog)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_3.addWidget(self.label_3, 0, Qt.AlignHCenter)

        self.listWidget_right = QListWidget(RemoveGoodsDialog)
        self.listWidget_right.setObjectName(u"listWidget_right")

        self.verticalLayout_3.addWidget(self.listWidget_right)


        self.horizontalLayout.addLayout(self.verticalLayout_3)


        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.buttonBox = QDialogButtonBox(RemoveGoodsDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout_4.addWidget(self.buttonBox)


        self.retranslateUi(RemoveGoodsDialog)
        self.buttonBox.accepted.connect(RemoveGoodsDialog.accept)
        self.buttonBox.rejected.connect(RemoveGoodsDialog.reject)

        QMetaObject.connectSlotsByName(RemoveGoodsDialog)
    # setupUi

    def retranslateUi(self, RemoveGoodsDialog):
        RemoveGoodsDialog.setWindowTitle(QCoreApplication.translate("RemoveGoodsDialog", u"Dialog", None))
        self.label_2.setText(QCoreApplication.translate("RemoveGoodsDialog", u"Goods list", None))
        self.pushButton_right.setText(QCoreApplication.translate("RemoveGoodsDialog", u">", None))
        self.pushButton_left.setText(QCoreApplication.translate("RemoveGoodsDialog", u"<", None))
        self.label_3.setText(QCoreApplication.translate("RemoveGoodsDialog", u"To be removed", None))
    # retranslateUi

