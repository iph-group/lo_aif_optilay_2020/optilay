# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'layout_compare_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_LayoutComparisonDialog(object):
    def setupUi(self, LayoutComparisonDialog):
        if not LayoutComparisonDialog.objectName():
            LayoutComparisonDialog.setObjectName(u"LayoutComparisonDialog")
        LayoutComparisonDialog.resize(871, 700)
        self.verticalLayout = QVBoxLayout(LayoutComparisonDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(LayoutComparisonDialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.scrollArea = QScrollArea(LayoutComparisonDialog)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 845, 595))
        self.verticalLayout_2 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")

        self.verticalLayout_2.addLayout(self.gridLayout)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout.addWidget(self.scrollArea)

        self.buttonBox = QDialogButtonBox(LayoutComparisonDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setStandardButtons(QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(LayoutComparisonDialog)

        QMetaObject.connectSlotsByName(LayoutComparisonDialog)
    # setupUi

    def retranslateUi(self, LayoutComparisonDialog):
        LayoutComparisonDialog.setWindowTitle(QCoreApplication.translate("LayoutComparisonDialog", u"Results", None))
        self.label.setText(QCoreApplication.translate("LayoutComparisonDialog", u"<h1>Results</h1>\n"
"", None))
    # retranslateUi

