# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'size_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_size_dialog(object):
    def setupUi(self, size_dialog):
        if not size_dialog.objectName():
            size_dialog.setObjectName(u"size_dialog")
        size_dialog.resize(454, 313)
        self.vertical_layout = QVBoxLayout(size_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.horizontal_layout = QHBoxLayout()
        self.horizontal_layout.setObjectName(u"horizontal_layout")
        self.label_cell_size = QLabel(size_dialog)
        self.label_cell_size.setObjectName(u"label_cell_size")

        self.horizontal_layout.addWidget(self.label_cell_size)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontal_layout.addItem(self.verticalSpacer_2)

        self.comboBox_cell_size = QComboBox(size_dialog)
        self.comboBox_cell_size.setObjectName(u"comboBox_cell_size")

        self.horizontal_layout.addWidget(self.comboBox_cell_size)


        self.vertical_layout.addLayout(self.horizontal_layout)

        self.horizontal_layout_2 = QHBoxLayout()
        self.horizontal_layout_2.setObjectName(u"horizontal_layout_2")
        self.label_factory_lenght = QLabel(size_dialog)
        self.label_factory_lenght.setObjectName(u"label_factory_lenght")
        self.label_factory_lenght.setMinimumSize(QSize(0, 0))

        self.horizontal_layout_2.addWidget(self.label_factory_lenght)

        self.doubleSpinBox_factory_length = QSpinBox(size_dialog)
        self.doubleSpinBox_factory_length.setObjectName(u"doubleSpinBox_factory_length")
        self.doubleSpinBox_factory_length.setProperty("showGroupSeparator", True)
        self.doubleSpinBox_factory_length.setMaximum(99999)
        self.doubleSpinBox_factory_length.setValue(100)

        self.horizontal_layout_2.addWidget(self.doubleSpinBox_factory_length)

        self.vertical_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontal_layout_2.addItem(self.vertical_spacer)

        self.label_factory_width = QLabel(size_dialog)
        self.label_factory_width.setObjectName(u"label_factory_width")

        self.horizontal_layout_2.addWidget(self.label_factory_width)

        self.doubleSpinBox_factory_width = QSpinBox(size_dialog)
        self.doubleSpinBox_factory_width.setObjectName(u"doubleSpinBox_factory_width")
        self.doubleSpinBox_factory_width.setProperty("showGroupSeparator", True)
        self.doubleSpinBox_factory_width.setMaximum(99999)
        self.doubleSpinBox_factory_width.setValue(100)

        self.horizontal_layout_2.addWidget(self.doubleSpinBox_factory_width)


        self.vertical_layout.addLayout(self.horizontal_layout_2)

        self.warn_label = QLabel(size_dialog)
        self.warn_label.setObjectName(u"warn_label")
        self.warn_label.setAlignment(Qt.AlignCenter)

        self.vertical_layout.addWidget(self.warn_label)

        self.button_box = QDialogButtonBox(size_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.vertical_layout.addWidget(self.button_box)


        self.retranslateUi(size_dialog)
        self.button_box.accepted.connect(size_dialog.accept)
        self.button_box.rejected.connect(size_dialog.reject)

        QMetaObject.connectSlotsByName(size_dialog)
    # setupUi

    def retranslateUi(self, size_dialog):
        size_dialog.setWindowTitle(QCoreApplication.translate("size_dialog", u"Conveyor layout size", None))
        self.label_cell_size.setText(QCoreApplication.translate("size_dialog", u"Edge length of the unit cells [mm]", None))
        self.label_factory_lenght.setText(QCoreApplication.translate("size_dialog", u"Layout length [mm]", None))
        self.label_factory_width.setText(QCoreApplication.translate("size_dialog", u"Layout width [mm]", None))
        self.warn_label.setText(QCoreApplication.translate("size_dialog", u"<html><head/><body><p><br/></p></body></html>", None))
    # retranslateUi

