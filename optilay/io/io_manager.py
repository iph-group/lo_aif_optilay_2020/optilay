from optilay.io.aml_exporter import AMLExporter
from optilay.io.aml_importer import AMLImporter
from optilay.io.io_type import IOType
from optilay.io.json_exporter import JSONExporter
from optilay.io.json_importer import JSONImporter
from optilay.model.layout_model import LayoutModel
from optilay.util.exceptions import NoPathException


class IOManager:
    """
    IOManager is responsible for all access to the file system.
    """

    def __init__(self) -> None:
        """

        """
        self.path = None

    def reset(self) -> None:
        """
        Resets all internal settings.
        :return:
        """
        self.path = None

    def save(self, io_type: IOType, layout: LayoutModel, path: str = None) -> None:
        """
        Saves the LayoutModel in the appropriate format in the file system.
        :param io_type: Data type in which the data is to be saved.
        :param layout: The LayoutModel that will be saved.
        :param path: The LayoutModel that will be saved.
        :return: None
        """
        if not path:
            if not self.path:
                raise NoPathException("No path")
            else:
                path = self.path
        else:
            self.path = path

        if io_type == IOType.AML:
            aml_exporter = AMLExporter(path, layout)
            aml_exporter.export()
        elif io_type == IOType.JSON:
            json_exporter = JSONExporter(path, layout)
            json_exporter.export()
        else:
            raise ValueError('Unknown IOType')

    def load(self, path: str) -> LayoutModel:
        """
        Loads LayoutModel from the given file path.
        :param path: File path where the file should be located.
        :return: The loaded LayoutModel.
        """
        # TODO: Decide from path which Importer to use
        if path.endswith('.json') or path.endswith(".JSON"):
            io_type: IOType = IOType.JSON
        elif path.endswith('.aml') or path.endswith('.AML'):
            io_type: IOType = IOType.AML
        else:
            raise ValueError("Optilay only accepts .json and .aml files")
        if io_type == IOType.AML:
            aml_importer = AMLImporter(path)
            return aml_importer.imports()
        elif io_type == IOType.JSON:
            json_importer = JSONImporter(path)
            return json_importer.imports()
        else:
            raise ValueError('Unknown IOType')
