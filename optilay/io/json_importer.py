import json
from typing import List

from optilay.evaluation.evaluation_method import EvaluationMethod
from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.control_parameter import ControlParameter, InitialisationMethod
from optilay.model.conveyor.conveyor import Conveyor
from optilay.model.conveyor.conveyor_direction import ConveyorDirection
from optilay.model.conveyor.conveyor_type import ConveyorType
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.deactivated_area import DeactivatedArea
from optilay.model.goods.goods import Goods
from optilay.model.goods.goods_factory import GoodsFactory
from optilay.model.goods.goods_type import GoodsType
from optilay.model.layout_db import LayoutDB
from optilay.model.layout_model import LayoutModel
from optilay.model.restricted_area import RestrictedArea
from optilay.model.sytemconnection.packet_stream import PacketStream
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_direction import SystemConnectionDirection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType
from optilay.model.util.cell_size import CellSize
from optilay.model.weighting import Weighting
from optilay.model.workplace import Workplace
from optilay.optimisation.optimisation_method import OptimisationMethod


class JSONImporter:
    """
    This class imports the LayoutModel from a JSON file.
    """

    def __init__(self, path: str) -> None:
        """

        :param path: File path where to import from.
        """
        self.path = path

    def imports(self) -> LayoutModel:
        """
        Starts the import.
        :return: The imported LayoutModel
        """
        with open(self.path, "r") as read_file:
            decode_dict = json.load(read_file, object_hook=enum_decode)
            return factory_model_decode(decode_dict)


def enum_decode(enc_object):
    """
    This method checks for the "__enum__" keyword inside the encoded string.
    If the keyword is found the corresponding Enum object is returned.
    The method is used as object_hook in the json.load method.
    :param enc_object: A Json encoded string
    :return: If no enum is found it returns the input, else the enum object.
    """
    if "__enum__" in enc_object:
        name, member = enc_object["__enum__"].split(".")
        if name == "CellSize":
            return getattr(CellSize, member)
        elif name == "ConveyorType":
            return getattr(ConveyorType, member)
        elif name == "OptimisationMethod":
            return getattr(OptimisationMethod, member)
        elif name == "EvaluationMethod":
            return getattr(EvaluationMethod, member)
        elif name == "ConveyorDirection":
            return getattr(ConveyorDirection, member)
        elif name == "SystemConnectionType":
            return getattr(SystemConnectionType, member)
        elif name == 'SystemConnectionDirection':
            return getattr(SystemConnectionDirection, member)
        elif name == 'GoodsType':
            return getattr(GoodsType, member)
        elif name == 'InitialisationMethod':
            return getattr(InitialisationMethod, member)
    else:
        return enc_object


def factory_model_decode(decode_dict):
    layout_model: LayoutModel = LayoutModel(length=decode_dict['_length'],
                                            width=decode_dict['_width'],
                                            cell_size=decode_dict['_cell_size'])
    layout_model._goods_factory = __decode_goods_factory(decode_dict['_goods_factory'], layout_model)

    layout_model.layout = __decode_conveyor_system_layout(decode_dict['layout'], layout_model._goods_factory,
                                                          layout_model._conveyor_factory)

    layout_model.parameter = __decode_control_parameter(decode_dict['parameter'], decode_dict['_length'],
                                                        decode_dict['_width'], decode_dict['_cell_size'])

    layout_model.weighting = __decode_weighting(decode_dict['weighting'])

    layout_model.best_result = __decode_evaluation_result(decode_dict['best_result'])

    layout_model.layouts = __decode_layout_db(decode_dict['layouts'], layout_model)

    return layout_model


def __decode_conveyor_system_layout(data, goods_factory, conv_factory) -> ConveyorSystemLayout:
    """
    This method constructs a ConveyorSystemLayout object from the given input.
    :param data: Dict parsed from JSON
    :return: Weighting
    """
    layout = ConveyorSystemLayout(data['_length'], data['_width'], data['_cell_size'], conv_factory=conv_factory,
                                  goods_factory=goods_factory)
    layout.MAXSIZE = data['MAXSIZE']
    layout.layout_id = data['layout_id']
    if data['evaluation']:
        layout.evaluation = __decode_evaluation_result(data['evaluation'])
    else:
        layout.evaluation = None

    layout.res_areas = __decode_restricted_area_list(data['res_areas'], layout)
    layout.workplaces = __decode_workplace_list(data['workplaces'], layout)
    layout.deactivated = __decode_deactivated_area_list(data['deactivated'], layout)
    layout.connection = __decode_connection_list(data['connection'], layout)
    layout.packet_stream = __decode_packet_stream_list(data['packet_stream'], layout)
    layout.conveyors = __decode_conveyor_list(data['conveyors'], layout)

    layout._goods_factory = goods_factory
    layout._conveyor_factory = conv_factory

    return layout


def __decode_goods_factory(data, layout) -> GoodsFactory:
    factory = GoodsFactory(layout._cell_size)
    factory.goods = data['goods']
    return factory


def __decode_packet_stream_list(data, layout) -> List[PacketStream]:
    streams = []
    for packstream in data:
        sink = None
        source = None
        for connection in layout.connection:
            if packstream['_sink'] == connection.id:
                sink = connection
                continue
            elif packstream['_source'] == connection.id:
                source = connection
                continue
            if (sink is not None) and (source is not None):
                break
        ps = PacketStream(sink, source)
        ps._path = packstream['_path']
        packstream_goods = __decode_goods_list(packstream['_goods'])
        for goods_id, amount in packstream['_packets'].items():
            for good in packstream_goods:
                if good.id == int(goods_id):
                    ps.add_goods(good, amount)
                    break
        streams.append(ps)
    return streams


def __decode_control_parameter(data, length, width, cell_size) -> ControlParameter:
    """
    This method constructs a ControlParameter object from the given input.
    :param data: Dict parsed from JSON
    :return: ControlParameter
    """
    parameter = ControlParameter(length, width, cell_size)
    parameter.__dict__.clear()
    parameter.__dict__.update(data)
    return parameter


def __decode_weighting(data) -> Weighting:
    """
    This method constructs a Weighting object from the given input.
    :param data: Dict parsed from JSON
    :return: Weighting
    """
    weights = Weighting()
    weights.__dict__.clear()
    weights.__dict__.update(data)
    return weights


def __decode_layout_db(data, layout_model) -> LayoutDB:
    """
    This method constructs a LayoutDB object from the given input.
    :param data: Dict parsed from JSON
    :return: LayoutDB
    """
    db = LayoutDB()
    for lay in data['_saved_layouts']:
        db.add_layout(__decode_conveyor_system_layout(data['_saved_layouts'][lay], layout_model._goods_factory,
                                                      layout_model._conveyor_factory), False)
    return db


def __decode_restricted_area_list(data, layout) -> List[RestrictedArea]:
    """
    This method constructs a list of RestrictedArea from the given input.
    :param data: Dict parsed from JSON
    :return: List of RestrictedArea
    """
    res_areas = []
    for area in data:
        res_area = RestrictedArea(area['_position'][1], area['_position'][0], layout=layout, load=True)
        res_area._size = area['_size']
        res_area._rotation = area['_rotation']
        res_area.cell_value = area['cell_value']
        res_areas.append(res_area)
    return res_areas


def __decode_workplace_list(data, layout) -> List[Workplace]:
    """
    This method constructs a list of Workplace from the given input.
    :param data: Dict parsed from JSON
    :return: List of Workplace
    """
    workplaces = []
    for place in data:
        workplace = Workplace(place['_position'][1], place['_position'][0], layout=layout, load=True)
        workplace._size = place['_size']
        workplace._rotation = place['_rotation']
        workplace.cell_value = place['cell_value']
        workplaces.append(workplace)
    return workplaces


def __decode_deactivated_area_list(data, layout) -> List[DeactivatedArea]:
    """
    This method constructs a list of DeactivatedArea from the given input.
    :param data: Dict parsed from JSON
    :return: List of DeactivatedArea
    """
    deactivated_areas = []
    for area in data:
        deactivated_area = DeactivatedArea(area['_position'][1], area['_position'][0], layout=layout, load=True)
        deactivated_area._size = area['_size']
        deactivated_area._rotation = area['_rotation']
        deactivated_area.cell_value = area['cell_value']
        deactivated_areas.append(deactivated_area)
    return deactivated_areas


def __decode_connection_list(data, layout) -> List[SystemConnection]:
    """
    This method constructs a list of SystemConnection from the given input.
    :param data: Dict parsed from JSON
    :return: List of SystemConnection
    """
    connections = []
    for connect in data:
        connection = SystemConnection(connect['type'], connect['connected_position'][1],
                                      connect['connected_position'][0], layout, connect['direction'],
                                      load=True, idx=connect['id'])
        connections.append(connection)
    return connections


def __decode_goods_list(data) -> List[Goods]:
    """
    This method constructs a list of Goods from the given input.
    :param data: Dict parsed from JSON
    :return: List of Goods
    """
    goods = []
    for good_d in data:
        good = Goods(good_d['_size'][0], good_d['_size'][1], good_d['name'], good_d['type'],
                     empty_weight=good_d['_empty_weight'],
                     max_weight=good_d['_max_weight'], load_weight=good_d['_load_weight'])
        good.id = good_d['id']
        goods.append(good)
    return goods


def __decode_conveyor_list(data, layout) -> List[Conveyor]:
    """
    This method constructs a list of Conveyor from the given input.
    :param data: Dict parsed from JSON
    :return: List of Conveyor
    """
    conveyors = []
    for conv in data:
        conveyor = Conveyor(name=conv['name'], conv_type=conv['type'], trans_direction=conv['trans_direction'],
                            velocity=conv['velocity'], max_weight=conv['max_weight'],
                            component_costs=conv['component_costs'], x=conv['_position'][1],
                            y=conv['_position'][0], layout=layout,
                            width=conv['_size'][0], length=conv['_size'][1],
                            cell_value=conv['cell_value'], rotation=conv['_rotation'], load=True)
        conveyors.append(conveyor)
    return conveyors


def __decode_evaluation_result(data) -> EvaluationResult:
    """
    This method constructs a EvaluationResult object from the given input.
    :param data: Dict parsed from JSON
    :return: EvaluationResult
    """
    evaluation = EvaluationResult(data['layout_id'])
    evaluation.__dict__.clear()
    evaluation.__dict__.update(data)
    return evaluation
