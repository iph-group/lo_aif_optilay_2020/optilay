from optilay.model.layout_model import LayoutModel


class AMLExporter:
    """
    This class exports the LayoutModel as an AML file.
    """

    def __init__(self, path: str, layout: LayoutModel) -> None:
        """

        :param path: File path where to export to.
        :param layout: LayoutModel which is to be exported.
        """
        self.path = path
        self.layout = layout

    def export(self) -> None:
        """
        Starts the export.
        :return: None
        """
        raise NotImplementedError
