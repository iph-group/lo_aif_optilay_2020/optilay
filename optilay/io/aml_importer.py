from optilay.model.layout_model import LayoutModel


class AMLImporter:
    """
    This class imports the LayoutModel from an AML file.
    """

    def __init__(self, path: str) -> None:
        """

        :param path: File path where to import from.
        """
        self.path = path

    def imports(self) -> LayoutModel:
        """
        Starts the import.
        :return: The imported LayoutModel
        """
        raise NotImplementedError
