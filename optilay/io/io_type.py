from enum import Enum


class IOType(Enum):
    """
    The different storage formats that are available for saving as an enum.
    """
    JSON = 0
    AML = 1
