import json
from json import JSONEncoder

from optilay.evaluation.evaluation_method import EvaluationMethod
from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.control_parameter import ControlParameter, InitialisationMethod
from optilay.model.conveyor.conveyor import Conveyor
from optilay.model.conveyor.conveyor_direction import ConveyorDirection
from optilay.model.conveyor.conveyor_factory import ConveyorFactory
from optilay.model.conveyor.conveyor_type import ConveyorType
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.deactivated_area import DeactivatedArea
from optilay.model.goods.goods import Goods
from optilay.model.goods.goods_factory import GoodsFactory
from optilay.model.goods.goods_type import GoodsType
from optilay.model.layout_db import LayoutDB
from optilay.model.layout_model import LayoutModel
from optilay.model.restricted_area import RestrictedArea
from optilay.model.sytemconnection.packet_stream import PacketStream
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_direction import SystemConnectionDirection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType
from optilay.model.util.cell_size import CellSize
from optilay.model.weighting import Weighting
from optilay.model.workplace import Workplace
from optilay.optimisation.optimisation_method import OptimisationMethod


class JSONExporter:
    """
    This class exports the LayoutModel as a JSON file.
    """

    def __init__(self, path: str, layout: LayoutModel) -> None:
        """

        :param path: File path where to export to.
        :param layout: LayoutModel which is to be exported.
        """
        self.path = path
        self.layout = layout

    def export(self) -> None:
        """
        Starts the export.
        :return: None
        """
        with open(self.path, 'w') as f:
            json.dump(self.layout.__dict__, f, cls=JSONFactoryModelEncoder)


class JSONFactoryModelEncoder(JSONEncoder):
    """
    This class is for encoding the factory into an JSON file. And is needed because the standard
    JSONEncoder isn't capable of encoding all data types used within the FactoryModel object.
    """

    def default(self, obj):
        """
        This method is overwriting the default method of the JSONEncoder.
        It is needed for handling the different data types within the FactoryModel.
        :param obj: The Object which should be encoded.
        :return: The encoded object as String
        """
        if isinstance(obj, ControlParameter) or isinstance(obj, Weighting) \
                or isinstance(obj, LayoutDB) \
                or isinstance(obj, Goods) \
                or isinstance(obj, GoodsFactory) \
                or isinstance(obj, EvaluationResult):
            return obj.__dict__
        elif isinstance(obj, ConveyorSystemLayout):
            #obj._goods_factory = None
            #obj._conveyor_factory = None
            retdict = obj.__dict__.copy()
            del retdict['_goods_factory']
            del retdict['_conveyor_factory']
            return retdict
        elif isinstance(obj, PacketStream):
            retdict = {}
            retdict['_sink'] = obj._sink.id
            retdict['_source'] = obj._source.id
            retdict['_packets'] = {}
            retdict['_goods'] = []
            retdict['_path'] = obj._path
            for good, amount in obj._packets.items():
                retdict['_packets'][good.id] = amount
                retdict['_goods'].append(good)
            return retdict

        elif isinstance(obj, RestrictedArea) or isinstance(obj, Workplace) \
                or isinstance(obj, DeactivatedArea) \
                or isinstance(obj, SystemConnection) \
                or isinstance(obj, Conveyor):
            retdict = obj.__dict__.copy()
            if '_layout' in retdict:
                del retdict['_layout']
            return retdict
        elif isinstance(obj, CellSize) or isinstance(obj, ConveyorType) \
                or isinstance(obj, OptimisationMethod) \
                or isinstance(obj, EvaluationMethod) \
                or isinstance(obj, ConveyorDirection) \
                or isinstance(obj, GoodsType) \
                or isinstance(obj, SystemConnectionType) \
                or isinstance(obj, InitialisationMethod) \
                or isinstance(obj, SystemConnectionDirection):
            return {"__enum__": str(obj)}
        elif isinstance(obj, ConveyorFactory):
            return {}

        return JSONEncoder.default(self, obj)
