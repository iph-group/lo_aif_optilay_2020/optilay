from enum import Enum


class SystemConnectionDirection(Enum):
    """

    """
    Horizontal = 0
    Vertical = 1
    Auto = 2
