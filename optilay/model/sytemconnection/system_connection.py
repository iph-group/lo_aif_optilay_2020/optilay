import uuid

from optilay.model.sytemconnection.system_connection_direction import SystemConnectionDirection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType
from optilay.model.util.cell_values import DEACTIVATED_AREA, WORKPLACE, RESTRICTED_AREA
from optilay.util.exceptions import NoAutoDirection


class SystemConnection:
    """

    """

    def __init__(self, c_type: SystemConnectionType, x: int, y: int, layout,
                 direction: SystemConnectionDirection = SystemConnectionDirection.Auto, load=False, idx=None, *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.connected_position = [y, x]
        self._layout = layout
        if not load:
            check_systemconnectionable_position(x, y, layout)
        self.type: SystemConnectionType = c_type

        self.direction: SystemConnectionDirection
        if direction == SystemConnectionDirection.Auto:
            self.direction = self.detect_direction()
        elif direction == SystemConnectionDirection.Horizontal or direction == SystemConnectionDirection.Vertical:
            if not load:
                self.check_direction(direction)
            self.direction = direction
        else:
            raise ValueError("Unknown SystemConnectionDirection: {}".format(direction))

        if load:
            self.id = idx
        else:
            self.id: int = uuid.uuid4().int

    def detect_direction(self) -> SystemConnectionDirection:
        """

        :return:
        """
        top, bottom, left, right = False, False, False, False
        layout = self._layout
        x = self.connected_position[1]
        y = self.connected_position[0]
        if x - 1 < 0 or layout[y][x - 1] == DEACTIVATED_AREA:
            left = True
        if x + 1 >= layout.shape[1] or layout[y][x + 1] == DEACTIVATED_AREA:
            right = True
        if y - 1 < 0 or layout[y - 1][x] == DEACTIVATED_AREA:
            bottom = True
        if y + 1 >= layout.shape[0] or layout[y + 1][x] == DEACTIVATED_AREA:
            top = True

        hor = left or right
        vert = top or bottom

        if hor and vert:
            raise NoAutoDirection(
                "No auto placing possible because SystemConnectionDirection can be vertical and horizontal")
        elif hor:
            return SystemConnectionDirection.Horizontal
        elif vert:
            return SystemConnectionDirection.Vertical
        else:
            raise ValueError("SystemConnection must be placed on a wall")

    def check_direction(self, direction: SystemConnectionDirection) -> None:
        """

        :param direction:
        :return:
        """
        layout = self._layout
        x = self.connected_position[1]
        y = self.connected_position[0]
        if direction == SystemConnectionDirection.Horizontal:
            if x - 1 < 0 or x + 1 >= layout.shape[1] or layout[y][x + 1] == DEACTIVATED_AREA or layout[y][
                x - 1] == DEACTIVATED_AREA:
                return
        elif direction == SystemConnectionDirection.Vertical:
            if y - 1 < 0 or y + 1 >= layout.shape[0] or layout[y + 1][x] == DEACTIVATED_AREA or layout[y - 1][
                x] == DEACTIVATED_AREA:
                return
        raise ValueError("SystemConnection with this SystemConnectionDirection cannot be placed here")

    def tool_tip(self):
        # +1 so that the coordiants for the user start at 1
        return "ID: {}\nConnected position: ({}, {})\nType: {}\nDirection: {}".format(self.id,
                                                                                      self.connected_position[1] + 1,
                                                                                      self.connected_position[0] + 1,
                                                                                      self.type.name,
                                                                                      self.direction.name)

    def __repr__(self) -> str:
        return "ID: {}\nConnected position: ({}, {})\nType: {}\nDirection: {}".format(self.id,
                                                                                      self.connected_position[1],
                                                                                      self.connected_position[0],
                                                                                      self.type.name,
                                                                                      self.direction.name)


def check_systemconnectionable_position(x: int, y: int, layout) -> None:
    """

    :return:
    """
    if layout[y][x] == DEACTIVATED_AREA or layout[y][x] == WORKPLACE or layout[y][x] == RESTRICTED_AREA:
        raise ValueError("SystemConnection cannot be placed on anything other than an empty cell or conveyor.")
    if x >= layout.shape[1] or y >= layout.shape[0] or x < 0 or y < 0:
        raise ValueError("System connection must be placed inside layout!")
    if 0 < x < layout.shape[1] - 1 and 0 < y < layout.shape[0] - 1:
        if layout[y][x + 1] != DEACTIVATED_AREA and layout[y][x - 1] != DEACTIVATED_AREA and layout[y + 1][
            x] != DEACTIVATED_AREA and layout[y - 1][x] != DEACTIVATED_AREA:
            raise ValueError("System connection must be placed on a wall!")
