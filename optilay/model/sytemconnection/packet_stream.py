from typing import Dict, List

from optilay.model.goods.goods import Goods
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType


class PacketStream:
    def __init__(self, sink: SystemConnection, source: SystemConnection):
        if source.type != SystemConnectionType.SOURCE:
            raise ValueError("Only SystemConnections with ConnectionType.SOURCE can be added as source!")
        if sink.type != SystemConnectionType.SINK:
            raise ValueError("Only SystemConnections with ConnectionType.SINK can be added as sink!")

        self._sink = sink
        self._source = source

        self._packets: Dict[Goods, int] = {}
        self._path: List[List[int]] = []

    def add_goods(self, goods: Goods, amount: int = 1) -> None:
        if amount < 0:
            raise ValueError("The amount of good between two SystemConnections must be greater than or equal to zero.")
        self._packets[goods] = amount

    def remove_goods(self, goods: Goods) -> None:
        self._packets.pop(goods)

    def get_goods(self) -> Dict[Goods, int]:
        return self._packets

    def set_goods_amount(self, goods: Goods, amount: int) -> None:
        self.add_goods(goods, amount)
        if not self._packets:
            del self

    def get_goods_by_id(self, idx: int):
        for good, amount in self._packets.items():
            if good.id == idx:
                return good, amount
        return None, None

    def get_sum_of_good_amounts(self):
        goods_amount = 0
        for good, amount in self._packets.items():
            goods_amount += amount
        return goods_amount

    def remove_goods_by_id(self, idx: int) -> None:
        for good, amount in self._packets.items():
            if good.id == idx:
                self.remove_goods(good)
                return

    def is_empty(self) -> bool:
        return not self._packets or sum(self._packets.values()) <= 0

    def add_path(self, path):
        """
        overwrites current path!
        """
        self._path = path

    def delete_path(self):
        self._path = []

    def get_path(self) -> List[List[int]]:
        return self._path

    @property
    def source(self) -> SystemConnection:
        """

        :return:
        """
        return self._source

    @source.setter
    def source(self, source: SystemConnection) -> None:
        """

        :param source:
        :return:
        """
        raise ValueError("The source of a SystemConnection is non mutable.")

    @property
    def sink(self) -> SystemConnection:
        """

        :return:
        """
        return self._sink

    @sink.setter
    def sink(self, sink: SystemConnection) -> None:
        """

        :param sink:
        :return:
        """
        raise ValueError("The sink of a SystemConnection is non mutable.")
