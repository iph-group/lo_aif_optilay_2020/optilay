import operator
from time import sleep
from typing import List, Dict, Tuple

from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.util.cell_size import CellSize


class LayoutDB:
    """
    LayoutDB is a storage unit for all ConveyorSystemLayout and their EvaluationResult.
    """

    def __init__(self) -> None:
        """

        """
        self._saved_layouts: Dict[int, ConveyorSystemLayout] = {}

    def clear(self) -> None:
        """
        Deletes all contents in this LayoutDB,
        :returns: None
        """
        self._saved_layouts = {}

    def add_layout(self, layout: ConveyorSystemLayout, overwrite=False) -> None:
        """
        Adds a ConveyorSystemLayout to the memory.
        :param overwrite: If true, overwriting other ConveyorSystemLayout with same layout_id
        :param layout: ConveyorSystemLayout to be saved.
        :return: None
        """
        if layout.layout_id in self._saved_layouts:
            if overwrite:
                self._saved_layouts[layout.layout_id] = layout
            else:
                raise KeyError(
                    "Layout_id of ConveyorSystemLayout is already in LayoutDB and the overwrite flag is not set.")
        else:
            self._saved_layouts[layout.layout_id] = layout

    def remove_layout_by_id(self, idx: int) -> None:
        """
        Removes a ConveyorSystemLayout from the memory by id.
        :param idx: Id of ConveyorSystemLayout which should get deleted.
        :return: None
        """
        if idx in self._saved_layouts:
            self._saved_layouts.pop(idx)

    def remove_layout(self, layout: ConveyorSystemLayout) -> None:
        """
        Removes a ConveyorSystemLayout from the memory by ConveyorSystemLayout.
        :param layout: ConveyorSystemLayout which should get deleted.
        :return: None
        """
        self.remove_layout_by_id(layout.layout_id)

    def get_layout(self, idx: int) -> ConveyorSystemLayout:
        """
        Returns ConveyorSystemLayout with corresponding id.
        :param idx: Id des ConveyorSystemLayout
        :return: ConveyorSystemLayout with corresponding id
        """
        return self._saved_layouts[idx]

    def get_all_layouts(self) -> List[ConveyorSystemLayout]:
        return list(self._saved_layouts.values())

    def get_all_layouts_idx(self) -> list:
        layouts_idx = []
        for layout_idx in self._saved_layouts:
            layouts_idx.append(layout_idx)
        return layouts_idx
        # return [a_dict["layout_id"] for a_dict in self._saved_layouts]

    def get_best_layouts(self, quantity: int) -> List[ConveyorSystemLayout]:
        """
        Returns the best ConveyorSystemLayout.
        :param quantity: Number of best ConveyorSystemLayout.
        :return: The best ConveyorSystemLayout.
        """
        return sorted(self._saved_layouts.values(), key=operator.attrgetter('evaluation.weighted_sum'),
                      reverse=True)[:quantity]

    def get_best_layouts_idx(self, quantity: int) -> list:
        layout_list = self.get_best_layouts(quantity)
        layouts_idx = []
        for layout in layout_list:
            layouts_idx.append(layout.layout_id)
        return layouts_idx

    def get_best_layouts_idx_weight_sum(self, quantity: int) -> list:
        layout_list = self.get_best_layouts(quantity)
        layouts_idx = []
        for layout in layout_list:
            layouts_idx.append((layout.layout_id, layout.evaluation.weighted_sum))
        return layouts_idx

    def merge_db(self, lay_db, overwrite=False) -> None:
        """
        Integrates other LayoutDB into this LayoutDB.
        :param overwrite: If true, overwriting entries with same id in this db, else keep these ones.
        :param lay_db: Other LayoutDB
        :return: None
        """
        if overwrite:
            self._saved_layouts |= lay_db._saved_layouts
        else:
            self._saved_layouts = lay_db._saved_layouts | self._saved_layouts
