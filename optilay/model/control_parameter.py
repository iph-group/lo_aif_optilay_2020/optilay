from enum import Enum
from typing import List

from optilay.evaluation.evaluation_method import EvaluationMethod
from optilay.model.conveyor.conveyor_type import ConveyorType
from optilay.model.util.cell_size import CellSize
from optilay.optimisation.optimisation_method import OptimisationMethod


class InitialisationMethod(Enum):
    A_STAR = 0
    RANDOM = 1
    ALL_MODULAR = 2


class ControlParameter:
    """
    ControlParameter contains all parameters necessary for optimisation and evaluation.
    """

    def __init__(self, length: int, width: int, cell_size: CellSize) -> None:
        """

        """
        self._length_cell = length / cell_size.value
        self._width_cell = width / cell_size.value
        # TODO: set standard values
        self.worker: int = 2
        self.eval_method: EvaluationMethod = EvaluationMethod.Throughput
        self.opt_method: OptimisationMethod = OptimisationMethod.GeneticAlgorithm

        # extended
        self.num_compare_layouts = 5

        # Genetic_Algorithm (default)
        self._number_of_generations: int = 1000
        self._population_size: int = 20
        self._mutation_rate: float = 0.7
        self._maximal_number_of_mutations: int = 2
        self._crossover_rate: float = 0.7
        self._crossover_two_point_operator_length: float = 2

        # Random_Placement
        self._number_of_iterations: int = 10000
        self._neighborhood_search = {"random_positioning": 0.33,
                                     "shift": 0.33,
                                     "swap": 0.34}

        # Initialisation
        self._initialisation_method: InitialisationMethod = InitialisationMethod.A_STAR
        self._path_search_counter: int = 5

        # Useable Conveyortypes
        self.conv_types: List[str] = []

    @property
    def number_of_generations(self):
        return self._number_of_generations

    @number_of_generations.setter
    def number_of_generations(self, value):
        if 1 <= value <= 10000:
            self._number_of_generations = value
        else:
            raise ValueError("Number of generations must be between 1 and 10.000")

    @property
    def population_size(self):
        return self._population_size

    @population_size.setter
    def population_size(self, value):
        if value % 2 != 0:
            raise ValueError("Population size must dividable by 2")
        if 1 <= value <= 1000:
            self._population_size = value
        else:
            raise ValueError("Population size must be between 1 and 1.000")

    @property
    def mutation_rate(self):
        return self._mutation_rate

    @mutation_rate.setter
    def mutation_rate(self, value):
        if 0 <= value <= 1:
            self._mutation_rate = value
        else:
            raise ValueError("Mutation rate must be between 1 and 1")

    @property
    def maximal_number_of_mutations(self):
        return self._maximal_number_of_mutations

    @maximal_number_of_mutations.setter
    def maximal_number_of_mutations(self, value):
        if 1 <= value <= (self._width_cell * self._length_cell):
            self._maximal_number_of_mutations = value
        else:
            raise ValueError(
                "Maximal number of mutations must be between 1 and {}".format(self._width_cell * self._length_cell))

    @property
    def crossover_rate(self):
        return self._crossover_rate

    @crossover_rate.setter
    def crossover_rate(self, value):
        if 0 <= value <= 1:
            self._crossover_rate = value
        else:
            raise ValueError("Crossover rate must be between 1 and 1")

    @property
    def crossover_two_point_operator_length(self):
        return self._crossover_two_point_operator_length

    @crossover_two_point_operator_length.setter
    def crossover_two_point_operator_length(self, value):
        if 1 <= value <= (self._width_cell * self._length_cell):
            self._crossover_two_point_operator_length = value
        else:
            raise ValueError(
                "Crossover two-point-operator length must be between 1 and {}".format(
                    self._width_cell * self._length_cell))

    @property
    def number_of_iterations(self):
        return self._number_of_iterations

    @number_of_iterations.setter
    def number_of_iterations(self, value):
        if 0 <= value <= 100000:
            self._number_of_iterations = value
        else:
            raise ValueError("Mutation rate must be between 1 and 100.000")

    @property
    def neighborhood_search(self):
        return self._neighborhood_search

    @neighborhood_search.setter
    def neighborhood_search(self, value):
        sums = 0
        for typ in ["random_positioning", "shift", "swap"]:
            if typ not in value:
                raise ValueError(
                    "'random_positioning', 'shift', 'swap' must be included in the neighborhood_search dict")
            sums += value[typ]
        if sums != 1:
            raise ValueError("The sum of the weighting from 'random_positioning', 'shift', 'swap' must be 1")
        self._neighborhood_search = value

    @property
    def initialisation_method(self):
        return self._initialisation_method

    @initialisation_method.setter
    def initialisation_method(self, value):
        if isinstance(value, InitialisationMethod):
            self._initialisation_method = value
        else:
            raise ValueError(
                "initialisation_method must be of type InitialisationMethod and not {}".format(type(value)))

    @property
    def path_search_counter(self):
        return self._path_search_counter

    @path_search_counter.setter
    def path_search_counter(self, value):
        if 1 <= value <= 50:
            self._path_search_counter = value
        else:
            raise ValueError("Path search counter rate must be between 1 and 50")
