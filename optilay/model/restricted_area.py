from optilay.model.util.cell_values import RESTRICTED_AREA
from optilay.model.util.placeable import Placeable


class RestrictedArea(Placeable):
    """

    """

    def __init__(self, *args, **kwargs) -> None:
        kwargs['cell_value'] = RESTRICTED_AREA
        kwargs['length'] = 1
        kwargs['width'] = 1
        kwargs['rotation'] = 0

        super().__init__(*args, **kwargs)
