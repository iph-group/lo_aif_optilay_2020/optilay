import copy
from typing import List

from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.control_parameter import ControlParameter
from optilay.model.conveyor.conveyor_factory import ConveyorFactory
from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.goods.goods_factory import GoodsFactory
from optilay.model.layout_db import LayoutDB
from optilay.model.util.cell_size import CellSize
from optilay.model.weighting import Weighting


class LayoutModel:
    """
    LayoutModel contains all information about the layout in Optilay.
    """

    def __init__(self, length: int = 2400, width: int = 2400, cell_size: CellSize = CellSize.CELL_400) -> None:
        """

        """
        self._length: int = length
        self._width: int = width
        self._cell_size: CellSize = cell_size
        self._goods_factory: GoodsFactory = GoodsFactory(self._cell_size)
        self._conveyor_factory: ConveyorFactory = ConveyorFactory(self)

        self.layout: ConveyorSystemLayout = ConveyorSystemLayout(length, width, cell_size,
                                                                 goods_factory=self._goods_factory,
                                                                 conv_factory=self._conveyor_factory)
        self.parameter: ControlParameter = ControlParameter(self._length, self._width, self._cell_size)
        self.weighting: Weighting = Weighting()
        self.layouts: LayoutDB = LayoutDB()
        self.best_result: EvaluationResult = EvaluationResult(0)
        # self.layouts.add_layout(self.layout)

    def get_parameter(self) -> ControlParameter:
        """
        Current ControlParameter for Evaluation und Optimisation
        :return: Current ControlParameter
        """
        return self.parameter

    def get_weighting(self) -> Weighting:
        """
        Current Weighting for the evaluation.
        :return: Current Weighting
        """
        return self.weighting

    def get_current_layout(self) -> ConveyorSystemLayout:
        """
        Returns the actual ConveyorSystemLayout.
        :return: Actual ConveyorSystemLayout
        """
        return self.layout

    def load_layout(self, idx: int):
        self.layout = self.layouts.get_layout(idx)

    def new_layout(self, from_actual_layout=False, from_actual_layout_without_conveyor=True, set_to_actual=False,
                   save_to_db=True) -> ConveyorSystemLayout:
        """
        Creates a new ConveyorSystemLayout inside LayoutModel
        :param from_actual_layout_without_conveyor: Creates deepcopy of the active ConveyorSystemLayout,
                but without conveyors
        :param save_to_db: Saves new ConveyorSystemLayout to LayoutDB
        :param from_actual_layout: Creates deepcopy of the active ConveyorSystemLayout
        :param set_to_actual: Sets the new ConveyorSystemLayout to the new active layout.
        :return: Id of the new conveyorSystemLayout
        """
        assert not (from_actual_layout and from_actual_layout_without_conveyor)
        if from_actual_layout:
            new_lay = copy.deepcopy(self.layout)
            new_lay.generate_new_id()
            new_lay.reset_paths()
        elif from_actual_layout_without_conveyor:
            new_lay = copy.deepcopy(self.layout)
            new_lay.generate_new_id()
            new_lay.remove_all_conveyors()
            new_lay.reset_evaluation()
            new_lay.reset_paths()
        else:
            new_lay = ConveyorSystemLayout(self._length, self._width, self._cell_size,
                                           goods_factory=self._goods_factory, conv_factory=self._conveyor_factory)

        if save_to_db:
            self.layouts.add_layout(new_lay)

        if set_to_actual:
            if not save_to_db:
                self.layout = new_lay
            else:
                self.load_layout(new_lay.layout_id)

        return new_lay

    def get_db(self) -> LayoutDB:
        """
        Returns the LayoutDB with all available ConveyorSystemLayout.
        :return: LayoutDB from this LayoutModel
        """
        return self.layouts

    def get_best_layouts(self, quantity: int = 1) -> List[ConveyorSystemLayout]:
        """

        :param quantity: Number of best layout to be returned.
        :return: List of best ConveyorSystemLayout
        """
        return self.layouts.get_best_layouts(quantity=quantity)

    def get_best_layouts_idx(self, quantity: int = 1) -> list:
        return self.layouts.get_best_layouts_idx(quantity=quantity)

    def get_best_layouts_idx_weight_sum(self, quantity: int = 1) -> list:
        return self.layouts.get_best_layouts_idx_weight_sum(quantity=quantity)

    def get_all_layouts_idx(self):
        return self.layouts.get_all_layouts_idx()

    def get_goods_factory(self) -> GoodsFactory:
        return self._goods_factory
