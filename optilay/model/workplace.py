from optilay.model.util.cell_values import WORKPLACE
from optilay.model.util.placeable import Placeable


class Workplace(Placeable):
    """

    """

    def __init__(self, *args, **kwargs) -> None:
        kwargs['cell_value'] = WORKPLACE
        kwargs['length'] = 1
        kwargs['width'] = 1
        kwargs['rotation'] = 0

        super().__init__(*args, **kwargs)
