import collections
import math
import uuid
from typing import List, Dict, Union, Tuple

import numpy as np
import random

from optilay.evaluation.evaluation_result import EvaluationResult
from optilay.model.conveyor.conveyor import Conveyor
from optilay.model.conveyor.conveyor_direction import ConveyorDirection
from optilay.model.conveyor.conveyor_factory import ConveyorFactory
from optilay.model.conveyor.conveyor_type import ConveyorType
from optilay.model.deactivated_area import DeactivatedArea
from optilay.model.goods.goods import Goods
from optilay.model.goods.goods_factory import GoodsFactory
from optilay.model.goods.goods_type import GoodsType
from optilay.model.restricted_area import RestrictedArea
from optilay.model.sytemconnection.packet_stream import PacketStream
from optilay.model.sytemconnection.system_connection import SystemConnection
from optilay.model.sytemconnection.system_connection_direction import SystemConnectionDirection
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType
from optilay.model.util.cell_size import CellSize
from optilay.model.util.cell_values import EMPTY_CELL
from optilay.model.util.fixed_values import MAX_SIZE
from optilay.model.util.cell_values import DEACTIVATED_AREA, WORKPLACE, RESTRICTED_AREA
from optilay.model.workplace import Workplace

OUT_OF_AREA_MSG = "The workspace must be less than {}m^2. Actual: {}m^2"


class ConveyorSystemLayout:
    """
    The ConveyorSystemLayout is a layout for the conveyor placement.
    The ConveyorSystemLayout is to be used completely without the direct editing of the attributes.
    Therefore a variety of methods are available.
    """

    def __init__(self, length: int, width: int, cell_size: CellSize, goods_factory: GoodsFactory,
                 conv_factory: ConveyorFactory) -> None:
        """

        :param length:
        :param width:
        :param cell_size: In mm
        """
        # Constant values
        self.MAXSIZE = MAX_SIZE

        # Creation Checks
        if (width * length) > self.MAXSIZE:
            raise ValueError(OUT_OF_AREA_MSG.format(self.MAXSIZE / 1000000, (width * length) / 1000000))

        # System Information
        self.layout_id: int = uuid.uuid1().int
        self.evaluation: EvaluationResult = EvaluationResult(self.layout_id)

        # Basis information
        self._length: int = length
        self._width: int = width
        self._cell_size: CellSize = cell_size

        # Here are all Information about the Layout stored
        # Read only!
        self.res_areas: List[RestrictedArea] = []
        self.workplaces: List[Workplace] = []
        self.deactivated: List[DeactivatedArea] = []
        self.connection: List[SystemConnection] = []
        self.packet_stream: List[PacketStream] = []
        self.conveyors: List[Conveyor] = []

        # Creator classes for Goods and Conveyor
        self._goods_factory: GoodsFactory = goods_factory
        self._conveyor_factory: ConveyorFactory = conv_factory

    @property
    def layout(self) -> np.ndarray:
        """
        Here the current layout is generated with the placements of the individual objects.
        ATTENTION: If you call the layout frequently, you should cache it to avoid
        that the generation is performed frequently.
        :return: Numpy array of layout
        """
        layout = np.full((math.ceil(self._width / self._cell_size.value),
                          math.ceil(self.length / self._cell_size.value)), EMPTY_CELL)
        for des_area in self.deactivated:
            layout[des_area.y:des_area.y + des_area.width,
            des_area.x:des_area.x + des_area.length] = des_area.cell_value
        for res_area in self.res_areas:
            layout[res_area.y:res_area.y + res_area.width,
            res_area.x:res_area.x + res_area.length] = res_area.cell_value
        for work_area in self.workplaces:
            layout[work_area.y:work_area.y + work_area.width,
            work_area.x:work_area.x + work_area.length] = work_area.cell_value
        for conv in self.conveyors:
            layout[conv.y:conv.y + conv.width, conv.x:conv.x + conv.length] = conv.cell_value
        return layout

    @layout.setter
    def layout(self, value):
        """
        The layout should not be changed by the matrix, that's what the other methods are for.
        :param value:
        :return:
        """
        raise AttributeError("The layout matrix is read only")

    def get_available_conveyor(self) -> List[str]:
        """
        Here a list of all possible conveyors placeable on the current layout is created.
        Conveyor types are defined in the ConveyorFactory.
        :return: List of conveyor types
        """
        return self._conveyor_factory.get_available_conveyor()

    def get_all_conveyor(self) -> List[str]:
        """
        A list of all possible conveyors is created here. The conveyor types are defined in the ConveyorFactory.
        :return: List of conveyor types
        """
        return self._conveyor_factory.get_all_conveyor()

    def get_conveyor_information(self, name: str):
        """
        The method can be used to obtain information about a specific type of conveyor.
        If no conveyor with the ID is available an exception is thrown.
        :param name: Name of the conveyor type about which information is to be given
        :return: A dict with all attributes of the Conveyor type
        """
        return self._conveyor_factory.get_conveyor_information(name)

    def add_conveyor(self, name: str, x: int, y: int, rotation: int = 0) -> Conveyor:
        """
        Adds a new Conveyor with the given specifications to the layout.
        If the Conveyor cannot be placed on the given Position an Exception will be thrown.
        :param name: Name of the Conveyor type
        :param x: X position of the upper left Conveyor corner
        :param y: Y position of the upper left Conveyor corner
        :param rotation: Sets the rotation
        :return: Returns the created Conveyor
        """
        conv = self._conveyor_factory.create_conveyor(name, x, y, rotation, self)
        if conv.length == 1 and conv.width == 1:
            if self.cell_is_empty(conv.x, conv.y):
                self.conveyors.append(conv)
                return conv
            else:
                raise ValueError("Conveyor cannot be placed here, because the given Position is not empty.")
        else:
            if self.cells_are_empty(conv.x, conv.y, conv.length, conv.width):
                self.conveyors.append(conv)
                return conv
            else:
                raise ValueError("Conveyor cannot be placed here, because the given Position is not empty.")

    def get_conveyor_by_id(self, idx: int) -> Conveyor:
        """
        Returns the Conveyor object from the layout with the specified ID.
        If no Conveyor with the ID is
        :param idx: ID of the Conveyor to be returned
        :return: Conveyor from the layout.
        """
        for conv in self.conveyors:
            if conv.cell_value == idx:
                return conv
        raise ValueError("There is no Conveyor with ID {}".format(idx))

    def get_conveyor_by_position(self, x: int, y: int) -> Conveyor:
        """
        Returns the conveyor from the layout that is placed at the given location.
        If no conveyor is available at the location an exception is thrown.
        :param x: X position of the upper left Conveyor corner
        :param y: Y position of the upper left Conveyor corner
        :return: Conveyor from the layout.
        """
        return self.get_conveyor_by_id(self.layout[y][x])

    def remove_conveyor_by_id(self, idx: int) -> None:
        """
        Deletes the Conveyor with the given ID from the layout.
        :param idx: ID of the Conveyor to delete
        :return: None
        """
        for conv in self.conveyors:
            if conv.cell_value == idx:
                self.conveyors.remove(conv)
                break
        else:
            raise ValueError("There is no Conveyor with ID {}".format(idx))

    def remove_conveyor_by_position(self, x: int, y: int):
        """
        Deletes the Conveyor on the given position from the layout.
        :param x: X position of the upper left Conveyor corner
        :param y: Y position of the upper left Conveyor corner
        :return: None
        """
        self.remove_conveyor_by_id(self.layout[y][x])

    def add_workplace(self, x: int, y: int):
        """
        Adds Workplace (1x1 cells) on the given position.
        If the Workplace cannot be placed on the given position an Exception will be thrown.
        :param x: X position of the workplace
        :param y: Y position of the workplace
        :return: None
        """
        if self.cell_is_empty(x, y):
            self.workplaces.append(Workplace(x, y, layout=self))
        else:
            raise ValueError("Workplace cannot be placed here, because the given Position is not empty.")

    def remove_workplace(self, x: int, y: int):
        """
        Deletes the Workplace on the given position from the layout.
        :param x: X position of the workplace
        :param y: Y position of the workplace
        :return: None
        """
        for workplace in self.workplaces:
            if workplace.x == x and workplace.y == y:
                self.workplaces.remove(workplace)
                break

    def add_restricted_area(self, x: int, y: int):
        """
        Adds RestrictedArea (1x1 cells) on the given position.
        If the RestrictedArea cannot be placed on the given position an Exception will be thrown.
        :param x: X position of the RestrictedArea
        :param y: Y position of the RestrictedArea
        :return: None
        """
        if self.cell_is_empty(x, y):
            self.res_areas.append(RestrictedArea(x, y, layout=self))
        else:
            raise ValueError("RestrictedArea cannot be placed here, because the given Position is not empty.")

    def remove_restricted_area(self, x: int, y: int):
        """
        Deletes the RestrictedArea on the given position from the layout.
        :param x: X position of the RestrictedArea
        :param y: Y position of the RestrictedArea
        :return: None
        """
        for res_area in self.res_areas:
            if res_area.x == x and res_area.y == y:
                self.res_areas.remove(res_area)
                break

    def add_deactivated_area(self, x: int, y: int):
        """
        Adds DeactivatedArea (1x1 cells) on the given position.
        If the DeactivatedArea cannot be placed on the given position an Exception will be thrown.
        :param x: X position of the DeactivatedArea
        :param y: Y position of the DeactivatedArea
        :return: None
        """
        if self.cell_is_empty(x, y):
            self.deactivated.append(DeactivatedArea(x, y, layout=self))
        else:
            raise ValueError("DeactivatedArea cannot be placed here, because the given Position is not empty.")

    def remove_deactivated_area(self, x: int, y: int):
        """
        Deletes the DeactivatedArea on the given position from the layout.
        :param x: X position of the DeactivatedArea
        :param y: Y position of the DeactivatedArea
        :return: None
        """
        for des_area in self.deactivated:
            if des_area.x == x and des_area.y == y:
                self.deactivated.remove(des_area)
                break

    def get_available_goods(self) -> List[str]:
        """
        Here a list of all possible Goods usable in the current layout is created.
        Goods types are defined in the GoodsFactory.
        :return: List of Goods types
        """
        return self._goods_factory.get_available_goods()

    def get_all_goods(self) -> List[str]:
        """
        A list of all possible Goods is created here. The Goods types are defined in the GoodsFactory.
        :return: List of Goodstypes
        """
        return self._goods_factory.get_all_goods()

    def get_goods_information(self, name: str):
        """
        The method can be used to obtain information about a specific type of Goods.
        If no Goods with the name is available an exception is thrown.
        :param name: Name of the Goods type about which information is to be given
        :return: A dict with all attributes of the Goods type
        """
        return self._goods_factory.get_goods_information(name)

    def get_input_of_system_connection(self, system_connection: SystemConnection) -> Dict[Goods, int]:
        """
        Get the goods and amounts, flow into the system through this SystemConnection
        :param system_connection:
        :return: Dict of Goods and their amounts
        """
        if system_connection.type == SystemConnectionType.SINK:
            return {}
        else:
            streams = self._get_packet_stream_of(system_connection)
            streams = [stream.get_goods() for stream in streams]
            counter = collections.Counter()
            for d in streams:
                counter.update(d)

            return dict(counter)

    def get_output_of_system_connection(self, system_connection: SystemConnection) -> Dict[Goods, int]:
        """
        Get the goods and amounts, flow out of the system through this SystemConnection
        :param system_connection:
        :return: Dict of Goods and their amounts
        """
        if system_connection.type == SystemConnectionType.SOURCE:
            return {}
        else:
            streams = self._get_packet_stream_of(system_connection)
            streams = [stream.get_goods() for stream in streams]
            counter = collections.Counter()
            for d in streams:
                counter.update(d)

            return dict(counter)

    def _get_packet_stream_of(self, system_connection: SystemConnection) -> List[PacketStream]:
        """
        Returns all PacketStream where the given SystemConnection is a sink or a source.
        :param system_connection: SystemConnection, where the corresponding PacketStreams should be returned.
        :return: List of PacketStream, where the given SystemConnection is either sink or source.
        """
        return [stream for stream in self.packet_stream if
                (stream.sink == system_connection or stream.source == system_connection)]

    def _get_packet_stream_between(self, sink: SystemConnection, source: SystemConnection) -> PacketStream:
        """
        Returns the PacketStream between the given sink and source SystemConnections.
        :param sink: Sink SystemConnection
        :param source: Source SystemConnection
        :return: PacketStream between sink and source.
        """
        streams = [stream for stream in self.packet_stream if (stream.sink == sink and stream.source == source)]
        stream_len = len(streams)
        if stream_len == 0:
            stream = PacketStream(sink, source)
            self.packet_stream.append(stream)
        elif stream_len == 1:
            stream = streams[0]
        else:
            raise ValueError("There can only one packet stream between sink and source")

        return stream

    def add_goods_between(self, sink: SystemConnection, source: SystemConnection, goods_name: str, load=1,
                          amount: int = 1) -> None:
        """
        Adds a Good to the connection between the given sink and source
        :param sink: Sink SystemConnection
        :param source: Source SystemConnection
        :param goods_name: The Goods type/template to be used for creating the new good.
        :param load: The weight load on the created Goods
        :param amount: The amount, how often this Good appears on the Connection between the sink and source.
        :return: None
        """
        stream = self._get_packet_stream_between(sink, source)
        stream.add_goods(self._goods_factory.create_goods(goods_name, load), amount)

    def remove_goods_between(self, sink: SystemConnection, source: SystemConnection, goods: Goods) -> None:
        """
        Removes the given Goods from the Connection between the given sink and source
        :param sink: Sink SystemConnection
        :param source: Source SystemConnection
        :param goods: The Goods to be removed.
        :return: None
        """
        stream = self._get_packet_stream_between(sink, source)
        stream.remove_goods(goods)
        if stream.is_empty():
            self.packet_stream.remove(stream)

    def remove_all_goods_between(self, sink: SystemConnection, source: SystemConnection) -> None:
        """
        Removes all goods, which going from the sink to the source.
        :param sink: Sink SystemConnection
        :param source: Source SystemConnection
        :return: None
        """
        for stream in self.packet_stream:
            if stream.sink == sink and stream.source == source:
                self.packet_stream.remove(stream)
                return

    def set_goods_amount_by_id(self, idx: int, new_amount: int) -> None:
        for packstream in self.packet_stream:
            searched_good, amount = packstream.get_goods_by_id(idx)
            if searched_good:
                packstream.set_goods_amount(searched_good, new_amount)
                return

    def set_goods_amount_between(self, sink: SystemConnection, source: SystemConnection, goods: Goods,
                                 amount: int) -> None:
        """
        Changes the amount of the given Goods on the connection between the given sink and source.
        :param sink: Sink SystemConnection
        :param source: Source SystemConnection
        :param goods: The Goods where the amount should be set.
        :param amount: The new amount of the given Good on the connection between the sink and source
        :return: None
        """
        stream = self._get_packet_stream_between(sink, source)
        stream.set_goods_amount(goods, amount)
        if stream.is_empty():
            self.packet_stream.remove(stream)

    def get_goods_between(self, sink: SystemConnection, source: SystemConnection) -> Dict[Goods, int]:
        """
        Returns the Goods and their amounts between the given SystemConnections.
        :param sink: Sink SystemConnection
        :param source: Source SystemConnection
        :return: A dict with the Goods between the SystemConnections. The Goods are the keys of the dict and the value
        is the amount of these Goods between the SystemConnections
        """
        stream = self._get_packet_stream_between(sink, source)
        return stream.get_goods()

    def add_system_connection(self, x: int, y: int, c_type: SystemConnectionType,
                              direction: SystemConnectionDirection = SystemConnectionDirection.Auto) -> SystemConnection:
        """
        Adds a SystemConnection on the given position. The position needs to be on the outer walls of the layout.
        If an error occurs during initialisation of the SystemConnection an Exception will be thrown.
        :param x: X position of the SystemConnection
        :param y: Y position of the SystemConnection
        :param c_type: SystemConnectionType of the SystemConnection, this can be sink or source.
        :param direction: The SystemConnectionDirection specifies in which direction the SystemConnection
                points (horizontal or vertical). The default value is auto, but auto only works in positions where the
                direction is unique and therefore can only point in one direction.
        :return:
        """
        syscon = SystemConnection(c_type, x, y, self.layout, direction=direction)
        self.connection.append(syscon)
        return syscon

    def get_sinks(self) -> List[SystemConnection]:
        """
        Returns all SystemConnections with the SystemConnectionType.SINK.
        :return: List of sink SystemConnections
        """
        return [system_connection for system_connection in self.connection if
                system_connection.type == SystemConnectionType.SINK]

    def get_sources(self) -> List[SystemConnection]:
        """
        Returns all SystemConnections with the SystemConnectionType.SOURCE.
        :return: List of source SystemConnections
        """
        return [system_connection for system_connection in self.connection if
                system_connection.type == SystemConnectionType.SOURCE]

    def remove_system_connection(self, x: int, y: int) -> None:
        """
        Removes the SystemConnection on the given position.
        :param x: X position of the SystemConnection to be removed
        :param y: Y position of the SystemConnection to be removed.
        :return: None
        """
        for connection in self.connection:
            if connection.connected_position[1] == x and connection.connected_position[0] == y:
                del_stream = []
                for stream in self.packet_stream:
                    if connection == stream.sink or connection == stream.source:
                        del_stream.append(stream)
                for stream in del_stream:
                    self.packet_stream.remove(stream)
                self.connection.remove(connection)
                break

    def systemconnection_having_goods(self, syscon: SystemConnection) -> bool:
        for packet in self.packet_stream:
            if packet._sink == syscon or packet._source == syscon:
                return True
        return False

    def get_good_by_id(self, idx: int) -> Tuple[Goods, int]:
        """

        :param idx:
        :return:
        """
        for packstream in self.packet_stream:
            searched_good, amount = packstream.get_goods_by_id(idx)
            if searched_good:
                return searched_good, amount

    def get_all_good_amounts(self) -> int:
        """

        :return: amount of all goods in layout (packet streams)
        """
        all_amounts = 0
        for packstream in self.packet_stream:
            goods = packstream.get_goods()
            for good in goods:
                searched_good, amount = packstream.get_goods_by_id(good.id)
                all_amounts += amount
        return all_amounts

    def remove_good_by_id(self, idx: int) -> None:
        """

        :param idx:
        :return:
        """
        for packstream in self.packet_stream:
            packstream.remove_goods_by_id(idx)
            if packstream.is_empty():
                self.packet_stream.remove(packstream)

    def remove_on_position(self, x: int, y: int) -> None:
        """
        Deletes whatever is on the given position. Except for DeactivatedArea and SystemConnection
        :param x: X position where tho object should be deleted
        :param y: Y position where tho object should be deleted
        :return: None
        """
        index = self.layout[y][x]
        if index >= 0:
            self.remove_conveyor_by_id(index)
        if index == WORKPLACE:
            self.remove_workplace(x, y)
        if index == RESTRICTED_AREA:
            self.remove_restricted_area(x, y)

    def cell_is_empty(self, x: int, y: int) -> bool:
        """
        Checks if the given position/cell is free/empty.
        :param x: X position of the cell
        :param y: Y position of the cell
        :return: True if empty
        """
        return self.layout[y][x] == -1

    def cells_are_empty(self, x: int, y: int, length: int, width: int) -> bool:
        """
        Checks if the given rect of cells is free/empty.
        :param x: X position of the upper left corner
        :param y: Y position of the upper left corner
        :param length: Length of the given rect.
        :param width: Width of the given rect.
        :return: True if all cells are empty/free.
        """
        return np.all(self.layout[y:y + width, x:x + length] == -1)

    def remove_all_paths_from_packet_stream(self) -> None:
        """

        :return: None
        """
        for pac_stream in self.packet_stream:
            pac_stream.delete_path()

    def cell_is_sink_source(self, x: int, y: int) -> bool:
        for syscon in self.connection:
            if syscon.connected_position[0] == y and syscon.connected_position[1] == x:
                return True
        return False

    def reset(self):
        self.res_areas = []
        self.workplaces = []
        self.connection = []
        self.packet_stream = []
        self.conveyors = []
        self.evaluation: EvaluationResult = EvaluationResult(self.layout_id)

    @property
    def length(self) -> int:
        """
        Length [mm] of the factory.
        :return: Factory length
        """
        return self._length

    @length.setter
    def length(self, length):
        """
        Sets the length [mm] of the factory with respect to the maximum possible size.
        :param length: Width to be set
        :return:
        """
        if (self._width * length) > self.MAXSIZE:
            raise ValueError(
                OUT_OF_AREA_MSG.format(self.MAXSIZE / 1000000, (self._width * length) / 1000000))
        else:
            self._length = length

    @property
    def width(self) -> int:
        """
        Width [mm] of the factory.
        :return: Factory width
        """
        return self._width

    @width.setter
    def width(self, width):
        """
        Sets the width [mm] of the factory with respect to the maximum possible size.
        :param width: Width to be set
        :return:
        """
        if (width * self._length) > self.MAXSIZE:
            raise ValueError(
                OUT_OF_AREA_MSG.format(self.MAXSIZE / 1000000, (width * self._length) / 1000000))
        else:
            self._width = width

    def generate_new_id(self):
        self.layout_id = uuid.uuid1().int

    def remove_all_conveyors(self):
        conv_ids = [conveyor.cell_value for conveyor in self.conveyors]
        for idx in conv_ids:
            self.remove_conveyor_by_id(idx)

    def reset_evaluation(self):
        self.evaluation: EvaluationResult = EvaluationResult(self.layout_id)

    def reset_paths(self):
        for pac_stream in self.packet_stream:
            pac_stream.delete_path()

    def random_add_conveyor(self, avail_conveyors):
        not_removed = True
        while not_removed:
            x_random = random.randint(0, self.layout.shape[1] - 1)
            y_random = random.randint(0, self.layout.shape[0] - 1)

            if self.layout[y_random, x_random] > 0:
                self.remove_conveyor_by_position(x_random, y_random)
                not_removed = False

        # set conveyor to random position
        self.add_conveyor(random.choice(avail_conveyors), x_random, y_random, random.randint(0, 1))

    def random_remove_conveyor(self, avail_conveyors):
        not_removed = True
        while not_removed:
            x_random = random.randint(0, self.layout.shape[1] - 1)
            y_random = random.randint(0, self.layout.shape[0] - 1)

            if self.layout[y_random, x_random] > 0:
                self.remove_conveyor_by_position(x_random, y_random)
                not_removed = False

    def shift_conveyor_by_position(self, x_from=None, y_from=None, x_to=None, y_to=None):
        if x_from is None:
            # choose positions
            while [x_from, y_from] == [x_to, y_to] \
                    or self.layout[y_from, x_from] <= -2 \
                    or self.layout[y_to, x_to] <= -2:
                x_from = random.randint(0, self.layout.shape[1] - 1)
                y_from = random.randint(0, self.layout.shape[0] - 1)
                x_to = random.randint(0, self.layout.shape[1] - 1)
                y_to = random.randint(0, self.layout.shape[0] - 1)

        try:
            conv_1 = self.get_conveyor_by_position(x_from, y_from)

            conv_1_rotation = conv_1.rotation
            conv_1_name = conv_1.name
            conv_1_id = conv_1.cell_value  # id

            # delete conv_2
            self.remove_conveyor_by_position(x_to, y_to)
            # delete conv_1:
            self.remove_conveyor_by_position(x_from, y_from)

            # shift conv_1 to new position
            self.add_conveyor(conv_1_name, x_to, y_to, conv_1_rotation)

        except Exception as e:
            # print(e)
            pass

    def swap_conveyor_by_position(self, x_1=None, y_1=None, x_2=None, y_2=None):
        if x_1 is None:
            # choose positions
            while [x_1, y_1] == [x_2, y_2] \
                    or self.layout[y_1, x_1] <= -2 \
                    or self.layout[y_2, x_2] <= -2:
                x_1 = random.randint(0, self.layout.shape[1] - 1)
                y_1 = random.randint(0, self.layout.shape[0] - 1)
                x_2 = random.randint(0, self.layout.shape[1] - 1)
                y_2 = random.randint(0, self.layout.shape[0] - 1)

        try:
            conv_1 = self.get_conveyor_by_position(x_1, y_1)
            conv_2 = self.get_conveyor_by_position(x_2, y_2)

            # get conveyors information
            conv_1_rotation = conv_1.rotation
            conv_1_name = conv_1.name
            conv_1_id = conv_1.cell_value

            conv_2_rotation = conv_2.rotation
            conv_2_name = conv_2.name
            conv_2_id = conv_2.cell_value

            # remove conveyors
            self.remove_conveyor_by_id(conv_1_id)
            self.remove_conveyor_by_id(conv_2_id)

            # add conveyors
            self.add_conveyor(conv_2_name, x_1, y_1, conv_2_rotation)
            self.add_conveyor(conv_1_name, x_2, y_2, conv_1_rotation)

        except:
            pass
