from typing import List

from optilay.model.util.cell_values import EMPTY_CELL


class Placeable:
    """

    """

    def __init__(self, x: int, y: int, cell_value: int, layout, length: int, width: int, rotation: int = 0, load=False) -> None:
        self._position: List[int] = [y, x]
        self._size: List[int] = [width, length]
        self._rotation: int = rotation
        self._layout = layout

        self.cell_value = cell_value # TODO: replace by uuid in future
        if not load:
            self.position = [y, x]
            self.size = [width, length]
            self.rotation = rotation

    @property
    def x(self) -> int:
        """

        :return:
        """
        return self._position[1]

    @x.setter
    def x(self, value: int):
        """

        :param value:
        :return:
        """
        self.position = [self._position[0], value]

    @property
    def y(self) -> int:
        """

        :return:
        """
        return self._position[0]

    @y.setter
    def y(self, value: int):
        """

        :param value:
        :return:
        """
        self.position = [value, self._position[1]]

    @property
    def length(self):
        return self.size[1]

    @length.setter
    def length(self, value):
        self.size = [self._size[0], value]

    @property
    def width(self):
        return self.size[0]

    @width.setter
    def width(self, value):
        self.size = [value, self._size[1]]

    @property
    def rotation(self) -> int:
        return self._rotation

    @rotation.setter
    def rotation(self, rotation: int) -> None:
        self.check_rotation_restrictions(rotation)
        self._rotation = rotation

    def check_rotation_restrictions(self, rotation: int):
        # Check for right value
        if rotation not in range(4):
            raise ValueError("Rotation must be between 0 and 3!")

        # Get values of new position in layout
        if rotation == 0 or rotation == 2:
            new_pos = self._layout.layout[self.y:self.y + self._size[0], self.x:self.x + self._size[1]]
        else:
            new_pos = self._layout.layout[self.y:self.y + self._size[1], self.x:self.x + self._size[0]]

        # check for overlapping or wrong size through some kind of error
        if new_pos.size != self._size[0] * self._size[1]:
            raise ValueError("Something is wrong with the object size")

        # Check if occupied
        if not ((new_pos == self.cell_value) | (new_pos == EMPTY_CELL)).all():
            raise ValueError("Position is already occupied!")

    @property
    def position(self) -> List[int]:
        """

        :return:
        """
        return self._position

    @position.setter
    def position(self, value: List[int]):
        """

        :param value:
        :return:
        """
        self.check_position_restrictions(value)
        self._position = value

    def check_position_restrictions(self, value: List[int]):
        # Check for right values
        if len(value) != 2:
            raise ValueError("The positions list only consist of three elements [y, x].")
        if value[0] < 0 or value[1] < 0:
            raise ValueError("Positions must be positive")
        if value[1] >= self._layout.length or value[0] >= self._layout.width:
            raise ValueError("Position is outside the ConveyorSystemLayout!")

        # Get values of new position in layout
        new_pos = self._layout.layout[value[0]:value[0] + self.width, value[1]:value[1] + self.length]

        # check for overlapping or wrong size through some kind of error
        if new_pos.size != self._size[0] * self._size[1]:
            raise ValueError("Something is wrong with the object size")

        # Check if occupied
        if not ((new_pos == self.cell_value) | (new_pos == EMPTY_CELL)).all():
            raise ValueError("Position is already occupied!")

    @property
    def size(self):
        if self._rotation == 0 or self._rotation == 2:
            return self._size
        else:
            return [self._size[1], self._size[0]]

    @size.setter
    def size(self, value: List[int]):
        self.check_size_restrictions(value)
        self._size = value

    def check_size_restrictions(self, value: List[int]):
        # Check for right values
        if len(value) != 2:
            raise ValueError("The size list only consist of three elements [width, length].")
        if value[0] < 0 or value[1] < 0:
            raise ValueError("Sizes must be positive")
        if not value[0] < self._layout.width or not value[1] < self._layout.length:
            raise ValueError("Size is bigger than the ConveyorSystemLayout!")

        # Get values of new position in layout
        new_pos = self._layout.layout[self.y:self.y + value[0], self.x:self.x + value[1]]

        # check for overlapping or wrong size through some kind of error
        if new_pos.size != self._size[0] * self._size[1]:
            raise ValueError("Something is wrong with the object size")

        # Check if occupied
        if not ((new_pos == self.cell_value) | (new_pos == EMPTY_CELL)).all():
            raise ValueError("Position is already occupied!")
