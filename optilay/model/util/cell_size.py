from enum import Enum


class CellSize(Enum):
    """

    """
    CELL_400 = 400
    CELL_600 = 600
    CELL_800 = 800
    CELL_1000 = 1000
    CELL_1200 = 1200
