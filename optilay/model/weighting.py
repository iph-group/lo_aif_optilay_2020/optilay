SIZE_MSG = "Each Weighting attribute must be 0 <= attribute <= 1"


class Weighting:
    """
    Weighting contains all the attributes that are evaluated and the weight/weighting of each attribute.
    """

    def __init__(self) -> None:
        """

        """
        self._throughput: float = 0.2
        self._lead_time: float = 0.2
        self._space_requirement: float = 0.2
        self._buffer_capacity: float = 0.2
        self._costs: float = 0.2

        # If changes are made to the weighted values, then these must also be adjusted
        # in evaluation_result.py, spider_chart.py and result_table_dialog.py.

    def equalise(self) -> None:
        """
        Distributes the weighting evenly across all attributes.
        :return: None
        """
        self._throughput = 0.2
        self._lead_time = 0.2
        self._space_requirement = 0.2
        self._buffer_capacity = 0.2
        self._costs = 0.2

    def valid_weighting(self) -> bool:
        """

        :return:
        """
        return self._costs + self._lead_time + self._throughput + self._buffer_capacity + self._space_requirement == 1

    def weighting_sum(self) -> float:
        """

        :return:
        """
        return self._costs + self._lead_time + self._throughput + self._buffer_capacity + self._space_requirement

    @property
    def throughput(self) -> float:
        """

        :return:
        """
        return self._throughput

    @throughput.setter
    def throughput(self, value: float):
        """

        :param value:
        :return:
        """
        if 0 <= value <= 1:
            self._throughput = value
        else:
            raise ValueError(SIZE_MSG)

    @property
    def lead_time(self) -> float:
        """

        :return:
        """
        return self._lead_time

    @lead_time.setter
    def lead_time(self, value: float):
        """

        :param value:
        :return:
        """
        if 0 <= value <= 1:
            self._lead_time = value
        else:
            raise ValueError(SIZE_MSG)

    @property
    def space_requirement(self) -> float:
        """

        :return:
        """
        return self._space_requirement

    @space_requirement.setter
    def space_requirement(self, value: float):
        """

        :param value:
        :return:
        """
        if 0 <= value <= 1:
            self._space_requirement = value
        else:
            raise ValueError(SIZE_MSG)

    @property
    def buffer_capacity(self) -> float:
        """

        :return:
        """
        return self._buffer_capacity

    @buffer_capacity.setter
    def buffer_capacity(self, value: float):
        """

        :param value:
        :return:
        """
        if 0 <= value <= 1:
            self._buffer_capacity = value
        else:
            raise ValueError(SIZE_MSG)

    @property
    def costs(self) -> float:
        """

        :return:
        """
        return self._costs

    @costs.setter
    def costs(self, value: float):
        """

        :param value:
        :return:
        """
        if 0 <= value <= 1:
            self._costs = value
        else:
            raise ValueError(SIZE_MSG)
