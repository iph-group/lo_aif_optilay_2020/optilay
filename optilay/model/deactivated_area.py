from optilay.model.util.cell_values import RESTRICTED_AREA, DEACTIVATED_AREA
from optilay.model.util.placeable import Placeable


class DeactivatedArea(Placeable):
    """

    """

    def __init__(self, *args, **kwargs) -> None:
        kwargs['cell_value'] = DEACTIVATED_AREA
        kwargs['length'] = 1
        kwargs['width'] = 1
        kwargs['rotation'] = 0

        super().__init__(*args, **kwargs)
