from typing import List

from optilay.model.goods.goods import Goods
from optilay.model.goods.goods_type import GoodsType
from optilay.model.util.cell_size import CellSize


class GoodsFactory:
    def __init__(self, cell_size: CellSize, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._cell_size = cell_size
        self.goods = {
            'Parcel_280x180x170': {'width': 180, 'length': 280, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 2000},
            'DHL_150x110x10': {'width': 110, 'length': 150, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                               'max_weight': 2000},
            'DHL_350x250x100': {'width': 250, 'length': 350, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                'max_weight': 2000},
            'DHL_600x300x150': {'width': 300, 'length': 600, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                'max_weight': 5000},
            'DHL_1200x600x600': {'width': 600, 'length': 1200, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                 'max_weight': 35000},
            'VDA-SLC_300x200x147': {'width': 200, 'length': 300, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_400x300x147': {'width': 300, 'length': 400, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_400x300x213': {'width': 300, 'length': 400, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_400x300x280': {'width': 300, 'length': 400, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_600x400x147': {'width': 400, 'length': 600, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_600x400x213': {'width': 400, 'length': 600, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_600x400x280': {'width': 400, 'length': 600, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-SLC_800x600x212': {'width': 600, 'length': 800, 'g_type': GoodsType.SLC, 'empty_weight': 300,
                                    'max_weight': 15000},
            'VDA-M1_600x400x150': {'width': 400, 'length': 600, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 15000},
            'VDA-M2_600x400x230': {'width': 400, 'length': 600, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 15000},
            'VDA-M3_600x400x280': {'width': 400, 'length': 600, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 15000},
            'VDA-M4_400x300x150': {'width': 300, 'length': 400, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 15000},
            'VDA-M5_400x300x280': {'width': 300, 'length': 400, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 15000},
            'VDA-M6_300x200x150': {'width': 200, 'length': 300, 'g_type': GoodsType.PARCELLED, 'empty_weight': 300,
                                   'max_weight': 15000},
            'E1_600x400x125': {'width': 400, 'length': 600, 'g_type': GoodsType.SLC, 'empty_weight': 1500,
                               'max_weight': 15000},
            'E2_600x400x200': {'width': 400, 'length': 600, 'g_type': GoodsType.SLC, 'empty_weight': 2000,
                               'max_weight': 15000},
            'E3_600x400x300': {'width': 400, 'length': 600, 'g_type': GoodsType.SLC, 'empty_weight': 3000,
                               'max_weight': 15000}
        }

    def create_goods(self, name: str, load: float = 0) -> Goods:
        if load < 0:
            raise ValueError("The weight of the loading for the good must be greater equals zero.")
        good = self.goods[name]
        if good['width'] <= self._cell_size.value and good['length'] <= self._cell_size.value:
            ret_good = Goods(width=good['width'], length=good['length'], name=name, empty_weight=good['empty_weight'],
                             max_weight=good['max_weight'], g_type=good['g_type'], load_weight=load)
            return ret_good
        else:
            raise ValueError("This conveyor is not available with the actual cell_size!")

    def get_available_goods(self) -> List[str]:
        names = []
        for name in self.goods:
            if self.goods[name]['width'] <= self._cell_size.value and self.goods[name][
                'length'] <= self._cell_size.value:
                names.append(name)
        return names

    def get_all_goods(self):
        return list(self.goods.keys())

    def get_goods_information(self, name: str):
        return self.goods[name]

    def add_good(self, name: str, width: int, length: int, g_type: GoodsType, empty_weight: int, max_weight: int):
        self.goods[name] = {'width': width, 'length': length, 'g_type': g_type, 'empty_weight': empty_weight,
                            'max_weight': max_weight}

    def remove_good(self, name: str):
        del self.goods[name]
