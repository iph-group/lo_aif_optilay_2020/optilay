import uuid
from typing import List

from optilay.model.goods.goods_type import GoodsType


class Goods:
    """

    """

    def __init__(self, width: int, length: int, name: str, g_type: GoodsType, empty_weight: float, max_weight: float,
                 load_weight: float = 0) -> None:
        super().__init__()
        if load_weight + empty_weight > max_weight:
            raise ValueError("The load of a good must be smaller than the nax allow weight")
        if load_weight < 0:
            raise ValueError("The load of an good must be greater equal zero.")
        self.id: int = uuid.uuid4().int
        self.name: str = name
        self.type: GoodsType = g_type
        self._empty_weight = empty_weight
        self._load_weight = load_weight
        self._max_weight = max_weight
        self._size: List[int] = [width, length]

    @property
    def actual_weight(self) -> float:
        return self._empty_weight + self._load_weight

    def __repr__(self):
        return str(self.__dict__)


