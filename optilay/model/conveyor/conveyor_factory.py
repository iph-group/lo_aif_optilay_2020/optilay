import math
import uuid
from typing import List, Union

from optilay.model.conveyor.conveyor import Conveyor
from optilay.model.conveyor.conveyor_direction import ConveyorDirection
from optilay.model.conveyor.conveyor_type import ConveyorType


class ConveyorFactory:
    def __init__(self, lay, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.layout = lay
        # Attention: The cost rates do not correspond to any specific manufacturer's data.
        # Attention: Static velocities were assumed. Curve speeds were not differentiated.
        self.conveyors = {
            'belt_conveyor_400': {'width': 400, 'length': 400, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                  'max_weight': 30, 'component_costs': 400, 'conv_direction': ConveyorDirection.ForwardBackward},
            'belt_conveyor_600': {'width': 600, 'length': 600, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                  'max_weight': 30, 'component_costs': 600, 'conv_direction': ConveyorDirection.ForwardBackward},
            'belt_conveyor_800': {'width': 800, 'length': 800, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                  'max_weight': 30, 'component_costs': 800, 'conv_direction': ConveyorDirection.ForwardBackward},
            'belt_conveyor_1000': {'width': 1000, 'length': 1000, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                   'max_weight': 30, 'component_costs': 1000, 'conv_direction': ConveyorDirection.ForwardBackward},
            'roller_conveyor_400': {'width': 400, 'length': 400, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                    'max_weight': 50, 'component_costs': 400, 'conv_direction': ConveyorDirection.ForwardBackward},
            'roller_conveyor_600': {'width': 600, 'length': 600, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                    'max_weight': 50, 'component_costs': 600, 'conv_direction': ConveyorDirection.ForwardBackward},
            'roller_conveyor_800': {'width': 800, 'length': 800, 'conv_type': ConveyorType.Conventional, 'velocity': 1000,
                                    'max_weight': 50, 'component_costs': 800, 'conv_direction': ConveyorDirection.ForwardBackward},
            'roller_conveyor_1000': {'width': 1000, 'length': 1000, 'conv_type': ConveyorType.Conventional,
                                     'velocity': 1, 'max_weight': 50, 'component_costs': 1000,
                                     'conv_direction': ConveyorDirection.ForwardBackward},
            'Celluveyor_400': {'width': 400, 'length': 400, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                               'max_weight': 30, 'component_costs': 800, 'conv_direction': ConveyorDirection.Omnidirectional},
            'Celluveyor_600': {'width': 600, 'length': 600, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                               'max_weight': 30, 'component_costs': 1200, 'conv_direction': ConveyorDirection.Omnidirectional},
            'FlexConveyor_400': {'width': 400, 'length': 400, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                 'max_weight': 50, 'component_costs': 600, 'conv_direction': ConveyorDirection.FourWayIntersection},
            'FlexConveyor_600': {'width': 600, 'length': 600, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                 'max_weight': 50, 'component_costs': 900, 'conv_direction': ConveyorDirection.FourWayIntersection},
            'FlexConveyor_800': {'width': 800, 'length': 800, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                 'max_weight': 50, 'component_costs': 1200, 'conv_direction': ConveyorDirection.FourWayIntersection},
            'FlexConveyor_1000': {'width': 1000, 'length': 1000, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                  'max_weight': 50, 'component_costs': 1500, 'conv_direction': ConveyorDirection.FourWayIntersection},
            'Interoll_RM_8731_400': {'width': 400, 'length': 400, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                     'max_weight': 50, 'component_costs': 600, 'conv_direction': ConveyorDirection.FourWayIntersection},
            'Interoll_RM_8731_600': {'width': 600, 'length': 600, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                     'max_weight': 50, 'component_costs': 900, 'conv_direction': ConveyorDirection.FourWayIntersection},
            'Interoll_RM_8731_800': {'width': 800, 'length': 800, 'conv_type': ConveyorType.Modular, 'velocity': 1000,
                                     'max_weight': 50, 'component_costs': 1200,'conv_direction': ConveyorDirection.FourWayIntersection},

        }

    def create_conveyor(self, name: str, x: int, y: int, rotation: int, conv_layout) -> Conveyor:
        conv = self.conveyors[name]
        if conv['width'] == self.layout._cell_size.value:
            ret_conv = Conveyor(name=name, conv_type=conv['conv_type'], trans_direction=conv['conv_direction'],
                                velocity=conv['velocity'], max_weight=conv['max_weight'],
                                component_costs=conv['component_costs'], x=x, y=y, rotation=rotation,
                                layout=conv_layout, width=1,
                                length=math.ceil(conv['length'] / self.layout._cell_size.value),
                                cell_value=uuid.uuid4().int >> 97)
            return ret_conv
        else:
            raise ValueError("This conveyor is not available with the actual cell_size!")

    def get_available_conveyor(self) -> List[str]:
        names = []
        for name in self.conveyors:
            if self.conveyors[name]['width'] == self.layout._cell_size.value:
                names.append(name)
        return names

    def get_all_conveyor(self):
        return list(self.conveyors.keys())

    def get_conveyor_information(self, name: str):
        return self.conveyors[name]
