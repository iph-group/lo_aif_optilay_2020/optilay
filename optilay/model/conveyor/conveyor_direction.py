from enum import Enum


class ConveyorDirection(Enum):
    """

    """
    ForwardBackward = 0
    FourWayIntersection = 1
    Omnidirectional = 2
