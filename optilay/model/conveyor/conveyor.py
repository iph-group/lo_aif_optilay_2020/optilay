from typing import List

from optilay.model.conveyor.conveyor_direction import ConveyorDirection
from optilay.model.conveyor.conveyor_type import ConveyorType
from optilay.model.util.placeable import Placeable


class Conveyor(Placeable):
    def __init__(self, name: str, conv_type: ConveyorType, trans_direction: ConveyorDirection, velocity: float,
                 max_weight: int, component_costs: int,
                 *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.name: str = name
        self.type: ConveyorType = conv_type
        self.trans_direction: ConveyorDirection = trans_direction
        self.velocity: float = velocity
        self.max_weight: float = max_weight
        self.component_costs: int = component_costs

    @Placeable.position.setter
    def position(self, value: List[int]) -> None:
        self.check_position_restrictions(value)
        # Implement here

        ################
        self._position = value

    @Placeable.rotation.setter
    def rotation(self, rotation: int) -> None:
        self.check_rotation_restrictions(rotation)
        # Implement here

        ################
        self._rotation = rotation

    @Placeable.size.setter
    def size(self, value: List[int]) -> None:
        self.check_size_restrictions(value)
        # Implement here

        ################
        self._size = value

    def tool_tip(self):
        # +1 so that the coordiants for the user start at 1
        return "ID: {}\nName: {}\nPosition: ({}, {})\nSize: {}x{}\nType: {}\nTransport direction: {}\nVelocity: {}\nMaximum transport weight: {}".format(
            self.cell_value, self.name, self.x + 1, self.y + 1, self.width, self.length, self.type.name,
            self.trans_direction.name, self.velocity, self.max_weight, self.component_costs)

    def __repr__(self) -> str:
        return "ID: {}\nName: {}\nPosition: ({}, {})\nSize: {}x{}\nType: {}\nTransport direction: {}\nVelocity: {}\nMaximum transport weight: {}".format(
            self.cell_value, self.name, self.x, self.y, self.width, self.length, self.type.name,
            self.trans_direction.name, self.velocity, self.max_weight, self.component_costs)
