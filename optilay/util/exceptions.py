class NoPathException(Exception):
    """
    An Exception thrown when no path is specified in the IOManager.
    """
    pass


class NoAutoDirection(Exception):
    """
    An Exception thrown when no path is specified in the IOManager.
    """
    pass
