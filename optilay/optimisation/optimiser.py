#!/usr/bin/env python3
import logging
import multiprocessing
import sys
import time

from optilay.evaluation.evaluator import Evaluator
from optilay.gui.util.write_stream import WriteStream
from optilay.model.layout_model import LayoutModel
from optilay.optimisation.optimisation_method import OptimisationMethod
from optilay.optimisation.genetic_algorithm import GeneticAlgorithm
from optilay.optimisation.random_placement import RandomPlacement


def optimiser(layout: LayoutModel, evaluator: Evaluator, result_queue: multiprocessing.Queue,
              output_stream: WriteStream = None, error_queue: multiprocessing.Queue = None) -> None:
    """
    Since a process can only be created with a function and not with a method, the optimiser is a function and not a
    class. This function is used to decide which optimisation method is to be started and, if necessary, to carry out
    preprocessing.
    :param layout: The LayoutModel which is optimised.
    :param evaluator: The Evaluator that can be used in the optimisation.
    :param result_queue: The LayoutDB with the results of the optimisation of the LayoutModel are written here.
    :param output_stream: If available, all outputs will be written in.
    :param error_queue: If available, caught exceptions are written in here.
    :return:
    """
    try:
        if output_stream:
            sys.stdout = output_stream

        layout.get_db().clear()

        if layout.get_parameter().opt_method == OptimisationMethod.METHOD1:
            # MethodA(layout, evaluator).start_optimisation() # How to start a method. This one raises an not
            # implemented exception. For loop was only implemented for testing purposes
            for i in range(100):
                time.sleep(0.1)
                print("+1", end='')  # One opitimise process needs to call this 100 times for the progress bar.
                print(i)
        elif layout.get_parameter().opt_method == OptimisationMethod.GeneticAlgorithm:
            GeneticAlgorithm(layout, evaluator).start_optimisation()

        elif layout.get_parameter().opt_method == OptimisationMethod.RandomPlacement:
            RandomPlacement(layout, evaluator).start_optimisation()
        else:
            raise ValueError('Unknown OptimisationMethod')

        result_queue.put(layout.get_db())

    except Exception as e:
        if error_queue:
            error_queue.put(str(e))
        logging.exception("Multiprocessing exception")
    finally:
        print("Done!", end="")
