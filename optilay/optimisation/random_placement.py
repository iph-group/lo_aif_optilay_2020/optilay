import copy
import random
import datetime

from optilay.evaluation.evaluator import Evaluator
from optilay.model.layout_model import LayoutModel
from optilay.evaluation.util_graph.graph_generation import LayoutGraph
from optilay.optimisation.util.layout_initialisation import random_init_layout
from optilay.optimisation.util.layout_initialisation import all_modular_init_layout
from optilay.optimisation.util.layout_initialisation import a_star_init_layout


class RandomPlacement:
    """
    This is an example optimisation class.
    The optimiser initialises the class with the LayoutModel, Evaluator and then calls the start_optimisation method.
    Otherwise, the class can be freely modified.
    """

    def __init__(self, layout_model: LayoutModel, evaluator: Evaluator) -> None:
        """

        :param layout: LayoutModel that is to be optimised.
        :param evaluator: Evaluator that is available for evaluation.
        """
        self.layout_model = layout_model
        self.evaluator = evaluator

    def start_optimisation(self) -> None:
        """
        The optimisation with this method is carried out here.
        :return: None
        """

        layout = self.layout_model.get_current_layout()

        # init layout
        if self.layout_model.parameter.initialisation_method.value == 0:
            a_star_init_layout(layout, self.layout_model.parameter.conv_types)
        elif self.layout_model.parameter.initialisation_method.value == 1:
            random_init_layout(layout, self.layout_model.parameter.conv_types)
        elif self.layout_model.parameter.initialisation_method.value == 2:
            all_modular_init_layout(layout, self.layout_model.parameter.conv_types)
        else:
            raise Exception("Unvalid inilisation method")

        # eval init layout
        self.evaluator.evaluate(self.layout_model)

        self.layout_model.layouts.add_layout(self.layout_model.get_current_layout(), overwrite=True)
        old_weight_sum = copy.copy(layout.evaluation.weighted_sum)
        old_layout_id = copy.copy(layout.layout_id)

        layout = self.layout_model.new_layout(True, False, True, False)
        print_counter = 1

        for i in range(self.layout_model.parameter.number_of_iterations):

            if i + 1 >= (self.layout_model.parameter.number_of_iterations / 100) * print_counter:
                print("+1", end='')
                print_counter += 1

            if random.random() < 0.9:  # todo: parameter
                # remove conveyor if on position
                method = random.random()
                if method < self.layout_model.parameter.neighborhood_search['random_positioning']:
                    # random position
                    layout.random_add_conveyor(self.layout_model.parameter.conv_types)

                elif method < (self.layout_model.parameter.neighborhood_search['random_positioning'] +
                               self.layout_model.parameter.neighborhood_search['shift']):
                    # shift
                    layout.shift_conveyor_by_position()

                else:
                    # swap
                    layout.swap_conveyor_by_position()
            else:
                # remove conveyor if on position and with probability of 30 %
                layout.random_remove_conveyor(self.layout_model.parameter.conv_types)

            self.evaluator.evaluate(self.layout_model)
            self.evaluator.result_updater(self.layout_model, False, old_layout_id)
            old_weight_sum = self.layout_model.layouts.get_layout(old_layout_id).evaluation.weighted_sum

            if layout.evaluation.weighted_sum > old_weight_sum:
                old_layout_id = copy.copy(layout.layout_id)
                old_weight_sum = layout.evaluation.weighted_sum
                self.layout_model.layouts.add_layout(self.layout_model.get_current_layout(), overwrite=True)
                layout = self.layout_model.new_layout(True, False, True, False)
            else:
                # self.layout_model.layouts.remove_layout_by_id(layout.layout_id)
                self.layout_model.load_layout(old_layout_id)
                layout = self.layout_model.new_layout(True, False, True, False)

        # update eval all and update all
        best_layouts_idx = self.layout_model.get_best_layouts_idx(len(self.layout_model.layouts._saved_layouts))
        for idx in best_layouts_idx:
            self.layout_model.load_layout(idx)
            self.evaluator.evaluate(self.layout_model, eval_all=True)
        else:
            self.evaluator.result_updater_layoutdb(self.layout_model, update_all=True)

        # set best layout as current
        best_layout = self.layout_model.get_best_layouts()
        self.layout_model.load_layout(best_layout[0].layout_id)
        layout = self.layout_model.get_current_layout()

        """
        graph = LayoutGraph(layout)
        graph.get_paths(layout.packet_stream, write_to_packet_streams=True)
        graph.get_intersections(layout.packet_stream)
        graph.plot_graph()
        """

        # print("Done!")
