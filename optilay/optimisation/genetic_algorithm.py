import copy
import os
import random

from optilay.evaluation.evaluator import Evaluator
from optilay.model.layout_model import LayoutModel
from optilay.model.layout_db import LayoutDB
from optilay.optimisation.util.layout_initialisation import random_init_layout
from optilay.optimisation.util.layout_initialisation import all_modular_init_layout
from optilay.optimisation.util.layout_initialisation import a_star_init_layout
from optilay.optimisation.util.chromosome import Chromosome
from optilay.evaluation.util_graph.graph_generation import LayoutGraph


class GeneticAlgorithm:
    """
    This is an example optimisation class.
    The optimiser initialises the class with the LayoutModel, Evaluator and then calls the start_optimisation method.
    Otherwise, the class can be freely modified.
    """

    def __init__(self, layout_model: LayoutModel, evaluator: Evaluator) -> None:
        """

        :param layout: LayoutModel that is to be optimised.
        :param evaluator: Evaluator that is available for evaluation.
        """
        self.layout_model = layout_model
        self.evaluator = evaluator

    def start_optimisation(self) -> None:
        """
        The optimisation with this method is carried out here.
        :return: None
        """

        print("[{}]: Start genetic algorithm.".format(os.getpid()))

        # init objects
        chromosome_list = Chromosome()

        # init first population
        for i in range(self.layout_model.parameter.population_size):
            if i == 0:
                # a star init
                a_star_init_layout(self.layout_model.get_current_layout(), self.layout_model.parameter.conv_types)
            elif i < round(self.layout_model.parameter.population_size / 3):
                # all modular init
                all_modular_init_layout(self.layout_model.get_current_layout(), self.layout_model.parameter.conv_types)
            else:
                # random init
                random_init_layout(self.layout_model.get_current_layout(), self.layout_model.parameter.conv_types)
            self.evaluator.evaluate(self.layout_model)  # eval layout
            self.layout_model.layouts.add_layout(self.layout_model.get_current_layout(), overwrite=True)  # save in layoutDB
            chromosome_list.decode_chromosome(self.layout_model.get_current_layout(), self.layout_model.parameter.conv_types)  # decode layout
            if i < self.layout_model.parameter.population_size - 1:
                self.layout_model.new_layout(False, True, True, False)  # create new layout -> current_layout

        print("[{}]: GA: Initialise population was successful".format(os.getpid()))
        print_counter = 1

        for generation in range(self.layout_model.parameter.number_of_generations):

            """
            if generation / self.layout_model.parameter.number_of_generations in [0.1, 0.25, 0.5, 0.75]:
                print(str(generation) + " out of " + str(self.layout_model.parameter.number_of_generations)
                      + " generations have been passed.")
            """

            if generation + 1 >= (self.layout_model.parameter.number_of_generations / 100) * print_counter:
                print("+1", end='')
                print_counter += 1
            #"""

            # change control parameter to enable deep_search instead of wide_search
            if generation == round(self.layout_model.parameter.number_of_generations / 2):
                self.layout_model.parameter.mutation_rate = 0.9
                self.layout_model.parameter.maximal_number_of_mutations = 1
                self.layout_model.parameter.crossover_two_point_operator_length = 1

            # save parent_chromosome
            parent_chromosomes = copy.deepcopy(chromosome_list.saved_chromosomes)

            # crossover
            chromosome_keys_list = list(chromosome_list.saved_chromosomes.keys())
            random.shuffle(chromosome_keys_list)
            while chromosome_keys_list:
                chromosome_list.crossover(chromosome_keys_list[0], chromosome_keys_list[1],
                                          self.layout_model.parameter.crossover_two_point_operator_length)
                chromosome_keys_list.pop(0)
                chromosome_keys_list.pop(0)

            # mutation
            for chromosome in chromosome_list.saved_chromosomes:
                if random.random() < self.layout_model.parameter.mutation_rate:
                    chromosome_list.mutation(chromosome, self.layout_model.parameter.conv_types,
                                             self.layout_model.parameter.maximal_number_of_mutations)

            # generate child_layouts from chromosome
            chromosome_keys_list = list(chromosome_list.saved_chromosomes.keys())
            for chromosome in chromosome_keys_list:
                layout = self.layout_model.new_layout(False, True, True, False)  # create new layout
                chromosome_list.encode_chromosome(layout, chromosome, self.layout_model.parameter.conv_types)  # encode layout from chromosome
                self.evaluator.evaluate(self.layout_model)  # eval layout
                chromosome_list.saved_chromosomes[layout.layout_id] = chromosome_list.saved_chromosomes.pop(chromosome)
                self.layout_model.layouts.add_layout(self.layout_model.get_current_layout(), overwrite=True)

            # selection
            chromosome_list.saved_chromosomes.update(parent_chromosomes)
            self.evaluator.result_updater_layoutdb(self.layout_model, update_all=False)

            """
            # best fit selection
            best_layouts_idx = self.layout_model.get_best_layouts_idx(self.layout_model.parameter.population_size)
            all_layouts_idx = self.layout_model.get_all_layouts_idx()
            for layout_idx in all_layouts_idx:
                if layout_idx not in best_layouts_idx:
                    self.layout_model.layouts.remove_layout_by_id(layout_idx)
                    chromosome_list.remove_chromosome_by_id(layout_idx)
            """

            # stochastic universal sampling
            # https://en.wikipedia.org/wiki/Stochastic_universal_sampling
            # pre-calculation for selection
            best_layouts_idx = self.layout_model.get_best_layouts_idx_weight_sum(self.layout_model.parameter.population_size * 2)
            total_fitness = 0
            for entry in best_layouts_idx:
                total_fitness += entry[1]
            distance_between_pointers = total_fitness / self.layout_model.parameter.population_size
            start = random.uniform(0, distance_between_pointers)
            pointers = []
            for entry in range(self.layout_model.parameter.population_size):
                pointers.append(start + (entry * distance_between_pointers))

            # selection of layouts
            keep = []
            for entry in pointers:
                i = 0
                fitness_sum = best_layouts_idx[i][1]
                while fitness_sum < entry:
                    i += 1
                    fitness_sum += best_layouts_idx[i][1]
                keep.append(best_layouts_idx[i][0])

            # update layoutDB and chromosome
            all_layouts_idx = self.layout_model.get_all_layouts_idx()
            for layout_idx in all_layouts_idx:
                if layout_idx not in keep:
                    self.layout_model.layouts.remove_layout_by_id(layout_idx)
                    chromosome_list.remove_chromosome_by_id(layout_idx)

            if len(chromosome_list.saved_chromosomes) < self.layout_model.parameter.population_size:
                all_layouts_idx = self.layout_model.get_all_layouts_idx()
                for entry in all_layouts_idx:
                    if entry in keep:
                        keep.pop(keep.index(entry))

                for entry in keep:
                    self.layout_model.load_layout(entry)
                    self.layout_model.new_layout(True, False, True, True)
                    chromosome_list.decode_chromosome(self.layout_model.get_current_layout(),
                                                      self.layout_model.parameter.conv_types)
            #"""

        # print("Genetic algorithm finished. Now analysing all results.")

        # update eval all and update all
        best_layouts_idx = self.layout_model.get_best_layouts_idx(self.layout_model.parameter.population_size)
        for idx in best_layouts_idx:
            self.layout_model.load_layout(idx)
            self.evaluator.evaluate(self.layout_model, eval_all=True)
        else:
            self.evaluator.result_updater_layoutdb(self.layout_model, update_all=True)

        # set best layout as current layout
        best_layouts_idx = self.layout_model.get_best_layouts_idx()
        self.layout_model.load_layout(best_layouts_idx[0])
        layout = self.layout_model.get_current_layout()

        """
        graph = LayoutGraph(layout)
        graph.get_paths(layout.packet_stream, write_to_packet_streams=True)
        graph.get_intersections(layout.packet_stream)
        breakpoint()
        graph.plot_graph()
        breakpoint()
        """
        #"""

        # print("Done!")
