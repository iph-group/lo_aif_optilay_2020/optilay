from enum import Enum


class OptimisationMethod(Enum):
    """
    This Enum class is used to specify in ControlParameter which optimisation method is to be used in the optimisation.
    """
    METHOD1 = 0
    GeneticAlgorithm = 1
    RandomPlacement = 2
    # ...
