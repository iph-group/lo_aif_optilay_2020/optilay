import random

import numpy as np

from typing import Dict

from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.model.util.cell_values import DEACTIVATED_AREA, WORKPLACE, RESTRICTED_AREA

class Chromosome:

    def __init__(self):
        # self.chromosome_id = None
        self.saved_chromosomes: Dict[int, list] = {}

    def get_chromosome_by_id(self, idx) -> list:
        return self.saved_chromosomes[idx]

    def remove_chromosome_by_id(self, idx) -> None:
        if idx in self.saved_chromosomes:
            self.saved_chromosomes.pop(idx)

    def decode_chromosome(self, layout: ConveyorSystemLayout, avail_conv) -> list:
        chromosome = []
        for y in range(layout.layout.shape[0]):
            for x in range(layout.layout.shape[1]):
                # get conveyor of position
                try:
                    conv = layout.get_conveyor_by_id(layout.layout[y, x])
                    chromosome.append((avail_conv.index(conv.name), conv.rotation))
                except:
                    chromosome.append((layout.layout[y, x], 0))

        self.saved_chromosomes[layout.layout_id] = chromosome

    def encode_chromosome(self, layout: ConveyorSystemLayout, chromosome_idx, avail_conv) -> None:
        """
        :param layout: plain layout
        :param chromosome_idx: index of chromosome which will be transferred to layout
        :param avail_conv: list of conveyors that could be used in experiment
        """
        chromosome = self.get_chromosome_by_id(chromosome_idx)

        x_max = layout.layout.shape[1] - 1
        y_max = layout.layout.shape[0] - 1
        y, x = 0, 0
        for gen in chromosome:
            if gen[0] > -1:  # check if gen represents conveyor
                try:
                    layout.add_conveyor(avail_conv[gen[0]], x, y, gen[1])
                except:
                    breakpoint()
            """
            # should ne prevented in decode!
            elif gen[0] == -30:
                layout.add_workplace(x, y)
            elif gen[0] == -50:
                layout.add_restricted_area(x, y)
            elif gen[0] == -99:
                layout.add_deactivated_area(x, y)
            """

            x += 1
            if x == x_max + 1:
                y += 1
                x = 0

    def crossover(self, chromosome_1, chromosome_2, two_point_operator_length) -> None:

        chromosome_1 = self.get_chromosome_by_id(chromosome_1)
        chromosome_2 = self.get_chromosome_by_id(chromosome_2)

        random_pos = random.randint(0, len(chromosome_1))
        random_length = random.randint(0, two_point_operator_length)
        while random_pos + random_length > len(chromosome_1) - 1:
            random_length -= 1

        # get gene segment
        for idx in range(random_pos, random_pos + random_length + 1, 1):
            gene_1 = chromosome_1[idx]
            gene_2 = chromosome_2[idx]
            if gene_1[0] in [WORKPLACE, RESTRICTED_AREA, DEACTIVATED_AREA] or \
                    gene_2[0] in [WORKPLACE, RESTRICTED_AREA, DEACTIVATED_AREA]:
                continue
            chromosome_1[idx] = gene_2
            chromosome_2[idx] = gene_1

    def mutation(self, chromosome, available_conveyors, maximum_of_mutations):

        chromosome = self.get_chromosome_by_id(chromosome)

        """
        # would be nicer, but could cause shift of position regarding [-30, -50, -99]! 
        finished = False
        while not finished:
            idx = random.randint(0, len(chromosome) - 1)
            if chromosome[idx][0] not in [-30, -50, -99]:
                finished = True
        gene_1 = chromosome.pop(idx)

        finished = False
        while not finished:
            idx = random.randint(0, len(chromosome) - 1)
            if chromosome[idx][0] not in [-30, -50, -99]:
                finished = True
        chromosome.insert(idx, gene_1)

        """
        for mutation in range(random.randint(1, maximum_of_mutations)):
            finished = False
            while not finished:
                idx = random.randint(0, len(chromosome) - 1)
                if chromosome[idx][0] not in [WORKPLACE, RESTRICTED_AREA, DEACTIVATED_AREA]:
                    finished = True

            if random.random() < 0.7:
                chromosome[idx] = (random.randint(0, len(available_conveyors) - 1), random.randint(0, 1))
            else:
                chromosome[idx] = (-1, 0)
