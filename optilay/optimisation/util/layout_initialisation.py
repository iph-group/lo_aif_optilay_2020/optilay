import random

from optilay.model.conveyor_system_layout import ConveyorSystemLayout
from optilay.optimisation.util.astar_algorithm import a_star_algorithm


def random_init_layout(layout: ConveyorSystemLayout, available_conveyors):

    x_max = layout.layout.shape[1]
    y_max = layout.layout.shape[0]

    for y in range(y_max):
        for x in range(x_max):
            if random.random() < 0.9:
                try:
                    layout.add_conveyor(random.choice(available_conveyors), x, y, random.randint(0, 1))
                except:
                    pass


def all_modular_init_layout(layout: ConveyorSystemLayout, available_conveyors):

    x_max = layout.layout.shape[1] - 1
    y_max = layout.layout.shape[0] - 1

    modular_conveyors = []
    for conv_name in available_conveyors:
        conv = layout.get_conveyor_information(conv_name)
        if conv.get('conv_type').value == 1:
            modular_conveyors.append(conv_name)

    if not modular_conveyors:
        raise Exception("Failure in initialisation: No modular conveyors were selected for optimisation.")

    for y in range(y_max + 1):
        for x in range(x_max + 1):
            try:
                layout.add_conveyor(random.choice(modular_conveyors), x, y, random.randint(0, 1))
            except:
                pass


def a_star_init_layout(layout: ConveyorSystemLayout, available_conveyors):

    path_list = []
    for pac_stream in layout.packet_stream:
        path_list += a_star_algorithm(layout,
                                      pac_stream.source.connected_position,
                                      pac_stream.sink.connected_position)

    new_list = []
    for elem in path_list:
        if elem not in new_list:
            new_list.append(elem)
    path_list = new_list

    modular_conveyors = []
    for conv_name in available_conveyors:
        conv = layout.get_conveyor_information(conv_name)
        if conv.get('conv_type').value == 1:
            modular_conveyors.append(conv_name)

    if not modular_conveyors:
        raise Exception("Failure in initialisation: No modular conveyors were selected for optimisation.")

    for element in path_list:
        try:
            layout.add_conveyor(random.choice(modular_conveyors), element[1], element[0], random.randint(0, 1))
        except:
            conv = layout.get_conveyor_by_position(element[1], element[0])
            if not conv.type.value == 1:
                layout.remove_conveyor_by_id(conv.cell_value)
                layout.add_conveyor(random.choice(modular_conveyors), element[1], element[0], random.randint(0, 1))
            # should be free?
