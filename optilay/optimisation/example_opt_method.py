from optilay.evaluation.evaluator import Evaluator
from optilay.model.layout_model import LayoutModel


class MethodA:
    """
    This is an example optimisation class.
    The optimiser initialises the class with the LayoutModel, Evaluator and then calls the start_optimisation method.
    Otherwise, the class can be freely modified.
    """

    def __init__(self, layout: LayoutModel, evaluator: Evaluator) -> None:
        """

        :param layout: LayoutModel that is to be optimised.
        :param evaluator: Evaluator that is available for evaluation.
        """
        self.layout = layout
        self.evaluator = evaluator

    def start_optimisation(self) -> None:
        """
        The optimisation with this method is carried out here.
        :return: None
        """
        raise NotImplementedError
