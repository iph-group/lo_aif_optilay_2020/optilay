import logging
import multiprocessing

from tqdm import tqdm

from optilay.evaluation.evaluator import Evaluator
from optilay.gui.util.write_stream import WriteStream
from optilay.io.io_manager import IOManager
from optilay.io.io_type import IOType
from optilay.model.layout_model import LayoutModel
from optilay.model.util.cell_size import CellSize
from optilay.optimisation.optimiser import optimiser


class MainSystem:
    """
    This class is the main system of Optilay. All important Optilay functions are carried out from here.
    """

    def __init__(self) -> None:
        """

        """
        self.layout: LayoutModel = LayoutModel()
        self.io_manager: IOManager = IOManager()
        self.evaluator: Evaluator = Evaluator()

    def new_layout(self, length: int, width: int, cell_size: CellSize) -> None:
        """
        This overwrites the current LayoutModel of the system and replaces it with a new one.
        :return: None
        """
        self.layout = LayoutModel(length, width, cell_size)
        self.reset_operator()

    def load_layout(self, path: str) -> None:
        """
        This loads a new LayoutModel from the file system and overwrites the current one.
        :param path: File path where the LayoutModel to be loaded is located.
        :return: None
        """
        try:
            self.layout = self.io_manager.load(path)
            self.reset_operator()
            self.io_manager.path = path
        except Exception as e:
            logging.error('Error in load_layout: /n {}'.format(str(e)))

    def save(self, io_type: IOType, gui=False) -> None:
        """
        This saves the current LayoutModel to the path stored in the IOManager.
        :param io_type: File format to be saved.
        :param gui: Indicates whether the method is called from the MainWindow.
        :return: None
        """
        try:
            self.io_manager.save(io_type, self.layout)
        except Exception as e:
            logging.error('Error in save: /n {}'.format(str(e)))
            if gui:
                raise e

    def save_as(self, io_type: IOType, path: str, gui=False) -> None:
        """
        This saves the current LayoutModel at the given path.
        :param io_type: File format to be saved.
        :param path: File path where the LayoutModel is to be saved.
        :param gui: Indicates whether the method is called from the MainWindow.
        :return: None
        """
        try:
            self.io_manager.save(io_type, self.layout, path=path)
        except Exception as e:
            logging.error('Error in save_as: /n {}'.format(e))
            if gui:
                raise e

    def reset_layout(self) -> None:
        """
        Resets the current ConveyorSystemLayout. All placed elements are deleted
        :return: None
        """
        self.layout.get_current_layout().reset()

    def evaluate(self, eval_all=False) -> None:
        """
        This method starts the evaluation of the current LayoutModel and saves the EvaluationResult
        internally.
        :return: None
        """
        try:
            self.evaluator.evaluate(self.layout, eval_all)
        except Exception as e:
            logging.error('Error in evaluate: /n {}'.format(str(e)))

    def evalutate_layoutdb(self) -> None:
        """

        :return: None
        """
        try:
            self.evaluator.evaluate_all_objectives_layoutdb(self.layout)
        except Exception as e:
            logging.error('Error in evaluate_layoutdb: /n {}'.format(str(e)))

    def result_updater_layoutdb(self, update_all=False) -> None:
        """

        :param update_all:
        :return: None
        """
        try:
            self.evaluator.result_updater_layoutdb(self.layout, update_all=update_all)
        except Exception as e:
            logging.error('Error in update results in layout db: /n {}'.format(str(e)))

    def optimise(self, output_stream: WriteStream = None, error_queue: multiprocessing.Queue = None,
                 gui_pid_list=None) -> multiprocessing.Queue:
        """
        Starts the optimisation of the LayoutModel. This starts as many processes as specified in
        ControlParameter. If the optimisation is not started by the MainWindow, the results are saved in the
        LayoutModel, otherwise a multiprocessing.queue is returned with the results of the various processes.
        :param output_stream: A stream where the processes write their outputs.
        :param error_queue: A multiprocessing.Queue where all Exceptions thrown in the processes are caught.
        :param gui_pid_list: A list passed from the MainWindow where the processes are stored inside. Signals whether
                                the method was called by the MainWindow.
        :return: A queue with the results from the processes if called from MainWindow
        """
        logging.info("Starting Optimisation")

        try:
            result_queue = multiprocessing.Queue()

            if gui_pid_list is not None:
                gui = True
            else:
                gui_pid_list = []
                queue = multiprocessing.Queue()
                output_stream = WriteStream(queue)
                gui = False

            logging.info("Creating processes")
            for _ in range(self.get_layout().get_parameter().worker):
                gui_pid_list.append(
                    multiprocessing.Process(target=optimiser, args=(self.layout, self.evaluator, result_queue,),
                                            kwargs={'output_stream': output_stream, 'error_queue': error_queue}))
            logging.info(gui_pid_list)

            for p in gui_pid_list:
                p.start()
                logging.info("Process {} started".format(p.pid))

            if not gui:
                logging.error("not in gui part")
                active_process = len(gui_pid_list)
                with tqdm(total=active_process * 100) as pbar:
                    while True:
                        text = queue.get()
                        if text == 'Done!':
                            active_process -= 1
                            if active_process == 0:
                                break
                        if text == '+1':
                            pbar.update(1)
                        elif text not in [' ', '\n']:
                            pbar.set_postfix(ordered_dict={'logs': text})
                for p in gui_pid_list:
                    while not result_queue.empty():
                        self.get_layout().get_db().merge_db(result_queue.get())
                    p.join()
                    logging.info("Process {} joined.".format(p.pid))
            else:
                return result_queue

        except Exception as e:
            logging.error('Error in optimise: {}'.format(str(e)))

    def reset_operator(self) -> None:
        """
        Resets the internal settings of all operators, such as IOManager, Evaluator
        :return: None
        """
        self.io_manager.reset()
        self.evaluator.reset()

    def get_layout(self) -> LayoutModel:
        """
        Returns the current LayoutModel.
        :return: LayoutModel
        """
        return self.layout
