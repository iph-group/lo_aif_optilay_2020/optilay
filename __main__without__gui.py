import copy
import logging
import socket

from optilay.io.io_type import IOType
from optilay.main_system import MainSystem
from optilay.model.sytemconnection.system_connection_type import SystemConnectionType
from optilay.model.util.cell_size import CellSize
from optilay.optimisation.optimisation_method import OptimisationMethod
from optilay.evaluation.util_graph.graph_generation import LayoutGraph

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    # Create program
    system = MainSystem()
    # get PC name
    pc_name = socket.gethostname()

    if pc_name == "LO-Quamfap" or pc_name == "Aurich":
        # load layout
        system.load_layout("D:\\LO_AiF(BVL)_OptiLay_2020\\Projektbearbeitung\\AP4_Umsetzung_des_Demonstrators\\OptiLay_Testcase.json")
        layout_model = system.get_layout()
    elif pc_name in ["LO-CAD01", "LO-CAD02", "lo-cad03", "lo-cad04", "POOL01", "POOL02", "POOL03", "POOL04", "POOL05",
                     "POOL06", "POOL07", "POOL08"]:

        # Remote-PCs
        # open file which includes path to factory modell
        file = open('//192.168.50.13/projekte/LO_AiF(BVL)_OptiLay_2020/Projektbearbeitung/AP4_Umsetzung_des_Demonstrators/file_path.txt',
                    'r')
        file_lines = file.readlines()

        if pc_name == "LO-CAD01":
            file_path = file_lines[0]
        elif pc_name == "LO-CAD02":
            file_path = file_lines[1]
        elif pc_name == "lo-cad03":
            file_path = file_lines[2]
        elif pc_name == "lo-cad04":
            file_path = file_lines[3]
        elif pc_name == "POOL01" or pc_name == "POOL05":
            file_path = file_lines[4]
        elif pc_name == "POOL02" or pc_name == "POOL06":
            file_path = file_lines[5]
        elif pc_name == "POOL03" or pc_name == "POOL07":
            file_path = file_lines[6]
        elif pc_name == "POOL04" or pc_name == "POOL08":
            file_path = file_lines[7]
        else:
            file_path = file_lines[8]

        file.close()

        # cut line break
        if file_path[-1] == "\n":
            file_path = file_path[:-1]

        # load layout
        system.load_layout(file_path)
        layout_model = system.get_layout()
    elif pc_name == "Marcs-MBP.fritz.box":
        system.load_layout("/Users/marc/Documents/Projects/Python/optilay/todo.json")
        layout_model = system.get_layout()
        layout_model.get_parameter().number_of_generations = 10

    else:
        # Create new layout
        system.new_layout(2500, 2500, CellSize.CELL_400)
        # Get layout model from our system this is where everything is stored
        layout_model = system.get_layout()
        # Other data stored in our layout model
        weighting = layout_model.get_weighting()
        print(weighting.__dict__)
        parameter = layout_model.get_parameter()
        parameter.opt_method = OptimisationMethod.RandomPlacement
        parameter.number_of_iterations = 100
        parameter.conv_types = ['belt_conveyor_400', 'Celluveyor_400']
        print(parameter.__dict__)
        db = layout_model.get_db()

        layout_model.weighting.throughput = 0
        layout_model.weighting.lead_time = 0
        layout_model.weighting.space_requirement = 0
        layout_model.weighting.buffer_capacity = 0
        layout_model.weighting.costs = 1

        # Get our current layout to place conveyors etc.
        unoptimised_layout = layout_model.get_current_layout()
        unoptimised_layout.add_deactivated_area(6, 0)
        unoptimised_layout.deactivated[0].rotation = 3
        unoptimised_layout.add_deactivated_area(5, 0)
        unoptimised_layout.add_deactivated_area(6, 1)
        unoptimised_layout.add_deactivated_area(5, 1)
        unoptimised_layout.add_restricted_area(0, 0)
        unoptimised_layout.add_workplace(6, 6)
        unoptimised_layout.add_conveyor('belt_conveyor_400', 4, 2)
        conv = unoptimised_layout.get_conveyor_by_position(4, 2)
        conv.rotation = 1
        print(unoptimised_layout.layout)
        convvv = unoptimised_layout.add_conveyor('belt_conveyor_400', 1, 2)
        unoptimised_layout.remove_conveyor_by_id(convvv.cell_value)
        print(unoptimised_layout.layout)
        sink = unoptimised_layout.add_system_connection(6, 5, SystemConnectionType.SINK)
        source = unoptimised_layout.add_system_connection(1, 0, SystemConnectionType.SOURCE)
        unoptimised_layout.add_goods_between(sink, source, 'Parcel_280x180x170', load=100)

    layout_model.parameter.worker = 1

    """
    layout_model.parameter.number_of_generations = 2500
    layout_model.parameter.population_size = 50
    layout_model.parameter.mutation_rate = 0.9
    layout_model.parameter.maximal_number_of_mutations = 1
    layout_model.parameter.crossover_two_point_operator_length = 1
    #"""

    """
    layout_model.weighting.throughput = 0.8
    layout_model.weighting.costs = 0.2
    layout_model.weighting.space_requirement = 0
    layout_model.weighting.buffer_capacity = 0
    layout_model.weighting.lead_time = 0
    #"""

    # Optimise layout
    system.optimise()

    """
    graph = LayoutGraph(layout_model.get_current_layout())
    graph.get_paths(layout_model.get_current_layout().packet_stream, write_to_packet_streams=True)
    graph.get_intersections(layout_model.get_current_layout().packet_stream)
    graph.plot_graph()

    system.evalutate_layoutdb()
    system.result_updater_layoutdb(update_all=True)

    breakpoint()
    """

    # Start optimisation
    # system.optimise()
    # Get the 2 best results available
    # results: List[ConveyorSystemLayout] = layout_model.get_best_layouts(2)
    # Save the layout model to a JSON file
    system.save_as(IOType.JSON, './test.json')

    # system.load_layout('./test.json')

